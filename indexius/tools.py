# -*- coding: UTF-8 -*-
# Copyright (C) 2019 Schmidt Cristian Hernán

import re
import json
import datetime
import unicodedata

from indexius.config import CFG

REGEX_LINE_RE = re.compile('^([^ ])?( +)(.+?) *$')
PATTERN_RE = re.compile(r'((\\+)([A-Za-z])[+*]?)(\\\3[+*]?)+')
NO_WORD_RE = re.compile(r'[^\w]+')
NUMBER_RE = re.compile(r'^(?:\d+(?:([,.])\d*)?|([,.])\d+)$')
KEY_OK_RE = re.compile(r'^(?i:(ok|result)s?)$')
VAL_OK_RE = re.compile(r'^(?i:(ok|true|succ?ess?|yes|si|1(\.0*)?))$')
KEY_ER_RE = re.compile(r'^(?i:(err?or|mess?age|descrip(t(ion)?)?|result)s?)$')
HTTPS_RE = re.compile(r'(^https?://|/+$)', re.IGNORECASE)
SLIM_RE = re.compile(r'[fiIjlrt\s]+')
YES_RE = re.compile(r'(y|s)[aei]|[tj]a|ok|oui|если|ναι', re.IGNORECASE)

ID_SEP = re.compile(r'''
 (["']?)        # Group 1 → quote
 ((?(1)         # Group 2 → identifier
     .+?        #   any character (in quotes)
   | \S+        #   or any non-space character (without quotes)
 ))
 (?(1)\1)       # quote closure
 (\s+)          # Group 3 → space
 ((?(3).+))     # Group 4 → text
''', re.VERBOSE)


class QueryDict(dict):

    def __init__(self, other=None, **kwargs):
        super().__init__()
        self.update(other, **kwargs)

    def __setitem__(self, key, value):
        ''' self[key] = value '''
        value = self.serialize(value)
        try:
            lst = self.__getitem__(key)
            if isinstance(lst, list):
                lst.append(value)
            else:
                super().__setitem__(key, [lst, value])
        except KeyError:
            super().__setitem__(key, value)

    def update(self, other=None, **kwargs):
        if other and self is not other:
            if hasattr(other, 'keys'):
                for key in other:
                    self[key] = other[key]
            else:
                for key, value in other:
                    self[key] = value
        for key in kwargs:
            self[key] = kwargs[key]

    @staticmethod
    def serialize(value):

        if isinstance(value, datetime.datetime):
            return int(value.timestamp())

        if isinstance(value, str):
            obj = NUMBER_RE.match(value)
            if obj:
                if any(obj.groups()):
                    return float(value)
                return int(value)

        return value


def pattern_normalizer(obj):
    text = obj.group()
    init = obj.group(1)
    bars = obj.group(2)
    char = obj.group(3)

    num = len(bars)
    if num % 2 == 0:
        prefix = init
        text = text[obj.end(1):]
    else:
        prefix = bars[:-1]

    modifier = '*' if text.count(char) == text.count('*') else '+'
    return fr'{prefix}\{char}{modifier}'


def get_limited_name(name):
    if len(name) < CFG.LIMIT_NAME_LENGTH:
        return name
    return name[:CFG.LIMIT_NAME_LENGTH - 1] + '…'


def get_arg(text):
    if text:
        parts = text.split(None, 1)
        if len(parts) > 1:
            cmd, arg = parts
            if cmd.startswith('/') and len(cmd) > 1 and arg:
                return arg.strip()
    return ''


def extract_error_from_dict(dic):
    for key, value in dic.items():
        if KEY_ER_RE.match(str(key)):

            if value and isinstance(value, str):
                return value

            if not isinstance(value, list):
                value = [value]

            for val in value:
                if isinstance(val, dict):
                    ret = extract_error_from_dict(val)
                    if ret:
                        return ret

                if val and isinstance(val, str):
                    return val
    return ''


def extract_errors_from_response(response):
    # A well designed API returns an HTTP state according to the result,
    # but many APIs always return an HTTP 200 state and add some information
    # to the body of the response...
    # Here is an attempt to find those errors
    try:
        data = response.json()
        if not isinstance(data, dict):
            data = {'ok': data}
            added = True
        else:
            added = False

        # Search for an indicator that it was correct
        for key, value in data.items():
            if KEY_OK_RE.match(str(key)) and VAL_OK_RE.match(str(value)):
                return ''

        if added:
            data['error'] = data.pop('ok')

        # Search for a possible error
        error = extract_error_from_dict(data)
        if error:
            return error

    except (json.JSONDecodeError, ValueError, TypeError):
        # At the moment only the body in JSON is analyzed
        pass

    # Presumed to have been correct
    return ''


def pseudo_text_length(text):
    ''' Counts the number of characters without considering mark and nonspacing,
        and only ~50% of the slim letters: space, "f", "i", "I", "j", "l", "r", "t"
    '''
    text = unicodedata.normalize('NFKD', text).strip()
    slim = sum(o.end() - o.start() for o in SLIM_RE.finditer(text)) // 2
    norm = len([c for c in SLIM_RE.sub('', text) if unicodedata.category(c) != 'Mn'])
    return slim + norm


def remove_diacritics(text):
    '''Remove the Mark and Nonspacing characters from the text.'''
    nfkd = unicodedata.normalize('NFKD', text)
    return ''.join(c for c in nfkd if unicodedata.category(c) != 'Mn')


def get_positions(text, letter, offset=0):
    ''' Returns all positions of the letter in the text,
        offset is an integer that modifies all positions.
    '''
    for index, char in enumerate(text):
        if letter == char:
            yield index + offset


def texdiff(text_a, text_b, insensitive=True, accents=False, onlyword=False):
    ''' Returns the fraction of the difference between "text_a" and "text_b".

        An incorrect letter is more penalized in a short word than in a long one.

        Parameters:
            insensitive=True    Set case-insensitive
            accents=False       There is no distinction between words with and
                                without accents
            onlyword=False      It analyzes only the characters considered word
                                in the regular expressions

        Use:
            fraction = texdiff(text_a, text_b)

        where:
            0 <= fraction <= 1

            0.0 Zero difference, "text_a" and "text_b" are the same
            0.5 Half difference, example: the same letters in another order
            1.0 Full difference, no letter matches

       Design: Schmidt Cristian Hernán <schcriher@gmail.com>
    '''
    if insensitive:
        text_a = text_a.lower()
        text_b = text_b.lower()

    if not accents:
        text_a = remove_diacritics(text_a)
        text_b = remove_diacritics(text_b)

    if onlyword:
        text_a = NO_WORD_RE.sub('', text_a)
        text_b = NO_WORD_RE.sub('', text_b)

    a_offset = text_b.find(text_a) if text_a in text_b else 0
    b_offset = text_a.find(text_b) if text_b in text_a else 0

    diff_quantity = 0
    diff_position = 0

    for letter in set(text_a + text_b):

        count_a = text_a.count(letter)
        count_b = text_b.count(letter)
        diff_quantity += abs(count_a - count_b)

        pos_a = set(get_positions(text_a, letter, a_offset))
        pos_b = set(get_positions(text_b, letter, b_offset))
        diff_position += len(pos_a.symmetric_difference(pos_b))

    return (diff_quantity + diff_position) / (2 * len(text_a + text_b))
