# -*- coding: UTF-8 -*-
# Copyright (C) 2019 Schmidt Cristian Hernán

import re
import enum

from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove, Chat
from telegram.ext import ConversationHandler

from indexius.tools import pseudo_text_length
from indexius.texts import (ARG_VALID_CHANNELS, ARG_VALID_GROUPS, ARG_INVALID,
                            CREATE_ACTION, UPDATE_ACTION, LIST_ACTION,
                            DELETE_ACTION, TEST_ACTION, NO_OPTION, YES_OPTION,
                            OVERALL_OPTION, UNCHANGED_OPTION, MENUARGS_CLS_OPTION,
                            MENUARGS_END_OPTION, GET_OPTION,
                            POST_FORM_OPTION, POST_JSON_OPTION)
from indexius.config import CFG


@enum.unique
class RuleRegex(enum.Enum):
    ONLY_NULL = enum.auto()
    ONLY_SET = enum.auto()
    ALL = enum.auto()


@enum.unique
class MenuStep(enum.IntEnum):
    STOP = ConversationHandler.END
    CHAT = enum.auto()
    ACTION = enum.auto()
    NAME = enum.auto()
    REGEX = enum.auto()
    RCONF = enum.auto()
    LNAME = enum.auto()
    LCONF = enum.auto()
    ARGS = enum.auto()
    ACONF = enum.auto()
    URL = enum.auto()
    UCONF = enum.auto()
    SEND = enum.auto()
    TEST = enum.auto()


@enum.unique
class MenuAction(enum.Enum):
    CREATE = CREATE_ACTION
    UPDATE = UPDATE_ACTION
    DELETE = DELETE_ACTION
    LIST = LIST_ACTION
    TEST = TEST_ACTION

    def __str__(self):
        return self.value


@enum.unique
class MenuArgs(enum.Enum):
    # For all
    MID = 'message_id'
    MDT = 'message_date'
    CID = 'chat_id'
    CTP = 'chat_type'
    CTT = 'chat_title'
    CUN = 'chat_username'
    # Only in channels
    PAU = 'post_author'   # author_signature
    PCW = 'post_website'  # connected_website
    # Only in groups
    UID = 'user_id'
    UFN = 'user_first_name'
    ULN = 'user_last_name'
    UUN = 'user_username'
    # For all (control)
    CLS = MENUARGS_CLS_OPTION
    END = MENUARGS_END_OPTION

    def __str__(self):
        return self.value

    @property
    def for_channel(self):
        # pylint: disable=no-member
        return not (self.value.startswith('user_') or self.for_control)

    @property
    def for_group(self):
        # pylint: disable=no-member
        return not (self.value.startswith('post_') or self.for_control)

    @property
    def for_control(self):
        return self.name in ('CLS', 'END')

    # --- class

    @classmethod
    def get_only_channels(cls):
        return [arg.value for arg in cls if arg.value.startswith('post_')]

    @classmethod
    def get_only_groups(cls):
        return [arg.value for arg in cls if arg.value.startswith('user_')]

    @classmethod
    def get_channels(cls):
        return [arg.value for arg in cls if arg.for_channel]

    @classmethod
    def get_groups(cls):
        return [arg.value for arg in cls if arg.for_group]

    @classmethod
    def get_control(cls):
        return [arg.value for arg in cls if arg.for_control]

    @classmethod
    def get(cls, name, scope):
        try:
            arg = cls(name)

            if (not arg.for_control and (
                    (scope == Chat.CHANNEL and not arg.for_channel) or
                    (scope in (Chat.GROUP, Chat.SUPERGROUP) and not arg.for_group))):

                if arg.for_channel:
                    return None, ARG_VALID_CHANNELS
                return None, ARG_VALID_GROUPS

            return arg, None

        except ValueError:
            return None, ARG_INVALID


@enum.unique
class SendType(enum.Enum):
    # pylint: disable=no-member,unsubscriptable-object

    GET = (GET_OPTION, 'params')
    POST_FORM = (POST_FORM_OPTION, 'data')
    POST_JSON = (POST_JSON_OPTION, 'json')

    @property
    def method(self):
        return self.name.split('_')[0]

    @property
    def var(self):
        return self.value[1]

    @classmethod
    def full_list(cls):
        return '\n'.join(f'\xa0•\xa0{i.name}:\xa0{i.value[0]}' for i in cls)


class Keyboard:

    no = NO_OPTION
    yes = YES_OPTION
    overall = OVERALL_OPTION
    unchanged = UNCHANGED_OPTION

    send = [i.name for i in SendType]
    _control = MenuArgs.get_control()
    control = [_control]
    control_unchanged = [_control, [unchanged]]

    def get(options, footer=None, one_time=True):
        # pylint: disable=no-self-argument,not-an-iterable
        keyboard = []
        if options:
            available_chars = CFG.MAX_CHARS_WIDE_KB
            current_row = []
            for option in options:

                # Empirical estimations
                text_length = pseudo_text_length(option)
                required_chars = text_length

                # The separation between options also requires space
                nsep = len(current_row)  # sep with the current row
                if nsep > 0:
                    required_chars += 3 * nsep

                # If there is no space left, pass to the next row
                if required_chars > available_chars:
                    # In the case of a very long option
                    # 'current_row' will be empty (do not add it)
                    if current_row:
                        keyboard.append(current_row)
                    # But still add the option to the keyboard, 'available_chars'
                    # will be negative in the next cycle leaving the long option
                    # in a single row
                    current_row = []
                    available_chars = CFG.MAX_CHARS_WIDE_KB
                    required_chars = text_length

                # Store
                current_row.append(option)
                available_chars -= required_chars

            keyboard.append(current_row)

        for row in footer or []:
            if row:
                keyboard.append(row)

        return ReplyKeyboardMarkup(keyboard,
                                   resize_keyboard=True,
                                   one_time_keyboard=one_time)

    def get_args(chat_type, create, exclude_args=None):
        # pylint: disable=no-self-argument
        method = f'get_{chat_type}s'.replace('super', '').lower()
        get_all = getattr(MenuArgs, method)
        args_kb = [arg for arg in get_all() if arg not in (exclude_args or [])]
        foot_kb = Keyboard.control if create else Keyboard.control_unchanged
        return Keyboard.get(args_kb, foot_kb, one_time=False)

    _CONF = get([no, yes])

    NULL = ReplyKeyboardRemove()
    ACTION = get([i.value for i in MenuAction])
    NAME_UPDATE = get([unchanged])
    REGEX_CREATE = get([overall])
    REGEX_UPDATE = get([overall, unchanged])
    RCONF = _CONF
    LNAME_UPDATE = get([unchanged])
    LCONF = _CONF
    ACONF = _CONF
    URL_UPDATE = get([unchanged])
    UCONF = _CONF
    SEND_CREATE = get(send)
    SEND_UPDATE = get(send, [[unchanged]])

    @classmethod
    def get_list(cls, obj):
        lst = []
        if isinstance(obj, str):
            obj = getattr(cls, obj)
        for row in obj.keyboard:
            lst.extend(row)
        return lst


class Regex:
    # pylint: disable=too-few-public-methods

    default = '(?s).+'
    arg_sep = '; '
    arg_sep_re = re.compile(r'\s*{}\s*'.format(arg_sep.strip()))

    def get(options):
        # pylint: disable=no-self-argument
        return '^({})$'.format('|'.join(options))

    _CONF = get([Keyboard.no, Keyboard.yes])

    CHAT = r'^\d+•\s+.+$'
    ACTION = get([i.value for i in MenuAction])
    RCONF = _CONF
    LNAME = get([r'[a-zA-Z]\w*', Keyboard.unchanged])
    LCONF = _CONF
    ACONF = _CONF
    UCONF = _CONF
    SEND = get([i.name for i in SendType] + [Keyboard.unchanged])
