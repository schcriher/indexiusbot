# -*- coding: UTF-8 -*-
# Copyright (C) 2019 Schmidt Cristian Hernán

import sys

if sys.hexversion < 0x03060000:
    raise RuntimeError('requires python 3.6 or higher')

from indexius.core import run  # pylint: disable=wrong-import-position
