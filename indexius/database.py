# -*- coding: UTF-8 -*-
# Copyright (C) 2019 Schmidt Cristian Hernán

import html
import datetime

from sqlalchemy import (create_engine, Column, ForeignKey, null,
                        BigInteger, Integer, String, Boolean, DateTime)
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base

from indexius.debug import flogger
from indexius.tools import NO_WORD_RE, ID_SEP, texdiff, get_limited_name
from indexius.models import RuleRegex, SendType
from indexius.config import CFG

AT = '@\N{ZERO WIDTH NO-BREAK SPACE}'
BASE = declarative_base()


class Chat(BASE):
    __tablename__ = 'chat'

    id = Column(BigInteger, primary_key=True)
    type = Column(String, nullable=False)
    title = Column(String)
    ignore = Column(Boolean, default=False)
    interacts = Column(BigInteger, nullable=False, default=0)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)
    rules = relationship('Rule', back_populates='chat', cascade="all, delete-orphan")

    def __str__(self):
        return (f'CHAT:{self.id}:{self.title}:{self.type}:I{self.interacts}:'
                f'IG{self.ignore}')

    @property
    def title_html(self):
        return html.escape(self.title)


class Rule(BASE):
    __tablename__ = 'rule'

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    regex = Column(String)
    lname = Column(String)
    args = Column(String)
    url = Column(String, nullable=False)
    send = Column(String)
    ignore = Column(Boolean, nullable=False, default=False)
    attempts = Column(Integer, nullable=False, default=0)
    interacts = Column(BigInteger, nullable=False, default=0)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)
    used_at = Column(DateTime, default=datetime.datetime.utcnow)
    user_id = Column(BigInteger, nullable=False)  # last user to edit the rule
    chat_id = Column(BigInteger, ForeignKey('chat.id'))
    chat = relationship('Chat', back_populates='rules')

    def __str__(self):
        return (f'RULE:{self.id}:{self.name}:I{self.interacts}:IG{self.ignore}:'
                f'A{self.attempts}')

    @property
    def send_type(self):
        return SendType[self.send]

    @property
    def name_html(self):
        return html.escape(self.name)

    @property
    def url_html(self):
        return html.escape(self.url)

# ---

@flogger
def create_session(create_all=False, url=None):
    engine = create_engine(url or CFG.DATABASE_URL)
    session = sessionmaker(bind=engine)

    if create_all:
        BASE.metadata.create_all(engine)

    return session

# ---

@flogger
def exists_rule_name(dbs, name, chat_id):
    _name = NO_WORD_RE.sub('%', name)
    query = dbs.query(Rule).filter(Rule.chat_id == chat_id)
    rules = query.filter(Rule.name.like(_name)).all()
    return any(rule.name == name for rule in rules)


@flogger
def get_rule(dbs, rule_id):
    return dbs.query(Rule).filter_by(id=rule_id).first()


@flogger
def get_rules(dbs, chat_id, which=RuleRegex.ALL, count=False):
    query = dbs.query(Rule).filter(Rule.chat_id == chat_id)

    if not count:
        # Not apply for counting the total rules
        query = query.filter(Rule.attempts <= CFG.RULE_MAX_NUM_ERRS)

    if which is RuleRegex.ONLY_NULL:
        query = query.filter(Rule.regex == null())

    elif which is RuleRegex.ONLY_SET:
        query = query.filter(Rule.regex != null())

    if count:
        return query.count()
    return query.all()


@flogger
def get_chat(dbs, chat_id, chat_type, chat_title):
    chat = dbs.query(Chat).filter_by(id=chat_id).first()
    title = get_limited_name(chat_title)
    if chat:
        # chat type cannot change, migrations change the id directly
        if chat.title != title:
            chat.title = title
    else:
        chat = Chat(id=chat_id, type=chat_type, title=title)
        dbs.add(chat)
        dbs.flush()
    return chat


@flogger
def migrate_chat(dbs, old_id, new_id, title=None):
    # Can only migrate "groups" to "supergroups"

    # Create new chat (must be a supergroup)
    get_chat(dbs, new_id, 'supergroup', title or '«automatically updated later»')

    # Updating the rules
    for rule in dbs.query(Rule).filter(Rule.chat_id == old_id):
        rule.chat_id = new_id

    # Remove old chat
    dbs.query(Chat).filter_by(id=old_id).delete()
    dbs.flush()


@flogger
def get_query(arg, query, attribute=None):
    identifier, text = ID_SEP.match(arg).group(2, 4)

    if identifier.lstrip('-').strip().isdigit():
        obj = query.filter_by(id=identifier).first()
    else:
        name = identifier.lstrip('@')
        similar = []
        for obj in query.all():
            value = getattr(obj, attribute)
            if value:
                diff = texdiff(name, value)
                if diff < CFG.MAX_DIFF_NAMES:
                    similar.append((diff, obj))
        if similar:
            similar.sort()
            obj = similar[0][1]
        else:
            obj = None

    return obj, text
