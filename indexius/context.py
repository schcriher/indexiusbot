# -*- coding: UTF-8 -*-
# Copyright (C) 2019 Schmidt Cristian Hernán

import time
import logging
import datetime
import functools
import threading
import collections

from telegram import TelegramError, ChatAction
from telegram.ext import run_async
from telegram.utils.helpers import mention_html

from indexius.debug import flogger
from indexius.cache import adm_cache, MemoryCache
from indexius.database import create_session, get_chat

HTML_NO_PREVIEW = {'parse_mode': 'HTML', 'disable_web_page_preview': True}


@adm_cache
@flogger
def get_admin_ids(bot, chat_id):
    """ Returns a list of admin IDs for a given chat """
    return [admin.user.id for admin in bot.get_chat_administrators(chat_id)]


@adm_cache
@flogger
def all_members_are_administrators(bot, chat_id):
    """ Returns true if all members are administrators in the given chat """
    return bot.get_chat(chat_id).all_members_are_administrators


class Context:
    # pylint: disable=no-member

    """ Contains the data of a request """

    def __init__(self, kwargs):
        self.logger = logging.getLogger(__name__)
        self.__dict__.update(kwargs)
        self.__text = None

        # Definition alias to methods
        params = HTML_NO_PREVIEW.copy()

        params['chat_id'] = self.cid
        self.send = self._define(self.bot.send_message, **params)

        if self.tgc.type == self.tgc.PRIVATE:
            self.reply = self._define(self.bot.send_message, **params)
        else:
            self.reply = self._define(self.bot.send_message, **params,
                                      reply_to_message_id=self.mid)

        params['message_id'] = self.mid
        self.edit = self._define(self.bot.edit_message_text, **params)

        self.set_typing = self._define(self.bot.send_chat_action,
                                       chat_id=self.cid,
                                       action=ChatAction.TYPING)

    def __repr__(self):
        return f'«{self.__class__.__name__}»'

    #@flogger
    def _define(self, func, **params):
        @run_async
        @functools.wraps(func)
        def _define_wrapper(**kwargs):
            throw_exc = kwargs.pop('throw_exc', False)
            result = None
            try:
                result = func(**{**params, **kwargs})
            except TelegramError as tge:
                self.logger.warning('%s: \033[41m\033[1;37m %s \033[0m',
                                    func.__name__, tge)
                if throw_exc:
                    raise
            return result
        return _define_wrapper

    @flogger
    def is_admin(self, chat_id, user_id):
        try:
            if user_id in get_admin_ids(self.bot, chat_id):
                return True
            return all_members_are_administrators(self.bot, chat_id)

        except TelegramError:
            chat = get_chat(self.dbs, chat_id, '', '')
            self.dbs.delete(chat)
            self.dbs.flush()
            return False

    @property
    def uid(self):
        return self.tgu.id

    @property
    def cid(self):
        return self.tgc.id

    @property
    def mid(self):
        return self.tgm.message_id

    @property
    def text(self):
        return self.__text or self.tgm.text or ''

    @text.setter
    def text(self, text):
        self.__text = text

    @property
    def nick(self):
        return mention_html(self.tgu.id, self.tgu.name or self.tgu.id)


class Contextualizer:

    @flogger
    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.locks = collections.defaultdict(threading.Lock)
        self.cache = MemoryCache()
        self.dbses = None

    #@flogger
    def __repr__(self):
        return f'«{self.__class__.__name__}»'

    @flogger
    def initialize(self, create_all=True, url=None):
        self.dbses = create_session(create_all, url)

    #@flogger
    def __call__(self, func):
        @functools.wraps(func)
        def __call___wrapper(*args, **kwargs):
            if args and isinstance(args[0], Context):
                result = func(args[0])
            else:
                result = None

                update = args[1]
                tgu = update.effective_user
                tgc = update.effective_chat
                tgm = update.effective_message

                start = time.time()
                with self.locks[tgc.id]:
                    self.logger.debug('%s wait %.3f seconds', func.__name__,
                                      time.time() - start)
                    self.logger.debug('db open')
                    dbs = self.dbses()
                    try:
                        if tgc.type != tgc.PRIVATE:
                            chat = get_chat(dbs, tgc.id, tgc.type, tgc.title)
                        else:
                            chat = None

                        if not chat or not chat.ignore:
                            kwargs['update'] = update
                            kwargs['bot'] = args[0]
                            kwargs['dbs'] = dbs
                            kwargs['tgu'] = tgu
                            kwargs['tgc'] = tgc
                            kwargs['tgm'] = tgm
                            kwargs['chat'] = chat
                            kwargs['cache'] = self.cache
                            ctx = Context(kwargs)
                            self.logger.debug('go to %s', func.__name__)
                            result = func(ctx)
                            if chat:
                                chat.interacts += 1
                                chat.updated_at = datetime.datetime.utcnow()
                    except:
                        self.logger.exception('db rollback')
                        dbs.rollback()
                        raise
                    else:
                        self.logger.debug('db commit')
                        dbs.commit()
                    finally:
                        self.logger.debug('db close')
                        dbs.close()
            return result
        return __call___wrapper
