# -*- coding: UTF-8 -*-
# Copyright (C) 2019 Schmidt Cristian Hernán

import time
import functools
import threading

from indexius.debug import flogger
from indexius.config import CFG


def adm_cache(func):
    cache = {}

    @functools.wraps(func)
    def adm_cache_wrapper(bot, chat_id):
        now = time.time()
        try:
            ret, age = cache[chat_id]
            if age + CFG.CACHING_ADM_TIME < now:
                raise KeyError
        except KeyError:
            ret = func(bot, chat_id)
            cache[chat_id] = ret, now
        return ret

    adm_cache_wrapper.clear = cache.clear
    return adm_cache_wrapper


LOCK = threading.RLock()


def sync(method):
    """ Synchronization decorator for MemoryCache """
    @functools.wraps(method)
    def sync_wrapper(*args, **kwargs):
        with LOCK:
            return method(*args, **kwargs)
    return sync_wrapper


class MemoryCache:

    __slots__ = ('__data',)

    def __init__(self):
        self.__data = {}

    def __repr__(self):
        return f'«{self.__class__.__name__}»'

    @sync
    @flogger
    def __getitem__(self, keys):
        data, key = self.__get_last_data_key(keys, 'get')
        return data.get(key, None)

    @sync
    @flogger
    def pop(self, *keys):
        data, key = self.__get_last_data_key(keys, 'get')
        return data.pop(key, None)

    @sync
    @flogger
    def __setitem__(self, keys, value):
        data, key = self.__get_last_data_key(keys, 'setdefault')
        data[key] = value

    @sync
    @flogger
    def __delitem__(self, keys):
        data, key = self.__get_last_data_key(keys, 'get')
        try:
            del data[key]
        except KeyError:
            pass

    #@flogger
    def __get_last_data_key(self, keys, method):
        if not isinstance(keys, (list, tuple)):
            keys = (keys,)

        data = self.__data
        for key in keys[:-1]:
            data = getattr(data, method)(key, {})

        return data, keys[-1]

    @sync
    @flogger
    def edited_chat(self, chat_id, user_id=None, nick=None, timeout=None):
        ''' Returns the nick that is editing the chat_id; if a user_id and nick is
            passed and no one is editing the chat, it is set to edited by that nick
        '''
        now = time.time()

        for uid in self.__data:
            if self[uid, 'chat', 'id'] == chat_id:
                if timeout and now - self[uid, 'last'] > timeout:
                    del self[uid]
                    break
                return self[uid, 'nick']

        if user_id and nick:
            # The chat will be completed later, here set the chat_id
            # to reserve the edition by this user
            self[user_id, 'chat', 'id'] = chat_id
            self[user_id, 'nick'] = nick
            self[user_id, 'last'] = now
            return True

        return False
