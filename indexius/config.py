# -*- coding: UTF-8 -*-
# Copyright (C) 2019 Schmidt Cristian Hernán

import os
import urllib3

from indexius import version

DEFAULT = {
    'TELEGRAM_USERNAME': str,   # bot username
    'TELEGRAM_TOKEN': str,      # bot token
    'TELEGRAM_FID': int,        # identified bot maintainer (father)

    'DATABASE_URL': str,        # dialect[+driver]://user:pass@host/dbname[?key=value..]

    'HOST': str,                # host server
    'PORT': int,                # typical: 443
    'BIND': str,                # typical: '0.0.0.0'
    'BOT_PATH': 'bot',          # telegram requests go to HOST:PORT/BOT_PATH

    'DATETIME_IN_LOG': True,
    'DATETIME_FORMAT': '%Y%m%d.%H%M%S',
    'LOG_TIME': '%(asctime)s.%(msecs)03d',
    'LOG_LINE': '%(levelname)-8s %(threadName)-10.10s %(name)-18s %(lineno)-4d '
                '%(message)s',

    'RELAX_TIME': 0.200,        # seconds of waiting to avoid overlapping
    'MAX_DIFF_NAMES': 0.222,    # difference for names
    'CACHING_ADM_TIME': 300.0,  # seconds to store the cache of administrators
    'TIMEOUT_EDIT_CHAT': 600.0, # seconds without activity to release the chat edition
    'LIMIT_RULE_NUMBER': 4,     # number of rule
    'MAX_CHARS_WIDE_KB': 33,    # number of characters (keyboard)
    'RULE_MAX_NUM_ERRS': 16,    # number of failed attempts to ignore the rule
    'LIMIT_NAME_LENGTH': 32,    # number of characters in names
    'REGEX_CACHE_NUMBER': 32,   # number of patterns in cache
}

INTERNAL = {
    # HTTP Status Code 500, 502, 503, 504 (server errors) may be temporary today
    'URL_TIMEOUT': 30,
    'URL_RETRIES': urllib3.util.retry.Retry(total=3,
                                            backoff_factor=0.9,
                                            status_forcelist=(500, 502, 503, 504)),
}

class Configuration:

    @property
    def log_format(self):
        if self.DATETIME_IN_LOG:
            return f'{self.LOG_TIME} {self.LOG_LINE}'
        return self.LOG_LINE

    @property
    def bot_url(self):
        return f'https://t.me/{self.TELEGRAM_USERNAME}'

    @property
    def user_agent(self):
        name = self.TELEGRAM_USERNAME.lower().rstrip('bot').rstrip('_').capitalize()
        return f'{name}/{version.VERSION} ({self.bot_url})'

    @property
    def bid(self):
        try:
            return self.TELEGRAM_BID
        except KeyError:
            bid = int(self.TELEGRAM_TOKEN.split(':')[0])
            self.__dict__['TELEGRAM_BID'] = bid
            return bid

    @property
    def fid(self):
        return self.TELEGRAM_FID

    def __getattr__(self, var):
        if var in INTERNAL:
            value = INTERNAL[var]
        else:
            default = DEFAULT.get(var)
            environ = os.getenv(var)

            if default is None:
                raise KeyError(f'No default value or variable type for {var}')

            if environ is None:
                if default.__class__ is type:
                    raise KeyError(f'{var} is not set in the environment variables')
                value = default
            else:
                value = self.__mapper__(default, environ)

        self.__dict__[var] = value
        return value

    @staticmethod
    def __mapper__(obj, val):
        decode = obj if obj.__class__ is type else obj.__class__

        if decode is bool:
            val = val.lower()
            if val in ('true', '1', 'yes'):
                return True
            if val in ('false', '0', 'no'):
                return False
            raise ValueError(f'Incorrect value for a boolean variable: {val}')

        if decode is int:
            # Allow the use of floats 1.0 → 1
            decode = lambda val: int(float(val))

        return decode(val)

CFG = Configuration()
