# -*- coding: UTF-8 -*-
# Copyright (C) 2019 Schmidt Cristian Hernán

from indexius.config import CFG

def sec_to_text(sec):

    if abs(sec) < 1:
        return f'{sec} segundos'

    minutes, seconds = divmod(int(sec), 60)
    hours, minutes = divmod(minutes, 60)
    days, hours = divmod(hours, 24)

    phrase = []
    items = (
        (days, 'día', 'días'),
        (hours, 'hora', 'horas'),
        (minutes, 'minuto', 'minutos'),
        (seconds, 'segundo', 'segundos')
    )
    for number, singular, plural in items:
        if number == 1:
            phrase.append(f'{number} {singular}')
        elif number != 0:
            phrase.append(f'{number} {plural}')

    if len(phrase) == 1:
        return phrase[0]
    return ', '.join(phrase[:-1]) + ' y ' + phrase[-1]

TIMEOUT_EDIT_CHAT = sec_to_text(CFG.TIMEOUT_EDIT_CHAT)
CACHING_ADM_TIME = sec_to_text(CFG.CACHING_ADM_TIME)

# -------------------------------------------------------------

USAGE_PUBLIC = '''
Soy un bot para indexar contenido en grupos y canales. \
Por privado tienes una explicación mas detallada.
'''

USAGE_PRIVATE = f'''
Soy un bot para indexar contenido en grupos y canales.

Por el momento no puedo almacenar la información internamente por lo que no \
dispongo de métodos de búsqueda. Tengo la finalidad de ser un "puente" entre telegram
y una base de datos extrena.

Todas mis funciones son utilizables únicamente por quienes sean administradores \
en el grupo o canal donde me usen y deben ser usadas en un chat privado. \
Ten en cuenta que todos los administradores de un grupo o canal determinado ven las \
mismas reglas (no pueden editarlas al mismo tiempo), las reglas se guardan por \
grupo o canal y no por usuario.

<b>Idea:</b> definir reglas a base de expresiones regulares (el patrón de \
búsqueda) y asignarles una url (webhook) donde enviar esos datos (mediante una \
petición GET o POST), además se pueden definir reglas sin patrones que se activan \
luego de las que si tienen patrones.

Entiendo los siguientes comandos:

• /start  Muestro esta ayuda.
• /help  Muestro esta ayuda.
• /menu  Despliego el menú de configuración (se usa en privado).

Todas las configuraciones se realizan con el comando /menu el cual va guiando paso \
a paso. Puedes establecer {CFG.LIMIT_RULE_NUMBER} reglas por grupo o canal, los \
nombres deber tener no mas de {CFG.LIMIT_NAME_LENGTH} caracteres (se cortan si son \
mas largos).

Mas información en https://gitlab.com/schcriher/indexiusbot

<i>By @\N{ZERO WIDTH NO-BREAK SPACE}Schcriher</i>
'''

ERROR_HAPPENED = ('Ha ocurrido un error intentando responderte 😭 '
                  'mi desarrollador será notificado 😊 disculpe las molestias.')

_ERROR = ('Es posible que haya ocurrido un error en la conexión/sevidor o inició '
          f'esta configuración hace mucho tiempo (máximo {TIMEOUT_EDIT_CHAT})')

MEM_ERROR = f'⛔️ {_ERROR}, inicie nuevamente el proceso de configuración por favor.'

TEX_ERROR = f'Si está continuando una configuración debe empezar de nuevo. {_ERROR}.'

NAME_ERROR = ('El nombre debe empezar con letra y luego puede contener (además '
              'de letras) números y/o guión bajo, no puede contener espacios.')

GROUP_RULES_ERROR = 'Reglas con errores en el grupo "{}":\n{}'
CHANNEL_RULES_ERROR = 'Reglas con errores en el canal "{}":\n{}'

CANCELED = '⚠️ Configuración cancelada.'
COMPLETED = '✅ Operación finalizada.'
WRONG_DATA = '⛔ Dato incorrecto, reintente.'

ARG_INVALID = 'Argumento no válido'
ARG_VALID_GROUPS = 'Argumento válido solo para grupos'
ARG_VALID_CHANNELS = 'Argumento válido solo para canales'

PRIVATE_USE = 'Use este comando por privado, son varios pasos.'

USE_KEYBOARD = 'Utilice el teclado con las opciones posibles.'

CAN_CANCEL = 'ℹ Puede cancelar con /cancel.\n\n'

ITEM = '•\N{NO-BREAK SPACE}'
CURRENT = '\nActual: '
SELECTED = 'Seleccionado: '
PROCESSING = 'Procesando: '

NOTHING_NOTE = ('Recuerda que los canales no me avisan cuando soy agregado por lo '
                'que deberás publicar (y luego borrar si lo deseas) un mensaje de '
                'texto cualquiera para saber que estoy allí. Además la comprobación '
                'de permisos se "cachea" (recuerda) con lo que puedo tardar unos '
                f'minutos (máximo {CACHING_ADM_TIME}) en saber que ya eres admin '
                'si acabas de hacerlo.')

NOTHING_TO_DO = ('No eres admin de ningún grupo o canal donde yo esté. '
                 f'{NOTHING_NOTE}')

NOTHING_MORE = ('\n\nNo eres admin de ningún otro grupo o canal donde yo esté. '
                f'{NOTHING_NOTE}')

CHAT_EDITED = '•\N{NO-BREAK SPACE}{} está siendo editado por {}'

NOT_POSSIBLE = ('Mientras elegías el chat para configurar, {} lo empezó a editar, '
                'espera o elije otro grupo o canal, en cualquier caso debes '
                'reiniciar el proceso con /menu.')

NO_HAVE_RIGHTS = '⛔️ Ya no eres admin para aplicar las reglas que configuraste.'

RULE_NOT_AVAILABLE = ('⛔️ El chat o la regla ya no existe en mi base de datos, '
                      'si es un error escriba un mensaje en el canal o grupo '
                      'para que lo vuelva a incluir en mi base de datos y '
                      'vuelva a crear la regla que estaba editando')

CHAT_CONFIG = 'Elija el grupo o canal a configurar:'

TO_DO = 'Elija lo que desee realizar:'

CREATE_ACTION = '✒️ Agregar'
UPDATE_ACTION = '🔧 Modificar'
DELETE_ACTION = '🧹 Eliminar'
LIST_ACTION = '🔦 Listar'
TEST_ACTION = '🔨 Probar'

NO_OPTION = 'No'
YES_OPTION = 'Si'
OVERALL_OPTION = '🌐 Global'
UNCHANGED_OPTION = '♻️ Dejar igual'

MENUARGS_CLS_OPTION = '🚮 Eliminar todos'
MENUARGS_END_OPTION = '🆗 Terminar'

GET_OPTION = 'Todos los parámetros en la URL (FORM)'
POST_FORM_OPTION = 'Todos los parámetros en el body como FORM'
POST_JSON_OPTION = 'Todos los parámetros en el body como JSON'

NOT_SET = '-'
SET_GLOBAL = '- (Global)'
NOT_REQUIRED = '- (No necesario)'
NOT_ELECTED = '- (No elegidos)'
MULTILINE = '«multilinea»\n'

RULES = 'Reglas:\n'

NO_RULES = '⚠️ No hay reglas disponibles'

NAME_EXISTS = 'Ese nombre ya existe.'

NEW_RULE_NAME = 'Escriba un nombre para la nueva regla:'
RULE_NAME = 'Escriba el nombre para la regla:'

RULE_LIMIT = f'⛔️ Solo puede configurar {CFG.LIMIT_RULE_NUMBER} reglas.'

RULE_CONFIG = 'Elija una regla entre los existentes:'

# \xa0: NO-BREAK SPACE
RULE_INFO = ('•\xa0Nombre:\xa0\xa0<code>{}</code>\n'
             '•\xa0Patrón:\xa0\xa0<code>{}</code>\n'
             '•\xa0Grupos\xa0sin\xa0nombre:\xa0\xa0<code>{}</code>\n'
             '•\xa0Args.\xa0adicionales:\xa0\xa0<code>{}</code>\n'
             '•\xa0Webhook:\xa0\xa0<code>{}</code>\n'
             '•\xa0Formato:\xa0\xa0<code>{}</code>\n')

TEST_DATA = 'Escriba los datos para la prueba:'

REGEX_CODE = ('Escriba el patrón de búsqueda (regex):\n\n'
              '<i>Ejemplos de plantillas:</i>\n'
              '•\xa0<i>Espacio:</i>\xa0\xa0<code>  \\s+</code>\n'
              '•\xa0<i>Palabra:</i>\xa0\xa0<code>Ł [\\w./-]+</code>\n'
              '•\xa0<i>Número:</i>\xa0\xa0<code>Ø \\d+(?:[,.]\\d*)?|[,.]\\d+</code>\n')

REGEX_GROUP_ERROR = ('El patrón debe contener uno o mas "grupos capturables", '
                     'ejemplo: "(patron)" o "(?P<nombre>patron)"')

EDITED_VAR_MOD = ('El patrón contiene la variable "edited" que se usa para enviar '
                  'la condición de mensaje o post editado, ¿quiere modificarlo?')

LIST_NAME = ('Escriba el nombre del parámetro con el que se '
             'enviarán los "grupos sin nombre" del patrón:')

NAME_EXISTS_MOD = ('El nombre elegido coincide con uno de los nombres que '
                   'aparecen en el patrón, ¿quiere modificarlo?')

ARGS_NAME = 'Elija los parámetros adicionales:'

ARGS_OVERLAP_MOD = ('Algunos de los argumentos seleccionados ({}) coinciden con '
                    'los nombres usandos en el patrón y/o en los "grupos sin '
                    'nombre" del patrón, ¿quiere modificarlo?')

WEBHOOK = 'Escriba la url (webhook):'

QUERY_OVERLAP_MOD = ('En el "query" de la url aparecen nombres ({}) que ya están '
                     'en el patrón o en los "grupos sin nombre" del patrón '
                     '¿quiere modificarlo?')

SEND_TYPE = ('Elija la forma en la que se enviarán los datos al servidor. '
             'Ésta elección depende del API que vaya a utilizar como webhook.\n\n')

INCORRECT_USE_UNCHANGED = (f'Si la opción {UNCHANGED_OPTION} no aparece en el '
                           'teclado no puede usarla')

TEST_WEBHOOK_ERROR = ('Error intentanto conectar con el servidor (webhook):\n'
                      '<code>{}</code>\n\n')

TEST_SUMMARY = ('Datos encontrados en el texto de prueba:'
                '\n\n{}\n\n'
                'La llamada a la API quedaría:'
                '\n\nHTTP: {}\n URL: {}\nBODY: {}\n\n'
                '<i>Nota 1: Se respetan tanto el orden como las mayúsculas de '
                'los argumentos extraidos, aunque en el servidor donde se envíen '
                'pueden tener un comportamiento distinto, generalmente esto no '
                'presenta ningún inconveniente.</i>')

TEST_SUMMARY_NOTE2 = ('\n<i>Nota 2: Tenga en cuenta que los argumentos para '
                      'canales ({}) estarán vacios aquí ya que este es un chat '
                      'privado no un canal; esto no es un error, funcionará '
                      'correctamente en el canal.</i>')

TEST_NO_DATA_FOUND = ('No se encontraron datos en el mensaje de prueba que '
                      'coincidieran con el patrón de la regla elegida.')

# ---------------------------------------------------------------------------- #

SAY_TEXT = '<code>/say</code> id/title mensaje\nGrupos/Canales:\n{}'

NEWS_TEXT = '<code>/news</code> mensaje'
NEWS_ERROR = '[news] {}: {}'

IGNORE_TEXT = '<code>/ignore</code> id/username/title si/no'
IGNORE_STATUS = 'Ignorar {}: {}'
