# -*- coding: UTF-8 -*-
# Copyright (C) 2019 Schmidt Cristian Hernán

import re
import sys
import html
import time
import urllib
import socket
import logging
import argparse
import datetime
import functools
import traceback
import collections

from requests import Session, RequestException
from requests.adapters import HTTPAdapter

from telegram import TelegramError, Chat as TelegramChat
from telegram.ext import (Updater, Filters, ConversationHandler, run_async,
                          CommandHandler, MessageHandler, RegexHandler)
from telegram.utils.helpers import mention_html

from indexius.debug import flogger
from indexius.tools import (REGEX_LINE_RE, YES_RE, PATTERN_RE, QueryDict,
                            pattern_normalizer, get_limited_name, remove_diacritics,
                            get_arg, extract_errors_from_response)
from indexius.texts import (USAGE_PUBLIC, USAGE_PRIVATE, ERROR_HAPPENED, MEM_ERROR,
                            NAME_ERROR, GROUP_RULES_ERROR, CHANNEL_RULES_ERROR,
                            COMPLETED, CANCELED, WRONG_DATA, PRIVATE_USE, USE_KEYBOARD,
                            CAN_CANCEL, ITEM, CURRENT, SELECTED, PROCESSING,
                            NOTHING_TO_DO, NOTHING_MORE, NO_HAVE_RIGHTS, TEX_ERROR,
                            RULE_NOT_AVAILABLE, CHAT_CONFIG, CHAT_EDITED, NOT_POSSIBLE,
                            TO_DO, NAME_EXISTS, RULES, NO_RULES, NEW_RULE_NAME,
                            RULE_NAME, RULE_LIMIT, RULE_CONFIG, RULE_INFO, NOT_SET,
                            SET_GLOBAL, NOT_REQUIRED, NOT_ELECTED, MULTILINE, TEST_DATA,
                            REGEX_CODE, REGEX_GROUP_ERROR, EDITED_VAR_MOD, LIST_NAME,
                            NAME_EXISTS_MOD, ARGS_NAME, ARGS_OVERLAP_MOD, WEBHOOK,
                            QUERY_OVERLAP_MOD, SEND_TYPE, INCORRECT_USE_UNCHANGED,
                            TEST_WEBHOOK_ERROR, TEST_SUMMARY, TEST_SUMMARY_NOTE2,
                            TEST_NO_DATA_FOUND, SAY_TEXT, NEWS_TEXT, NEWS_ERROR,
                            IGNORE_TEXT, IGNORE_STATUS)
from indexius.config import CFG
from indexius.models import (RuleRegex, MenuStep, MenuAction, MenuArgs, SendType,
                             Keyboard, Regex)
from indexius.context import Contextualizer
from indexius.version import VERSION
from indexius.database import (Chat, Rule, exists_rule_name, get_rule, get_rules,
                               migrate_chat, get_query)

logger = logging.getLogger(__name__)
context = Contextualizer()

MESSAGE_ARGS_TRANSLATE = {
    'id': 'message_id',
    'author': 'author_signature',
    'website': 'connected_website',
}

MENU_STEP_ERRORS = {
    'CHAT': USE_KEYBOARD,
    'ACTION': USE_KEYBOARD,
    'LNAME': NAME_ERROR,
    'LCONF': USE_KEYBOARD,
    'ARGS': USE_KEYBOARD,
    'UCONF': USE_KEYBOARD,
    'SEND': USE_KEYBOARD,
}

SCHEME_PORT = {
    'http': 80,
    'https': 443,
}

NEEDLESS_KEYS = ('_sa_instance_state', 'created_at', 'updated_at', 'used_at',
                 'ignore', 'rules', 'chat_id', 'chat')

SPACE_SYM = '␠'

# ---------------------------------------------------------------------------- #
# Decorating functions


def check_memory_cache(func):
    @functools.wraps(func)
    def check_memory_cache_wrapper(ctx):
        # The cache is only a function of the user_id
        # because it is realised in private and one chat configuration at a time

        chats = ctx.cache[ctx.uid, 'chats']  # is choosing
        cid = ctx.cache[ctx.uid, 'chat', 'id']  # already chosen
        nick = ctx.cache.edited_chat(cid, timeout=CFG.TIMEOUT_EDIT_CHAT)  # timeouted

        if chats or nick == ctx.nick:
            ctx.cache[ctx.uid, 'last'] = time.time()
            return func(ctx)

        ctx.reply(text=MEM_ERROR, reply_markup=Keyboard.NULL)
        del ctx.cache[ctx.uid]
        return MenuStep.STOP
    return check_memory_cache_wrapper


def send_finished_operation(func):
    @functools.wraps(func)
    def send_finished_operation_wrapper(ctx):
        result = func(ctx)
        if result is MenuStep.STOP:
            ctx.reply(text=COMPLETED, reply_markup=Keyboard.NULL)
        return result
    return send_finished_operation_wrapper


# ---------------------------------------------------------------------------- #
# Commands and events


@flogger
def send_text(bot, update, text):
    chat_id = update.effective_chat.id
    bot.send_message(chat_id=chat_id,
                     text=text.strip(),
                     parse_mode='HTML',
                     disable_web_page_preview=True)


@flogger
@run_async
def start_private_handler(bot, update):
    return send_text(bot, update, USAGE_PRIVATE)


@flogger
@run_async
def help_private_handler(bot, update):
    return send_text(bot, update, USAGE_PRIVATE)


@flogger
@run_async
def start_public_handler(bot, update):
    return send_text(bot, update, USAGE_PUBLIC)


@flogger
@run_async
def help_public_handler(bot, update):
    return send_text(bot, update, USAGE_PUBLIC)


@flogger
@run_async
def menu_public_handler(bot, update):
    return send_text(bot, update, PRIVATE_USE)


@flogger
@run_async
def old_config_handler(bot, update):
    return send_text(bot, update, TEX_ERROR)


@flogger
@context
def migrate_handler(ctx):
    if ctx.tgc.type == ctx.tgc.GROUP:
        old_id = ctx.cid
        new_id = ctx.tgm.migrate_to_chat_id
        title = None
    else:
        old_id = ctx.tgm.migrate_from_chat_id
        new_id = ctx.cid
        title = ctx.tgc.title
    migrate_chat(ctx.dbs, old_id, new_id, title)
    logger.debug('Migration completed: %d to %d', old_id, new_id)


@flogger
@context
def empty_handler(_ctx):
    pass  # whatever needs to be update, will be updated in the context decorator


# ---------------------------------------------------------------------------- #
# Indexing functions


@functools.lru_cache(maxsize=CFG.REGEX_CACHE_NUMBER)
@flogger
def compile_regex(regex):
    if '\n' in regex:
        *aliases, pattern = regex.strip('\n').split('\n')

        for line in aliases:
            obj = REGEX_LINE_RE.search(line)
            if obj:
                alias = obj.group(1) or ' '
                value = obj.group(3)

                if '|' in value and not value.startswith('(?:'):
                    value = '(?:' + value + ')'

                pattern = pattern.replace(alias, value)

        regex = PATTERN_RE.sub(pattern_normalizer, pattern)

    return re.compile(regex, re.DOTALL)


@flogger
def get_regex_list_size(regex):
    obj = compile_regex(regex)
    if not obj.groups:
        raise ValueError(REGEX_GROUP_ERROR)
    return obj.groups - len(obj.groupindex)


@flogger
def get_fields_from_regex(regex):
    pattern = compile_regex(regex)
    return list(pattern.groupindex)


@flogger
def get_qdic_from_text(regex, lname, text):
    qdic = QueryDict()

    pattern = compile_regex(regex)
    obj = pattern.match(text)
    if obj:
        fields = {index: name for name, index in pattern.groupindex.items()}
        for index in range(1, pattern.groups + 1):
            qdic[fields.get(index, lname)] = obj.group(index)

    return qdic


@flogger
def get_qdic_from_update(args, update):
    qdic = QueryDict()

    for arg in Regex.arg_sep_re.split(args) if args else []:
        target, attribute = arg.split('_', 1)

        if target == 'chat':
            obj = update.effective_chat
        elif target == 'user':
            obj = update.effective_user
        else:
            obj = update.effective_message
            attribute = MESSAGE_ARGS_TRANSLATE.get(attribute, attribute)

        qdic[arg] = getattr(obj, attribute)

    return qdic


@flogger
def check_server_connection(url):
    scheme, host, *_ = urllib.parse.urlsplit(url)

    _port = SCHEME_PORT.get(scheme)
    port = None
    sep = host.rfind(':')
    if sep > -1:
        # From http.client.HTTPConnection._get_hostport
        if sep > host.rfind(']'):           # ipv6 addresses have [...]
            try:
                port = int(host[sep+1:])
            except ValueError:
                if host[sep+1:] == '':      # http://foo.com:/ == http://foo.com/
                    port = _port
                else:
                    return 'Invalid URL'
            host = host[:sep]
        else:
            port = _port
        if host and host[0] == '[' and host[-1] == ']':
            host = host[1:-1]

    if not port:
        if _port:
            port = _port
        else:
            return 'Invalid URL'

    try:
        sock = socket.create_connection((host, port), timeout=CFG.URL_TIMEOUT)
        sock.close()
        return None
    except socket.error as error:
        return str(error)


#@flogger
def call_webhook(data):
    scheme_host = '{}://{}/*'.format(*urllib.parse.urlsplit(data['url']))
    tini = time.time()
    session = None
    try:
        session = Session()
        session.mount(data['url'], HTTPAdapter(max_retries=CFG.URL_RETRIES))
        response = session.request(**data)
        logger.debug('%s %s %s %s', response.status_code, response.reason,
                     data['method'], scheme_host)
        response.raise_for_status()
        error = extract_errors_from_response(response)
        return error or ''

    except RequestException as exc:
        # pylint: disable=no-member

        if hasattr(exc, 'response') and exc.response is not None:
            error = extract_errors_from_response(exc.response)
            error = f': {error}' if error else ''
            error = f'{exc.response.status_code} {exc.response.reason}{error}'

        elif exc.args and hasattr(exc.args[0], 'reason'):
            error = str(exc.args[0].reason).split(':', 1)[-1].strip()

        else:
            error = str(exc)

        logger.debug('%s %s → %s', data['method'], scheme_host, error)
        return error

    finally:
        logger.debug('Call to webhook took %.3f seconds', time.time() - tini)
        if session:
            session.close()


#@flogger
def make_webhook(method, url, varname, qdic_regex_args):
    scheme, host, path, query, fragment = urllib.parse.urlsplit(url)

    qdic = QueryDict()
    # First all parameters that were already in the url
    qdic.update(urllib.parse.parse_qsl(query))
    # Then all the parameters found in the text and additional parameters
    qdic.update(qdic_regex_args)

    data = {
        'method': method,
        'url': urllib.parse.urlunsplit((scheme, host, path, '', fragment)),
        varname: qdic,
        'timeout': CFG.URL_TIMEOUT,
        'headers': {'User-Agent': CFG.user_agent},
    }

    return data


@flogger
def dispatch_webhook(rule, update, chat, qdic, errores):
    qdic.update(get_qdic_from_update(rule.args, update))
    data = make_webhook(rule.send_type.method, rule.url, rule.send_type.var, qdic)
    error = call_webhook(data)
    if error:
        rule.attempts += 1
        errores[rule.user_id][chat].append(error)
    else:
        rule.used_at = datetime.datetime.utcnow()
        rule.attempts = 0
        rule.interacts += 1


@flogger
@context
def indexer_handler(ctx):
    match = False

    edited = bool(ctx.update.edited_message or
                  ctx.update.edited_channel_post or
                  ctx.update.effective_message.edit_date)

    errores = collections.defaultdict(lambda: collections.defaultdict(list))

    # Rule
    for rule in get_rules(ctx.dbs, ctx.cid, RuleRegex.ONLY_SET):
        qdic = get_qdic_from_text(rule.regex, rule.lname, ctx.text)
        if qdic:
            match = True
            qdic['edited'] = edited
            dispatch_webhook(rule, ctx.update, ctx.chat, qdic, errores)

    # Global
    if match:
        time.sleep(CFG.RELAX_TIME)

        for rule in get_rules(ctx.dbs, ctx.cid, RuleRegex.ONLY_NULL):
            qdic = QueryDict({'edited': edited})
            dispatch_webhook(rule, ctx.update, ctx.chat, qdic, errores)

    # Errors
    for user_id, data in errores.items():
        for chat, error_list in data.items():
            text = (CHANNEL_RULES_ERROR if chat.type == TelegramChat.CHANNEL else
                    GROUP_RULES_ERROR).format(chat.title_html,
                                              html.escape('\n'.join(error_list)))
            try:
                ctx.send(chat_id=user_id, text=text, throw_exc=True)
            except TelegramError as exc:
                logger.error('Can not notified user_id=%d: %s\n%s',
                             user_id, exc, html.unescape(text))


# ---------------------------------------------------------------------------- #
# Configuration system (private chat)


@flogger
def row_to_dict(row):
    dic = row.__dict__.copy()

    # Unnecessary keys for the configuration menu
    for key in NEEDLESS_KEYS:
        dic.pop(key, None)

    # For the Rule table
    if 'args' in dic:
        args = dic.pop('args')
        dic['args'] = Regex.arg_sep_re.split(args) if args else []

    return dic


@flogger
def show_regex_html(regex):
    if regex:
        if '\n' in regex:
            regex = MULTILINE + regex.replace(' ', SPACE_SYM)
    else:
        regex = SET_GLOBAL
    return html.escape(regex)


@flogger
@context
def menu_handler(ctx):
    ctx.cache[ctx.uid] = {'nick': ctx.nick, 'last': time.time()}  # clean start

    edited = []
    chats = {}
    for chat in ctx.dbs.query(Chat).all():
        if ctx.is_admin(chat.id, ctx.uid):
            nick = ctx.cache.edited_chat(chat.id, timeout=CFG.TIMEOUT_EDIT_CHAT)
            title = chat.title_html.strip()
            if nick:
                edited.append(CHAT_EDITED.format(title, nick))
            else:
                chats[f'{len(chats)+1}{ITEM}{title}'] = row_to_dict(chat)

    edited = '\n'.join(edited)

    if chats:
        ctx.cache[ctx.uid, 'chats'] = chats

        if edited:
            edited += '\n\n'

        if len(chats) > 1:
            ctx.reply(text=f'{edited}{CAN_CANCEL}{CHAT_CONFIG}',
                      reply_markup=Keyboard.get(chats.keys()))
            return MenuStep.CHAT

        ctx.text = list(chats)[0]
        title = f'<code>{ctx.text.split(ITEM, 1)[1]}</code>'
        select = f'{edited}{CAN_CANCEL}{PROCESSING}{title}\n'
        ctx.cache[ctx.uid, 'select'] = select
        return chat_handler(ctx)

    if edited:
        edited += NOTHING_MORE

    ctx.reply(text=edited or NOTHING_TO_DO, reply_markup=Keyboard.NULL)
    return MenuStep.STOP


@flogger
@context
@check_memory_cache
def chat_handler(ctx):
    chat = ctx.cache[ctx.uid, 'chats', ctx.text]
    if chat:
        nick = ctx.cache.edited_chat(chat['id'], ctx.uid, ctx.nick)
        if nick is not True:
            del ctx.cache[ctx.uid]
            ctx.reply(text=NOT_POSSIBLE.format(nick), reply_markup=Keyboard.NULL)
            return MenuStep.STOP

        del ctx.cache[ctx.uid, 'chats']
        ctx.cache[ctx.uid, 'chat'] = chat

        select = ctx.cache.pop(ctx.uid, 'select') or ''
        ctx.reply(text=f'{select}{TO_DO}', reply_markup=Keyboard.ACTION)
        return MenuStep.ACTION

    return wrong_handler_ctx(ctx, USE_KEYBOARD)


@flogger
@context
@check_memory_cache
def action_handler(ctx):
    # It is not necessary to check the validity of 'ctx.text' because
    # the menu manager checks it, the possible options are in Regex.ACTION
    action = MenuAction(ctx.text)
    ctx.cache[ctx.uid, 'action'] = action
    chat_id = ctx.cache[ctx.uid, 'chat', 'id']

    text = NO_RULES

    if action is MenuAction.CREATE:
        if get_rules(ctx.dbs, chat_id, count=True) < CFG.LIMIT_RULE_NUMBER:
            ctx.reply(text=NEW_RULE_NAME, reply_markup=Keyboard.NULL)
            return MenuStep.NAME

        text = RULE_LIMIT

    elif action in (MenuAction.UPDATE, MenuAction.DELETE, MenuAction.TEST):
        which = RuleRegex.ONLY_SET if action is MenuAction.TEST else RuleRegex.ALL
        rules = {}
        for rule in get_rules(ctx.dbs, chat_id, which):
            rules[f'{len(rules)+1}{ITEM}{rule.name_html}'] = row_to_dict(rule)

        if rules:
            ctx.cache[ctx.uid, 'rules'] = rules

            if len(rules) > 1:
                ctx.reply(text=RULE_CONFIG, reply_markup=Keyboard.get(rules.keys()))
                return MenuStep.NAME

            ctx.text = list(rules)[0]
            select = f'{PROCESSING}<code>{ctx.text.split(ITEM, 1)[1]}</code>\n'
            ctx.cache[ctx.uid, 'select'] = select
            return name_handler(ctx)

    elif action is MenuAction.LIST:
        info = []
        for rule in get_rules(ctx.dbs, chat_id):
            regex = show_regex_html(rule.regex)
            lname = html.escape(rule.lname or NOT_REQUIRED)
            args = html.escape(rule.args or NOT_ELECTED)
            send = html.escape(rule.send)
            info.append(RULE_INFO.format(rule.name_html, regex, lname, args,
                                         rule.url_html, send))
        if info:
            text = '\n'.join([RULES] + info).strip()

    ctx.reply(text=text, reply_markup=Keyboard.NULL)
    del ctx.cache[ctx.uid]
    return MenuStep.STOP


@flogger
@context
@check_memory_cache
@send_finished_operation
def name_handler(ctx):
    name = ctx.text
    select = ctx.cache.pop(ctx.uid, 'select') or ''
    action = ctx.cache[ctx.uid, 'action']
    mod = ctx.cache.pop(ctx.uid, 'mod')

    if action is MenuAction.CREATE:
        name = get_limited_name(name)
        if exists_rule_name(ctx.dbs, name, ctx.cache[ctx.uid, 'chat', 'id']):
            return wrong_handler_ctx(ctx, NAME_EXISTS)

        ctx.reply(text=f'{select}{REGEX_CODE}',
                  reply_markup=Keyboard.REGEX_CREATE)
        ctx.cache[ctx.uid, 'rule', 'name'] = name
        return MenuStep.REGEX

    if mod:
        # This happens when name_handler with UPDATE is called for the first time
        if name not in (Keyboard.unchanged, ctx.cache[ctx.uid, 'rule', 'name']):
            name = get_limited_name(name)
            if exists_rule_name(ctx.dbs, name, ctx.cache[ctx.uid, 'chat', 'id']):
                ctx.cache[ctx.uid, 'mod'] = True  # was poped from the cache
                return wrong_handler_ctx(ctx, NAME_EXISTS)
            ctx.cache[ctx.uid, 'rule', 'name'] = name
    else:
        rule = ctx.cache[ctx.uid, 'rules', name]
        if not rule:
            # User can send incorrect text
            return wrong_handler_ctx(ctx, USE_KEYBOARD)

        del ctx.cache[ctx.uid, 'rules']
        ctx.cache[ctx.uid, 'rule'] = rule

    if action is MenuAction.UPDATE:
        if not mod:
            ctx.reply(text=(f'{select}{RULE_NAME}{CURRENT}{html.escape(rule["name"])}'),
                      reply_markup=Keyboard.NAME_UPDATE)
            # Once the rule has been chosen by name, it is necessary
            # to give the possibility to edit the name first
            ctx.cache[ctx.uid, 'mod'] = True
            return MenuStep.NAME

        return go_regex_menu(ctx)

    if action is MenuAction.TEST:
        ctx.reply(text=f'{select}{TEST_DATA}', reply_markup=Keyboard.NULL)
        return MenuStep.TEST

    # action is MenuAction.DELETE
    if select:
        ctx.reply(text=select.strip(), reply_markup=Keyboard.NULL)
        time.sleep(CFG.RELAX_TIME)  # prevents 'finished operation' is sent earlier
    rule = get_rule(ctx.dbs, rule['id'])
    ctx.dbs.delete(rule)
    del ctx.cache[ctx.uid]
    return MenuStep.STOP


@flogger
def go_regex_menu(ctx):
    regex = ctx.cache[ctx.uid, 'rule', 'regex']
    text = f'{REGEX_CODE}{CURRENT}{show_regex_html(regex)}'

    # It will always be Keyboard.REGEX_UPDATE, because in case of
    # MenuAction.CREATE the function is called with mod activated
    ctx.reply(text=text, reply_markup=Keyboard.REGEX_UPDATE)

    lsize = get_regex_list_size(regex) if regex else 0
    ctx.cache[ctx.uid, 'rule', 'lsize'] = lsize
    return MenuStep.REGEX


@flogger
@context
@check_memory_cache
def regex_handler(ctx):
    regex = ctx.text
    new = ctx.cache[ctx.uid, 'action'] is MenuAction.CREATE
    mod = ctx.cache.pop(ctx.uid, 'mod')

    if regex == Keyboard.overall:
        ctx.cache[ctx.uid, 'rule', 'regex'] = regex = None
        ctx.cache[ctx.uid, 'rule', 'lsize'] = 0

    elif regex == Keyboard.unchanged and (mod or not new):
        regex = ctx.cache[ctx.uid, 'rule', 'regex']

    else:
        try:
            regex = regex.replace(SPACE_SYM, ' ')
            lsize = get_regex_list_size(regex)

        except (ValueError, re.error) as exc:
            return wrong_handler_ctx(ctx, str(exc))

        ctx.cache[ctx.uid, 'rule', 'regex'] = regex
        ctx.cache[ctx.uid, 'rule', 'lsize'] = lsize

    fields = get_fields_from_regex(regex) if regex else []
    edited_already_exists = 'edited' in fields

    fields.append('edited')  # 'edited' can be added when calling indexer_handler
    ctx.cache[ctx.uid, 'rule', 'fields'] = fields

    if edited_already_exists:
        ctx.reply(text=EDITED_VAR_MOD, reply_markup=Keyboard.RCONF)
        return MenuStep.RCONF

    return go_lname_or_args_menu(ctx)


@flogger
@context
@check_memory_cache
def rconf_handler(ctx):
    rconf = ctx.text

    if rconf == Keyboard.yes:
        ctx.cache[ctx.uid, 'mod'] = True
        return go_regex_menu(ctx)

    return go_lname_or_args_menu(ctx)


@flogger
def go_lname_or_args_menu(ctx):

    if ctx.cache[ctx.uid, 'rule', 'lsize'] > 0:
        return go_lname_menu(ctx)

    ctx.cache[ctx.uid, 'rule', 'lname'] = None
    return go_args_menu(ctx)


@flogger
def go_lname_menu(ctx):
    new = ctx.cache[ctx.uid, 'action'] is MenuAction.CREATE
    text = LIST_NAME

    if new:
        keyboard = Keyboard.NULL
    else:
        lname = ctx.cache[ctx.uid, 'rule', 'lname']
        text = f'{text}{CURRENT}{html.escape(lname or NOT_SET)}'
        keyboard = Keyboard.LNAME_UPDATE

    ctx.reply(text=text, reply_markup=keyboard)
    return MenuStep.LNAME


@flogger
@context
@check_memory_cache
def lname_handler(ctx):
    lname = ctx.text
    new = ctx.cache[ctx.uid, 'action'] is MenuAction.CREATE

    if lname == Keyboard.unchanged and not new:
        lname = ctx.cache[ctx.uid, 'rule', 'lname']
    else:
        lname = get_limited_name(lname)
        ctx.cache[ctx.uid, 'rule', 'lname'] = lname

    fields = ctx.cache[ctx.uid, 'rule', 'fields']
    lname_already_exists = lname in fields
    fields.append(lname)

    if lname_already_exists:
        ctx.reply(text=NAME_EXISTS_MOD, reply_markup=Keyboard.LCONF)
        return MenuStep.LCONF

    return go_args_menu(ctx)


@flogger
@context
@check_memory_cache
def lconf_handler(ctx):
    lconf = ctx.text

    if lconf == Keyboard.yes:
        ctx.cache[ctx.uid, 'mod'] = True
        return go_regex_menu(ctx)

    return go_args_menu(ctx)


@flogger
def go_args_menu(ctx):
    new = ctx.cache[ctx.uid, 'action'] is MenuAction.CREATE
    ctyp = ctx.cache[ctx.uid, 'chat', 'type']
    text = ARGS_NAME

    if new:
        args = None
        ctx.cache[ctx.uid, 'rule', 'args'] = []
    else:
        args = ctx.cache[ctx.uid, 'rule', 'args']
        args_text = Regex.arg_sep.join(args) if args else NOT_SET
        text = f'{text}{CURRENT}{html.escape(args_text)}'
        ctx.cache[ctx.uid, 'rule', '_args'] = args[:]  # copy

    ctx.reply(text=text, reply_markup=Keyboard.get_args(ctyp, new, args))
    return MenuStep.ARGS


@flogger
@context
@check_memory_cache
def args_handler(ctx):
    arg = ctx.text
    new = ctx.cache[ctx.uid, 'action'] is MenuAction.CREATE
    ctyp = ctx.cache[ctx.uid, 'chat', 'type']

    if arg == Keyboard.unchanged and not new:
        ctx.cache[ctx.uid, 'rule', 'args'] = ctx.cache[ctx.uid, 'rule', '_args']
        arg = MenuArgs.END
    else:
        arg, error = MenuArgs.get(arg, ctyp)
        if error:
            return wrong_handler_ctx(ctx, f'{error}. {USE_KEYBOARD}')

    args = ctx.cache[ctx.uid, 'rule', 'args']

    if arg is MenuArgs.END:
        del ctx.cache[ctx.uid, 'rule', '_args']

        fields = ctx.cache[ctx.uid, 'rule', 'fields']
        overlap = ', '.join(a for a in args if a in fields)

        for arg in args:
            fields.append(arg)

        if overlap:
            text = ARGS_OVERLAP_MOD.format(overlap)
            ctx.reply(text=text, reply_markup=Keyboard.ACONF)
            return MenuStep.ACONF

        return go_url_menu(ctx)

    if arg is MenuArgs.CLS:
        args.clear()

    elif arg.value not in args:
        args.append(arg.value)

    text = f'{SELECTED}{Regex.arg_sep.join(args)}'
    ctx.reply(text=text, reply_markup=Keyboard.get_args(ctyp, new, args))

    return None  # to continue in the same menu for accept more parameters


@flogger
@context
@check_memory_cache
def aconf_handler(ctx):
    aconf = ctx.text

    if aconf == Keyboard.yes:
        ctx.cache[ctx.uid, 'mod'] = True
        return go_regex_menu(ctx)

    return go_url_menu(ctx)


@flogger
def go_url_menu(ctx):
    new = ctx.cache[ctx.uid, 'action'] is MenuAction.CREATE
    mod = ctx.cache[ctx.uid, 'mod']
    text = WEBHOOK

    if new and not mod:
        keyboard = Keyboard.NULL
    else:
        url = ctx.cache[ctx.uid, 'rule', 'url']
        text = f'{text}{CURRENT}{html.escape(url)}'
        keyboard = Keyboard.URL_UPDATE

    ctx.reply(text=text, reply_markup=keyboard)
    return MenuStep.URL


@flogger
@context
@check_memory_cache
def url_handler(ctx):
    url = ctx.text
    new = ctx.cache[ctx.uid, 'action'] is MenuAction.CREATE
    mod = ctx.cache.pop(ctx.uid, 'mod')

    if url == Keyboard.unchanged and (not new or mod):
        url = ctx.cache[ctx.uid, 'rule', 'url']
    else:
        try:
            # May take a few seconds
            ctx.set_typing()

            url = urllib.parse.urlunsplit(urllib.parse.urlsplit(url, 'http'))

            error = check_server_connection(url)
            if error:
                raise ValueError(error)

        except ValueError as exc:
            return wrong_handler_ctx(ctx, str(exc))

        ctx.cache[ctx.uid, 'rule', 'url'] = url

    fields = ctx.cache[ctx.uid, 'rule', 'fields']

    parts = urllib.parse.urlsplit(url)
    qlist = urllib.parse.parse_qs(parts.query)
    overlap = ', '.join(q for q in qlist if q in fields)

    for arg in qlist:
        fields.append(arg)

    if overlap:
        ctx.reply(text=QUERY_OVERLAP_MOD.format(overlap),
                  reply_markup=Keyboard.UCONF)
        return MenuStep.UCONF

    return go_send_menu(ctx)


@flogger
@context
@check_memory_cache
def uconf_handler(ctx):
    uconf = ctx.text

    if uconf == Keyboard.yes:
        ctx.cache[ctx.uid, 'mod'] = True
        return go_regex_menu(ctx)

    return go_send_menu(ctx)


@flogger
def go_send_menu(ctx):
    new = ctx.cache[ctx.uid, 'action'] is MenuAction.CREATE
    text = SEND_TYPE + SendType.full_list()

    if new:
        keyboard = Keyboard.SEND_CREATE
    else:
        send = ctx.cache[ctx.uid, 'rule', 'send']
        text = f'{text}{CURRENT}{html.escape(send)}'
        keyboard = Keyboard.SEND_UPDATE

    ctx.reply(text=text, reply_markup=keyboard)
    return MenuStep.SEND


@flogger
@context
@check_memory_cache
def send_handler(ctx):
    send = ctx.text
    new = ctx.cache[ctx.uid, 'action'] is MenuAction.CREATE

    if send == Keyboard.unchanged:
        if new:
            return wrong_handler_ctx(ctx, INCORRECT_USE_UNCHANGED)
    else:
        # It is not necessary to check the validity of 'ctx.text' (send) because
        # the menu manager checks it, the possible options are in Regex.SEND
        ctx.cache[ctx.uid, 'rule', 'send'] = send

    return try_save_rule(ctx)


@flogger
def try_save_rule(ctx):
    # Separates into two functions (try_save_rule and save_rule) to be able
    # to return MenuStep.STOP without the "operation completed" message
    # sent by @send_finished_operation

    new = ctx.cache[ctx.uid, 'action'] is MenuAction.CREATE

    if not ctx.is_admin(ctx.cache[ctx.uid, 'chat', 'id'], ctx.uid):
        ctx.reply(text=NO_HAVE_RIGHTS, reply_markup=Keyboard.NULL)
        del ctx.cache[ctx.uid]
        return MenuStep.STOP

    if not new:
        rule = get_rule(ctx.dbs, ctx.cache[ctx.uid, 'rule', 'id'])
        if not rule:
            ctx.reply(text=RULE_NOT_AVAILABLE, reply_markup=Keyboard.NULL)
            del ctx.cache[ctx.uid]
            return MenuStep.STOP

        ctx.cache[ctx.uid, 'db_rule'] = rule

    return save_rule(ctx)


@flogger
@send_finished_operation
def save_rule(ctx):
    # And finally the data is saved in the database

    rule = ctx.cache[ctx.uid, 'db_rule']
    data = ctx.cache[ctx.uid, 'rule']
    name = data['name']
    url = data['url']

    if rule:
        rule.name = name
        rule.url = url
        rule.user_id = ctx.uid
    else:
        chat_id = ctx.cache[ctx.uid, 'chat', 'id']
        rule = Rule(name=name, chat_id=chat_id, url=url, user_id=ctx.uid)
        ctx.dbs.add(rule)

    rule.regex = data['regex']
    rule.lname = data.get('lname') or None
    rule.args = Regex.arg_sep.join(data.get('args', [])) or None
    rule.send = data['send']
    rule.attempts = 0  # reset
    rule.updated_at = datetime.datetime.utcnow()

    del ctx.cache[ctx.uid]
    return MenuStep.STOP


@flogger
@context
def test_handler(ctx):
    rule = get_rule(ctx.dbs, ctx.cache[ctx.uid, 'rule', 'id'])
    text = []

    ctx.set_typing()  # may take a few seconds

    qdic1 = get_qdic_from_text(rule.regex, rule.lname, ctx.text)
    if qdic1:

        error = check_server_connection(rule.url)
        if error:
            text.append(TEST_WEBHOOK_ERROR.format(error))
        else:
            rule.attempts = 0

        *scheme_host_path, query, fragment = urllib.parse.urlsplit(rule.url)

        qdic2 = QueryDict()
        # First all parameters that were already in the url
        qdic2.update(urllib.parse.parse_qsl(query))
        # ... then all the parameters found in the text
        qdic2.update(qdic1)
        # ... and additional parameters
        qdic2.update(get_qdic_from_update(rule.args, ctx.update))

        if rule.send == 'POST_JSON':
            query = ''
            body = qdic2
        else:
            _qs = urllib.parse.urlencode(qdic2, doseq=True)
            query, body = ('', _qs) if rule.send == 'POST_FORM' else (_qs, '')

        data = '\n'.join(f'<code>{html.escape(k)}: {html.escape(str(v))}</code>'
                         for k, v in qdic1.items())

        url = urllib.parse.urlunsplit((*scheme_host_path, query, fragment))

        text.append(TEST_SUMMARY.format(data, rule.send_type.method,
                                        html.escape(url), html.escape(str(body))))

        args = rule.args or []
        args_ch = [arg for arg in MenuArgs.get_only_channels() if arg in args]
        if args_ch:
            text.append(TEST_SUMMARY_NOTE2.format(', '.join(args_ch)))
    else:
        text.append(TEST_NO_DATA_FOUND)

    ctx.reply(text='\n'.join(text))
    del ctx.cache[ctx.uid]
    return MenuStep.STOP


@flogger
@context
def stop_handler(ctx):
    del ctx.cache[ctx.uid]
    ctx.reply(text=CANCELED, reply_markup=Keyboard.NULL)
    return MenuStep.STOP


@flogger
def wrong_handler_ctx(ctx, message):
    return wrong_handler(ctx.bot, ctx.update, message)


@flogger
def wrong_handler(bot, update, message=None):
    chat_id = update.effective_chat.id
    text = WRONG_DATA + (f'\n<i>{html.escape(message)}</i>' if message else '')
    bot.send_message(chat_id=chat_id, text=text, parse_mode='HTML')


# ---------------------------------------------------------------------------- #
# Administrative commands


@flogger
@context
def say_handler(ctx):
    arg = get_arg(ctx.text)
    if arg:
        chat, text = get_query(arg, ctx.dbs.query(Chat), 'title')
        if chat and text:
            ctx.send(chat_id=chat.id, text=text)
            return
    text = SAY_TEXT.format('\n'.join(f' <code>{chat.id}</code> ─ {chat.title_html}'
                                     for chat in ctx.dbs.query(Chat).all()))
    ctx.reply(text=text)


@flogger
@context
def news_handler(ctx):
    arg = get_arg(ctx.text)
    if arg:
        for chat in ctx.dbs.query(Chat).order_by(Chat.title.asc()).all():
            try:
                ctx.send(chat_id=chat.id, text=arg, throw_exc=True)
            except TelegramError as exc:
                error = str(exc)
                logger.error(error)
                ctx.reply(text=NEWS_ERROR.format(chat.title_html, html.escape(error)))
            time.sleep(CFG.RELAX_TIME)
    else:
        ctx.reply(text=NEWS_TEXT)


@flogger
@context
def ignore_handler(ctx):
    arg = get_arg(ctx.text)
    if arg:
        chat, text = get_query(arg, ctx.dbs.query(Chat), 'title')
        if chat:
            if text and chat.id not in (CFG.fid, CFG.bid):
                chat.ignore = bool(YES_RE.match(remove_diacritics(text)))
                ctx.reply(text=IGNORE_STATUS.format(chat.id, chat.ignore))
                return
    ctx.reply(text=IGNORE_TEXT)


@flogger
@context
def debug_handler(ctx):
    # pylint: disable=protected-access
    from pprint import pformat

    texts = []
    for model in (Chat, Rule):
        rows = '\n'.join(f'• {str(row)}' for row in ctx.dbs.query(model).all())
        if rows:
            texts.append(rows)

    logger.debug('DEBUGGING:\n\nMEM:\n%s\n\nDB:\n%s\n',
                 pformat(ctx.cache._MemoryCache__data),
                 '\n\n'.join(texts))


# ---------------------------------------------------------------------------- #
# Startup functions


def error_handler(bot, update, error):
    logger.critical(error)

    user = update.effective_user
    chat = update.effective_chat
    message = update.effective_message

    if message:
        message.reply_text(text=ERROR_HAPPENED)

    data = []

    if user:
        data.append(f'with the user {mention_html(user.id, user.name or user.id)}')

    if chat:
        name = chat.title or chat.first_name or chat.id
        data.append(f'within the chat <i>{html.escape(name)}</i>')
        if chat.username:
            data.append(f'(@{chat.username})')

    head_text = f'The error <code>{error}</code> happened {" ".join(data)}. '
    trace_text = 'The full traceback:\n\n<code>{}</code>'

    trace = traceback.format_tb(sys.exc_info()[2])
    size = len(head_text) + len(trace_text)

    # To avoid exceeding the limit of 4096 characters per message
    for num in range(0, len(trace), 2):
        note = [f'    ...{num} lines were cut...\n'] if num else []
        _trace = ''.join(trace[:2] + note + trace[2 + num:])
        if size + len(_trace) <= 4098:  # +2 by the characters '{}' in trace_text
            trace_text = trace_text.format(_trace)
            break

    bot.send_message(chat_id=CFG.fid, text=head_text + trace_text, parse_mode='HTML')


def get_states_handlers():
    cancel = CommandHandler('cancel', stop_handler, Filters.private)
    states = {}
    for step in MenuStep:
        if step is not MenuStep.STOP:
            # Handler for correct values
            pattern = getattr(Regex, step.name, Regex.default)
            rhandler = globals()['{}_handler'.format(step.name.lower())]
            right = RegexHandler(pattern, rhandler)
            # Handler for incorrect values
            try:
                message = MENU_STEP_ERRORS[step.name]
                whandler = functools.partial(wrong_handler, message=message)
            except KeyError:
                whandler = wrong_handler
            wrong = MessageHandler(Filters.all, whandler)
            # Set handlers
            states[step.value] = [cancel, right, wrong]
    return states


def add_handlers(dispatcher):
    only_father = Filters.user(CFG.fid) & Filters.private

    command_handlers = (
        # pylint: disable=bad-whitespace
        ('start',  Filters.private, start_private_handler),
        ('help',   Filters.private, help_private_handler),
        #
        ('start',  ~Filters.private, start_public_handler),
        ('help',   ~Filters.private, help_public_handler),
        ('menu',   ~Filters.private, menu_public_handler),
        #
        ('say',    only_father, say_handler),
        ('news',   only_father, news_handler),
        ('ignore', only_father, ignore_handler),
        ('debug',  only_father, debug_handler),
    )
    for cmd, filters, handler in command_handlers:
        dispatcher.add_handler(CommandHandler(cmd, handler, filters))

    menu_handlers = ConversationHandler(
        entry_points=[CommandHandler('menu', menu_handler, Filters.private)],
        states=get_states_handlers(),
        fallbacks=[CommandHandler('cancel', stop_handler, Filters.private)]
    )
    dispatcher.add_handler(menu_handlers)

    message_handlers = (
        # "left_chat_member" is not necessary: chat are checked in the menu
        (Filters.status_update.new_chat_members, empty_handler),
        (Filters.status_update.new_chat_title, empty_handler),
        (Filters.status_update.migrate, migrate_handler),
        (Filters.text & ~Filters.private, indexer_handler),
        (Filters.text & Filters.private, old_config_handler),
    )
    for filters, func in message_handlers:
        dispatcher.add_handler(MessageHandler(filters, func, edited_updates=True))

    dispatcher.add_error_handler(error_handler)


def only_bot_logging():
    logger_names = (
        'urllib3.connectionpool',
        'JobQueue',
        'telegram.bot',
        'telegram.ext.updater',
        'telegram.ext.dispatcher',
        'telegram.ext.conversationhandler',
        'telegram.vendor.ptb_urllib3.urllib3.util.retry',
        'telegram.vendor.ptb_urllib3.urllib3.connectionpool',
    )
    for logger_name in logger_names:
        logging.getLogger(logger_name).setLevel(logging.ERROR)


def run():
    parser = argparse.ArgumentParser(
        description='The default operating mode is webhook',
        epilog='Verbose: 0 WARNING, 1 INFO, 2 DEBUG (only bot); 3 Full DEBUG')
    parser.add_argument(
        '-p', '--polling',
        help='start the bot in polling mode',
        action='store_true')
    parser.add_argument(
        '-c', '--clean',
        help='clean any pending updates',
        action='store_true')
    parser.add_argument(
        '-v', '--verbose',
        help='verbose level, repeat up to three times',
        action='count',
        default=0)
    args = parser.parse_args()

    levels = (logging.WARNING, logging.INFO, logging.DEBUG)
    level = levels[min(len(levels) - 1, args.verbose)]
    logging.basicConfig(level=logging.DEBUG,
                        format=CFG.log_format,
                        datefmt=CFG.DATETIME_FORMAT)
    logger.setLevel(level)

    if args.verbose < 3:
        only_bot_logging()

    logger.info('Initializing bot v%s...', VERSION)

    updater = Updater(CFG.TELEGRAM_TOKEN)
    add_handlers(updater.dispatcher)
    context.initialize()

    # Mode
    if args.polling:
        updater.start_polling(clean=args.clean)
        logger.debug('Start in polling mode, clean=%s', args.clean)
    else:
        # start_webhook: only set webhook if SSL is handled by library
        updater.bot.set_webhook(f'{CFG.HOST}/{CFG.BOT_PATH}')
        # cleaning updates is not supported if SSL-termination happens elsewhere
        updater.start_webhook(listen=CFG.BIND, port=CFG.PORT, url_path=CFG.BOT_PATH)
        logger.debug('Start in webhook mode')

    # Wait...
    updater.idle()

    # Stop.
    logger.debug('compile_regex: %s', compile_regex.cache_info())
    logger.info('Stopped Bot.')
    return 0
