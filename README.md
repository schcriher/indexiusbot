# IndexiusBot

🇦🇷🇪🇸 Bot para indexado de contenido en grupos y canales en telegram

🇬🇧🇺🇸 Bot for indexing content in groups and channels in telegram

Disponible como [@indexiusbot](https://t.me/indexiusbot)



## Funcionamiento

El bot tiene que ser agregado al canal o grupo donde se quiera tener un registro de la
actividad del mismo. Definiendo una o mas reglas, las cuales tendrán asociadas una URL
cada una, a las cuales se enviarán los datos capturados por el patrón de la regla en
cuestión (funcionamiento de [webhook][WEBHOOK]).



## Regla

Cada regla tiene:

* **Nombre:** único para ese grupo o canal para identificarla.
* **Patrón:** expresión regular para capturar los datos a ser enviado al webhook.
* **Nombre de Lista:** nombre de la lista de los "grupos sin nombre" del patrón.
* **Argumentos adicionales:** datos disponibles del grupo o canal, del mensaje
  y del usuario, para ser enviado al webhook si se desea.
* **Webhook:** URL donde se enviarán los datos capturados por el patrón mediante.
* **Forma de envío:** La petición al webhook puede realizarse por GET o POST.


### Patrón

Se utilizan las [expresiones regulares][RE] disponibles en Python en dos formas:

1. Patrón en una línea: será compilada por la regex de Python sin ninguna modificación.

2. Patrón en varias líneas: se considera como patrón la última línea y como plantillas
   las demás líneas a ser reemplazadas en el patrón. Por ejemplo:
```
     \s+
   Ø \d+(?:[,.]\d*)?|[,.]\d+
   Ł [\w./-]+
   (Ø) Ł (?P<code>Ł)
```
Donde los espacios son reemplazados por `\s+` (uno o mas espacios), se utiliza `Ø`
para representar cualquier tipo de número (enteros y flotantes) y `Ł` para representar
una palabra que incluye todas las letras, números y guión bajo (`\w`) además de punto,
la barra y guión medio.

El patrón en si dice: *«número»«espacio»«palabra»«espacio»«palabra»* estando el número
en un grupo capturable sin nombre y la segunda palabra en capturada bajo el nombre de
`code`.

Reemplazando las plantillas en el patrón quedaría:
```
   ((?:\d+(?:[,.]\d*)?|[,.]\d+))\s+[\w./-]+\s+(?P<code>[\w./-]+)
```

Si se prueba con un texto de ejemplo como `1.0 abc ok` y considerando un nombre de
lista `array` los datos capturados serían:
* `array=1.0`
* `code=ok`

Existe la posibilidad de definir reglas sin patrón (*Global*), estas reglas son
ejecutadas solo si alguna de las reglas con patrón es ejecutada. Útil para lanzar
acciones de compilación o despliegues luego de actualizar alguna base de datos con
los datos de los reglas con patrones.

El patrón se compila con la bandera [DOTALL][DOTALL] activada, para que el punto (`.`)
signifique cualquier cosa incluido los saltos de línea (que pueden haber en un mensaje).


### Nombre de Lista

En el patrón pueden haber grupos capturados con nombres [`(?P<name>...)`][RE17] y "grupos sin
nombre" [`(...)`][RE14], en este caso estos datos deben de tener un nombre de variable para
ser enviados al webhook y es este nombre el que se elije en este apartado.


### Argumentos adicionales

Se puede elegir, además de los datos capturados por el patrón de búsqueda, que se envíen
datos del canal o grupo, del mensaje analizado y/o del usuario que envió dicho mensaje,
entre estos se disponen (ver [API de telegram][API]):

* En todos los casos: `message_id`, `message_date`, `chat_id`, `chat_type`,
                      `chat_title`, `chat_username`

* Solo para canales: `post_author` (`author_signature`),
                     `post_website` (`connected_website`)

* Solo para grupos: `user_id`, `user_first_name`, `user_last_name`, `user_username`


### Webhook y Forma de envío

El webhook es la URL donde se enviarán los datos capturados (por el patrón y los
argumentos adicionales) mediante una petición GET o POST, se incluye además la variable
`edited` si el mensaje es editado. Si necesita agregar otros datos, como tokens, puede
hacerlo en la [query string][QS] de la URL, que serán enviados en la query string o
en el body de la petición según el modo elegido.

Modos de envío disponibles:

* `GET`: Petición [GET][GET] con todas las variables enviadas en el query string

* `POST_FORM`: Petición [POST][POST] con todas las variables enviadas en el body
               en formato [FORM][FORM]

* `POST_JSON`: Petición [POST][POST] con todas las variables enviadas en el body
               en formato [JSON][JSON]



## Webhook en hojas de cálculo de Google:
* http://railsrescue.com/blog/2015-05-28-step-by-step-setup-to-send-form-data-to-google-sheets/
* https://www.hackster.io/gusgonnet/pushing-data-to-google-docs-02f9c4



[API]: https://core.telegram.org/bots/api

[RE]: https://docs.python.org/3/library/re.html
[RE14]: https://docs.python.org/3/library/re.html#index-14
[RE17]: https://docs.python.org/3/library/re.html#index-17
[DOTALL]: https://docs.python.org/3/library/re.html#re.DOTALL

[QS]: https://en.wikipedia.org/wiki/Query_string
[GET]: https://en.wikipedia.org/wiki/GET_(HTTP)
[POST]: https://en.wikipedia.org/wiki/POST_(HTTP)
[FORM]: https://en.wikipedia.org/wiki/Percent-encoding
[JSON]: https://en.wikipedia.org/wiki/JSON
[WEBHOOK]: https://en.wikipedia.org/wiki/Webhook
