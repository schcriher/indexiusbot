# -*- coding: UTF-8 -*-
# Copyright (C) 2019 Schmidt Cristian Hernán

# pylint: disable=undefined-variable,too-many-lines

import re
import html
import time
import urllib
import functools

from telegram.ext import CommandHandler
from telegram.utils.helpers import mention_html

from environ import (logging, unittest, mock, skipIf, UNTESTED_BOT,
                     MOCKED_SESSION_REQUEST_HISTORY, mocked_connection, mocked_session,
                     BotIOAdapter, CFG, core, context, models, tools, texts, database)

from tgs import (DEVID, ADMID, G1ID, G2ID, G3ID, S1ID, C1ID, CHAT_HEAD, CHAT_DATA,
                 USERS, TelegramServer)

NOW = time.time()

DBChat = database.Chat
DBRule = database.Rule

GLOBALS = globals()

for _var in dir(texts):
    if _var.isupper():
        GLOBALS[_var] = getattr(texts, _var)

for _var in 'RuleRegex MenuStep MenuAction MenuArgs SendType Keyboard Regex'.split():
    GLOBALS[_var] = getattr(models, _var)

for _var in dir(Keyboard):
    if _var.isupper() and _var not in ('NULL', '_CONF'):
        GLOBALS[_var] = Keyboard.get_list(_var)

CHECK_DATABASE = {
    DBChat: 'id type title'.split(),
    DBRule: 'name regex lname args url send user_id chat_id'.split(),
}

CHECK_INDEXES = [CHAT_HEAD.index(var) for var in CHECK_DATABASE[DBChat]]
CHAT_TITLE_IDX = CHAT_HEAD.index('title')
for _row in CHAT_DATA:
    title = _row[CHAT_TITLE_IDX]
    GLOBALS[f'CHAT_{title}'] = tuple(_row[i] for i in CHECK_INDEXES)


# ---------------------------------


def get_args_text_and_kb(chat_type, new=True, args=''):
    args = [str(getattr(MenuArgs, arg)) for arg in args.split()]
    text = f'{SELECTED}{Regex.arg_sep.join(args)}'
    args_kb = Keyboard.get_list(Keyboard.get_args(chat_type, new, args))
    return text, args_kb


def get_args_kb(chat_type, new=True, args=''):
    return get_args_text_and_kb(chat_type, new, args)[1]


def get_rule_info(name, regex, lname, args, url, send):
    # pylint: disable=too-many-arguments
    name = html.escape(name)
    regex = core.show_regex_html(regex)
    lname = html.escape(lname or NOT_REQUIRED)
    args = html.escape(args or NOT_ELECTED)
    url = html.escape(url)
    send = html.escape(send)
    return RULE_INFO.format(name, regex, lname, args, url, send)


# ---------------------------------


def check_database(**kwargs):
    dbs = kwargs['core'].context.dbses()
    data = {}
    try:
        for model, variables in CHECK_DATABASE.items():
            name = model.__tablename__
            data[name] = []
            for row in dbs.query(model).all():
                for variable in variables:
                    data[name].append(getattr(row, variable))
    finally:
        dbs.close()
    kwargs['test'].assertEqual(kwargs['expected'], data)


CONSENT_CACHE = {}

def edit_consent(**kwargs):
    cid = kwargs['cid']
    tgs = kwargs['tgs']

    context.get_admin_ids.clear()
    context.all_members_are_administrators.clear()

    if 'value' in kwargs:
        CONSENT_CACHE.setdefault(cid, []).append(tgs.consent[cid])
        value = kwargs['value']
    else:
        value = CONSENT_CACHE[cid].pop()

    tgs.consent[cid] = value


EDIT_URL_RULE_CACHE = {}

def edit_url_rule(**kwargs):
    name = kwargs['name']
    url = kwargs.get('url')
    try:
        dbs = kwargs['core'].context.dbses()
        rule = dbs.query(DBRule).filter(DBRule.name == name).first()
        assert rule, f'no rule "{name}" in the database'

        if url:
            EDIT_URL_RULE_CACHE.setdefault(name, []).append(rule.url)
        else:
            url = EDIT_URL_RULE_CACHE[name].pop()

        rule.url = url
        dbs.commit()
    finally:
        dbs.close()


def clear_request(**_):
    MOCKED_SESSION_REQUEST_HISTORY.clear()


def check_request(**kwargs):
    kwargs['test'].assertEqual(kwargs['expected'], MOCKED_SESSION_REQUEST_HISTORY)


def delete_cache(**kwargs):
    kwargs['core'].context.cache.pop(kwargs['user_id'])


SET_STATUS_CACHE = {}

def set_status(**kwargs):
    tgs = kwargs['tgs']
    cid = kwargs['cid']
    uid = kwargs['uid']
    key = f'{cid}:{uid}'

    context.get_admin_ids.clear()
    context.all_members_are_administrators.clear()

    if 'status' in kwargs:
        SET_STATUS_CACHE.setdefault(key, []).append(tgs.status[cid][uid])
        status = kwargs['status']
    else:
        status = SET_STATUS_CACHE[key].pop()

    tgs.status[cid][uid] = status


def del_rule(**kwargs):
    dbs = kwargs['core'].context.dbses()
    try:
        dbs.query(DBRule).filter_by(name=kwargs['name']).delete()
        dbs.commit()
    finally:
        dbs.close()


TIMEOUT_EDIT_CHAT_CACHE = []

def edit_timeout_edit_chat(**kwargs):
    if 'new' in kwargs:
        TIMEOUT_EDIT_CHAT_CACHE.append(CFG.TIMEOUT_EDIT_CHAT)
        timeout = round((time.time() - NOW) / 10, 3)  # related to system load
    else:
        timeout = TIMEOUT_EDIT_CHAT_CACHE.pop()

    CFG.TIMEOUT_EDIT_CHAT = timeout


def wait_timeout_edit_chat(**_):
    time.sleep(CFG.TIMEOUT_EDIT_CHAT)


# ------------------------------------------------------


MENU_PROCESSING_G1 = (
    ['adm', '/menu'],
    ['bot', f'{CAN_CANCEL}{PROCESSING}<code>G1</code>\n{TO_DO}', ACTION]
)

MENU_SELECT_G1_C1 = (
    ['adm', '/menu'],
    ['bot', f'{CAN_CANCEL}{CHAT_CONFIG}', [f'1{ITEM}G1', f'2{ITEM}C1']],
)

MENU_SELECT_C1_S1_S2 = (
    ['adm', '/menu'],
    ['bot', f'{CAN_CANCEL}{CHAT_CONFIG}', [f'1{ITEM}C1', f'2{ITEM}S1', f'3{ITEM}S2']],
)

TO_CANCEL = (
    ['adm', '/cancel'],
    ['bot', CANCELED],
)


# ----------


RULE_A_REGEX = 'Ł \\w+\n \\s*\n@ .+|.*\n^(?P<N>Ł) \\s+@'
RULE_A_URL = r'http://ok.net/get?v=12'
RULE_A_CHECK = ('rule_a', RULE_A_REGEX[:-1], None, 'message_id; user_id',
                RULE_A_URL[:-1], 'GET', ADMID, G1ID)
RULE_A = (
    *MENU_PROCESSING_G1,
    ['adm', MenuAction.CREATE],
    ['bot', f'{NEW_RULE_NAME}'],
    # ------------------------------------------------------------------
    # Two users can not configure the same chat at the same time
    ['dev', '/menu'],
    ['bot', f'{CHAT_EDITED.format("G1", mention_html(ADMID, "@adm")) + NOTHING_MORE}'],
    # ------------------------------------------------------------------
    ['adm', 'rule_a'],
    ['bot', f'{REGEX_CODE}', REGEX_CREATE],
    ['adm', RULE_A_REGEX[:-1]],
    ['bot', f'{ARGS_NAME}', get_args_kb('group')],
    ['adm', MenuArgs.MID],
    ['bot', *get_args_text_and_kb('group', True, 'MID')],
    ['adm', MenuArgs.UID],
    ['bot', *get_args_text_and_kb('group', True, 'MID UID')],
    ['adm', MenuArgs.END],
    ['bot', f'{WEBHOOK}'],
    ['adm', RULE_A_URL[:-1]],
    ['bot', f'{SEND_TYPE + SendType.full_list()}', SEND_CREATE],
    ['adm', 'GET'],
    ['bot', COMPLETED],
)


MOD_RULE_A_CHECK = ('rule_a2', RULE_A_REGEX, None, 'user_id',
                    RULE_A_URL, 'GET', ADMID, G1ID)
RULE_A_CHECK_2 = *RULE_A_CHECK[:-1], S1ID
MOD_RULE_A = (
    *MENU_PROCESSING_G1,
    ['adm', MenuAction.UPDATE],
    ['bot', f'{PROCESSING}<code>rule_a</code>\n{RULE_NAME}{CURRENT}rule_a',
     NAME_UPDATE],
    ['adm', 'rule_a2'],
    ['bot', f'{REGEX_CODE}{CURRENT}{core.show_regex_html(RULE_A_REGEX[:-1])}',
     REGEX_UPDATE],
    ['adm', RULE_A_REGEX],
    ['bot', f'{ARGS_NAME}{CURRENT}message_id; user_id', get_args_kb('group',
                                                                    False,
                                                                    'MID UID')],
    ['adm', MenuArgs.CLS],
    ['bot', *get_args_text_and_kb('group', False)],
    ['adm', MenuArgs.UID],
    ['bot', *get_args_text_and_kb('group', False, 'UID')],
    ['adm', MenuArgs.END],
    ['bot', f'{WEBHOOK}{CURRENT}{RULE_A_URL[:-1]}', URL_UPDATE],
    ['adm', RULE_A_URL],
    ['bot', f'{SEND_TYPE + SendType.full_list()}{CURRENT}GET', SEND_UPDATE],
    ['adm', 'GET'],
    ['bot', COMPLETED],
)


RULE_B_UNDONE = (*RULE_A[:4], ['adm', 'rule_b'], *RULE_A[7:-2], *TO_CANCEL)

RULE_A_B_UNDONE = (
    *MENU_PROCESSING_G1,
    ['adm', 'error'],
    ['bot', f'{WRONG_DATA}\n<i>{USE_KEYBOARD}</i>'],
    ['adm', MenuAction.CREATE],
    ['bot', f'{NEW_RULE_NAME}'],
    ['adm', 'rule_a2'],
    ['bot', f'{WRONG_DATA}\n<i>{NAME_EXISTS}</i>'],
    ['adm', 'rule_b'],
    ['bot', f'{REGEX_CODE}', REGEX_CREATE],
    ['adm', r'\w+\s+\w+'],
    ['bot', f'{WRONG_DATA}\n<i>{html.escape(REGEX_GROUP_ERROR)}</i>'],
    ['adm', r'\w+('],
    ['bot', f'{WRONG_DATA}\n<i>missing ), unterminated subpattern at position 3</i>'],
    ['adm', r'^(?P<N>\w+)\s+.*$'],
    ['bot', f'{ARGS_NAME}', get_args_kb('group')],
    ['adm', MenuArgs.MID],
    ['bot', *get_args_text_and_kb('group', True, 'MID')],
    ['adm', MenuArgs.MID],
    ['bot', *get_args_text_and_kb('group', True, 'MID')],
    ['adm', MenuArgs.PCW],
    ['bot', f'{WRONG_DATA}\n<i>{ARG_VALID_CHANNELS}. {USE_KEYBOARD}</i>'],
    ['adm', 'error'],
    ['bot', f'{WRONG_DATA}\n<i>{ARG_INVALID}. {USE_KEYBOARD}</i>'],
    ['adm', MenuArgs.END],
    ['bot', f'{WEBHOOK}'],
    ['adm', 'http://raise.webhook/'],
    ['bot', f'{WRONG_DATA}\n<i>raise.webhook</i>'],
    *TO_CANCEL,
)


C1_EDITED = CHAT_EDITED.format("C1", mention_html(ADMID, "@adm")) + '\n\n'
RULE_B_REGEX = r'^(\w)(\w).*$'
RULE_B_URL = r'http://ok.net/get?v=2'
RULE_B_CHECK = ('rule_b', RULE_B_REGEX, 'array', 'post_author',
                RULE_B_URL, 'GET', ADMID, C1ID)
RULE_B = (
    *MENU_SELECT_G1_C1,
    ['adm', 'error'],
    ['bot', f'{WRONG_DATA}\n<i>{USE_KEYBOARD}</i>'],
    ['adm', f'1{ITEM}error'],
    ['bot', f'{WRONG_DATA}\n<i>{USE_KEYBOARD}</i>'],
    ['adm', f'2{ITEM}C1'],
    ['bot', f'{TO_DO}', ACTION],
    # ------------------------------------------------------------------
    # Two users can not configure the same chat at the same time
    ['dev', '/menu'],
    ['bot', f'{C1_EDITED}{CAN_CANCEL}{PROCESSING}<code>G1</code>\n{TO_DO}', ACTION],
    ['dev', '/cancel'],
    ['bot', CANCELED],
    # ------------------------------------------------------------------
    ['adm', MenuAction.CREATE],
    ['bot', f'{NEW_RULE_NAME}'],
    ['adm', 'rule_b'],
    ['bot', f'{REGEX_CODE}', REGEX_CREATE],
    ['adm', RULE_B_REGEX],
    ['bot', f'{LIST_NAME}'],
    ['adm', '555'],
    ['bot', f'{WRONG_DATA}\n<i>{html.escape(NAME_ERROR)}</i>'],
    ['adm', 'te=st$5'],
    ['bot', f'{WRONG_DATA}\n<i>{html.escape(NAME_ERROR)}</i>'],
    ['adm', 'array'],
    ['bot', f'{ARGS_NAME}', get_args_kb('channel')],
    ['adm', MenuArgs.PAU],
    ['bot', *get_args_text_and_kb('channel', True, 'PAU')],
    ['adm', MenuArgs.END],
    ['bot', f'{WEBHOOK}'],
    ['adm', RULE_B_URL],
    ['bot', f'{SEND_TYPE + SendType.full_list()}', SEND_CREATE],
    ['adm', 'GET_JSON'],
    ['bot', f'{WRONG_DATA}\n<i>{USE_KEYBOARD}</i>'],
    ['adm', 'GET'],
    ['bot', COMPLETED],
)


RULE_C_UNDONE = (
    functools.partial(edit_timeout_edit_chat, new=True),

    ['adm', '/menu'],
    ['bot', f'{CAN_CANCEL}{CHAT_CONFIG}', [f'1{ITEM}G1', f'2{ITEM}C1']],

    ['dev', '/menu'],
    ['bot', f'{CAN_CANCEL}{CHAT_CONFIG}', [f'1{ITEM}G1', f'2{ITEM}C1']],

    # 'dev' chooses 'G1'
    ['dev', f'1{ITEM}G1'],
    ['bot', f'{TO_DO}', ACTION],

    # 'adm' can no longer choose 'G1'
    ['adm', f'1{ITEM}G1'],
    ['bot', f'{NOT_POSSIBLE.format(mention_html(DEVID, "@dev"))}'],

    ['dev', MenuAction.CREATE],
    ['bot', f'{NEW_RULE_NAME}'],
    ['dev', 'rule_b'],
    ['bot', f'{REGEX_CODE}', REGEX_CREATE],

    wait_timeout_edit_chat,  # will no longer be able to edit

    ['dev', '(.*)'],
    ['bot', f'{MEM_ERROR}'],

    functools.partial(edit_timeout_edit_chat),              # reset timeout
)


RULE_C_REGEX = None
RULE_C_URL = r'http://ok.net/post?v=3'
RULE_C_CHECK = ('rule_c', RULE_C_REGEX, None, 'chat_username',
                RULE_C_URL, 'POST_JSON', ADMID, C1ID)
RULE_C = (
    *MENU_SELECT_G1_C1,
    ['adm', f'2{ITEM}C1'],
    ['bot', f'{TO_DO}', ACTION],
    ['adm', MenuAction.CREATE],
    ['bot', f'{NEW_RULE_NAME}'],
    ['adm', 'rule_c'],
    ['bot', f'{REGEX_CODE}', REGEX_CREATE],
    ['adm', OVERALL_OPTION],
    ['bot', f'{ARGS_NAME}', get_args_kb('channel')],
    ['adm', MenuArgs.MID],
    ['bot', *get_args_text_and_kb('channel', True, 'MID')],
    ['adm', MenuArgs.MDT],
    ['bot', *get_args_text_and_kb('channel', True, 'MID MDT')],
    ['adm', MenuArgs.CID],
    ['bot', *get_args_text_and_kb('channel', True, 'MID MDT CID')],
    ['adm', MenuArgs.CTP],
    ['bot', *get_args_text_and_kb('channel', True, 'MID MDT CID CTP')],
    ['adm', MenuArgs.CTT],
    ['bot', *get_args_text_and_kb('channel', True, 'MID MDT CID CTP CTT')],
    ['adm', MenuArgs.CUN],
    ['bot', *get_args_text_and_kb('channel', True, 'MID MDT CID CTP CTT CUN')],
    ['adm', MenuArgs.PAU],
    ['bot', *get_args_text_and_kb('channel', True, 'MID MDT CID CTP CTT CUN PAU')],
    ['adm', MenuArgs.PCW],
    ['bot', *get_args_text_and_kb('channel', True, 'MID MDT CID CTP CTT CUN PAU PCW')],
    ['adm', MenuArgs.CLS],
    ['bot', *get_args_text_and_kb('channel', True)],
    ['adm', MenuArgs.CUN],
    ['bot', *get_args_text_and_kb('channel', True, 'CUN')],
    ['adm', MenuArgs.END],
    ['bot', f'{WEBHOOK}'],
    ['adm', RULE_C_URL],
    ['bot', f'{SEND_TYPE + SendType.full_list()}', SEND_CREATE],
    ['adm', 'POST_JSON'],
    ['bot', COMPLETED],
)


MOD_RULE_C_CHECK = RULE_C_CHECK
MOD_RULE_C = (
    *MENU_SELECT_G1_C1,
    ['adm', f'2{ITEM}C1'],
    ['bot', f'{TO_DO}', ACTION],
    ['adm', MenuAction.UPDATE],
    ['bot', f'{RULE_CONFIG}', [f'1{ITEM}rule_b', f'2{ITEM}rule_c']],
    ['adm', f'error'],
    ['bot', f'{WRONG_DATA}\n<i>{USE_KEYBOARD}</i>'],
    ['adm', f'2{ITEM}error'],
    ['bot', f'{WRONG_DATA}\n<i>{USE_KEYBOARD}</i>'],
    ['adm', f'2{ITEM}rule_c'],
    ['bot', f'{RULE_NAME}{CURRENT}rule_c', NAME_UPDATE],
    ['adm', UNCHANGED_OPTION],
    ['bot', f'{REGEX_CODE}{CURRENT}{SET_GLOBAL}', REGEX_UPDATE],
    ['adm', UNCHANGED_OPTION],
    ['bot', f'{ARGS_NAME}{CURRENT}chat_username', get_args_kb('channel', False, 'CUN')],
    ['adm', UNCHANGED_OPTION],
    ['bot', f'{WEBHOOK}{CURRENT}{RULE_C_URL}', URL_UPDATE],
    ['adm', UNCHANGED_OPTION],
    ['bot', f'{SEND_TYPE + SendType.full_list()}{CURRENT}POST_JSON', SEND_UPDATE],
    ['adm', 'POST_JSON'],
    ['bot', COMPLETED],
)


LIST_G1_INFO = '\n'.join([
    get_rule_info('rule_a2', RULE_A_REGEX, None, 'user_id', RULE_A_URL, 'GET'),
])
LIST_G1 = (
    *MENU_SELECT_G1_C1,
    ['adm', f'1{ITEM}G1'],
    ['bot', f'{TO_DO}', ACTION],
    ['adm', MenuAction.LIST],
    ['bot', f'{RULES}\n{LIST_G1_INFO.rstrip()}'],
)


LIST_C1_INFO = '\n'.join([
    get_rule_info('rule_b', RULE_B_REGEX, 'array', 'post_author', RULE_B_URL, 'GET'),
    get_rule_info('rule_c', None, None, 'chat_username', RULE_C_URL, 'POST_JSON'),
])
LIST_C1 = (
    *MENU_SELECT_G1_C1,
    ['adm', f'2{ITEM}C1'],
    ['bot', f'{TO_DO}', ACTION],
    ['adm', MenuAction.LIST],
    ['bot', f'{RULES}\n{LIST_C1_INFO.rstrip()}'],
)


RULE_D_REGEX = r'^(?P<edited>\w+) (?P<chat_id>\w+) (\d+) (\d+)$'
RULE_D_URL = r'http://ok.net/post?v=4&chat_id=4'
RULE_D_CHECK = ('rule_d', RULE_D_REGEX, 'nums', 'chat_id',
                RULE_D_URL, 'POST_FORM', ADMID, C1ID)
RULE_D_PART_1 = (
    ['bot', f'{REGEX_CODE}{CURRENT}{html.escape(RULE_D_REGEX)}', REGEX_UPDATE],
    ['adm', UNCHANGED_OPTION],
    ['bot', f'{EDITED_VAR_MOD}', RCONF],
    ['adm', NO_OPTION],
    ['bot', f'{LIST_NAME}'],
    ['adm', 'nums[0]'],
    ['bot', f'{WRONG_DATA}\n<i>{html.escape(NAME_ERROR)}</i>'],
    ['adm', 'nums'],
    ['bot', f'{ARGS_NAME}', get_args_kb('channel')],
    ['adm', MenuArgs.CID],
    ['bot', *get_args_text_and_kb('channel', True, 'CID')],
    ['adm', MenuArgs.END],
    ['bot', f'{ARGS_OVERLAP_MOD.format("chat_id")}', ACONF],
)
RULE_D_PART_2 = (
    *RULE_D_PART_1,
    ['adm', YES_OPTION],
    *RULE_D_PART_1,
    ['adm', NO_OPTION],
    ['bot', f'{WEBHOOK}'],
    ['adm', RULE_D_URL],
    ['bot', f'{QUERY_OVERLAP_MOD.format("chat_id")}', ACONF],
)
RULE_D = (
    *MENU_SELECT_G1_C1,
    ['adm', f'2{ITEM}C1'],
    ['bot', f'{TO_DO}', ACTION],
    ['adm', MenuAction.CREATE],
    ['bot', f'{NEW_RULE_NAME}'],
    ['adm', 'rule_d'],
    ['bot', f'{REGEX_CODE}', REGEX_CREATE],
    ['adm', RULE_D_REGEX],
    ['bot', f'{EDITED_VAR_MOD}', RCONF],
    ['adm', YES_OPTION],
    *RULE_D_PART_2,
    ['adm', YES_OPTION],
    *RULE_D_PART_2,
    ['adm', NO_OPTION],
    ['bot', f'{SEND_TYPE + SendType.full_list()}', SEND_CREATE],
    ['adm', 'POST_FORM'],
    ['bot', COMPLETED],
)


RULE_E_REGEX = None
RULE_E_URL = r'http://ok.net/get?v=5'
RULE_E_CHECK = ('rule_e', RULE_E_REGEX, None, None,
                RULE_E_URL, 'GET', ADMID, C1ID)
RULE_E = (
    *MENU_SELECT_G1_C1,
    ['adm', f'2{ITEM}C1'],
    ['bot', f'{TO_DO}', ACTION],
    ['adm', MenuAction.CREATE],
    ['bot', f'{NEW_RULE_NAME}'],
    ['adm', 'rule_e'],
    ['bot', f'{REGEX_CODE}', REGEX_CREATE],
    ['adm', OVERALL_OPTION],
    ['bot', f'{ARGS_NAME}', get_args_kb('channel')],
    ['adm', MenuArgs.END],
    ['bot', f'{WEBHOOK}'],
    ['adm', RULE_E_URL],
    ['bot', f'{SEND_TYPE + SendType.full_list()}', SEND_CREATE],
    ['adm', 'GET'],
    ['bot', COMPLETED],
)


RULE_F_UNDONE = (
    *MENU_SELECT_G1_C1,
    ['adm', f'2{ITEM}C1'],
    ['bot', f'{TO_DO}', ACTION],
    ['adm', MenuAction.CREATE],
    ['bot', f'{RULE_LIMIT}'],
)


_RULE_F_NAME = 'ab' * CFG.LIMIT_NAME_LENGTH  # over the limit
RULE_F_NAME = tools.get_limited_name(_RULE_F_NAME)
RULE_F_REGEX = r'^(?P<var>\w+).*$'
RULE_F_URL = r'http://ok.net:/post?v=6'
RULE_F_CHECK = (RULE_F_NAME, RULE_F_REGEX, None, None,
                RULE_F_URL, 'POST_JSON', ADMID, C1ID)
RULE_F = (
    *MENU_SELECT_G1_C1,
    ['adm', f'2{ITEM}C1'],
    ['bot', f'{TO_DO}', ACTION],
    ['adm', MenuAction.CREATE],
    ['bot', f'{NEW_RULE_NAME}'],
    ['adm', _RULE_F_NAME],
    ['bot', f'{REGEX_CODE}', REGEX_CREATE],
    ['adm', RULE_F_REGEX],
    ['bot', f'{ARGS_NAME}', get_args_kb('channel')],
    ['adm', MenuArgs.UID],
    ['bot', f'{WRONG_DATA}\n<i>{ARG_VALID_GROUPS}. {USE_KEYBOARD}</i>'],
    ['adm', MenuArgs.END],
    ['bot', f'{WEBHOOK}'],
    ['adm', 'ftp://unit.test/'],
    ['bot', f'{WRONG_DATA}\n<i>Invalid URL</i>'],
    ['adm', 'ftp://unit.test:a/'],
    ['bot', f'{WRONG_DATA}\n<i>Invalid URL</i>'],
    ['adm', 'ftp://[1:2:3:4:5:6]/'],
    ['bot', f'{WRONG_DATA}\n<i>Invalid URL</i>'],
    ['adm', RULE_F_URL],
    ['bot', f'{SEND_TYPE + SendType.full_list()}', SEND_CREATE],
    ['adm', 'POST_JSON'],
    ['bot', COMPLETED],
)

RULE_F_G_UNDONE = (
    *MENU_SELECT_G1_C1,
    ['adm', f'2{ITEM}C1'],
    ['bot', f'{TO_DO}', ACTION],
    ['adm', MenuAction.CREATE],
    ['bot', f'{NEW_RULE_NAME}'],
    ['adm', _RULE_F_NAME],
    ['bot', f'{WRONG_DATA}\n<i>{NAME_EXISTS}</i>'],
    ['adm', 'rule_g'],
    ['bot', f'{REGEX_CODE}', REGEX_CREATE],
    *TO_CANCEL,
)


_RULE_G_REGEX = r'^(?P<edited>\w+) (\d+)'
RULE_G_REGEX = _RULE_G_REGEX.replace('edited', 'var')
RULE_G_URL = r'http://ok.net/get?v=7'
RULE_G_CHECK = ('rule_g', RULE_G_REGEX, 'array', 'chat_id',
                RULE_G_URL, 'GET', ADMID, C1ID)
RULE_G = (
    *MENU_SELECT_C1_S1_S2,
    ['adm', f'1{ITEM}C1'],
    ['bot', f'{TO_DO}', ACTION],
    ['adm', MenuAction.CREATE],
    ['bot', f'{NEW_RULE_NAME}'],
    ['adm', 'rule_g'],
    ['bot', f'{REGEX_CODE}', REGEX_CREATE],
    ['adm', _RULE_G_REGEX],
    ['bot', f'{EDITED_VAR_MOD}', RCONF],
    ['adm', YES_OPTION],
    ['bot', f'{REGEX_CODE}{CURRENT}{html.escape(_RULE_G_REGEX)}', REGEX_UPDATE],
    ['adm', RULE_G_REGEX],
    ['bot', f'{LIST_NAME}'],
    ['adm', 'var'],
    ['bot', f'{NAME_EXISTS_MOD}', LCONF],
    ['adm', YES_OPTION],
    ['bot', f'{REGEX_CODE}{CURRENT}{html.escape(RULE_G_REGEX)}', REGEX_UPDATE],
    ['adm', UNCHANGED_OPTION],
    ['bot', f'{LIST_NAME}'],
    ['adm', 'chat_id'],
    ['bot', f'{ARGS_NAME}', get_args_kb('channel')],
    ['adm', MenuArgs.CID],
    ['bot', *get_args_text_and_kb('channel', True, 'CID')],
    ['adm', MenuArgs.END],
    ['bot', f'{ARGS_OVERLAP_MOD.format("chat_id")}', ACONF],
    ['adm', YES_OPTION],
    ['bot', f'{REGEX_CODE}{CURRENT}{html.escape(RULE_G_REGEX)}', REGEX_UPDATE],
    ['adm', UNCHANGED_OPTION],
    ['bot', f'{LIST_NAME}'],
    ['adm', 'array'],
    ['bot', f'{ARGS_NAME}', get_args_kb('channel')],
    ['adm', MenuArgs.CID],
    ['bot', *get_args_text_and_kb('channel', True, 'CID')],
    ['adm', MenuArgs.END],
    ['bot', f'{WEBHOOK}'],
    ['adm', RULE_G_URL],
    ['bot', f'{SEND_TYPE + SendType.full_list()}', SEND_CREATE],
    ['adm', UNCHANGED_OPTION],
    ['bot', f'{WRONG_DATA}\n<i>{INCORRECT_USE_UNCHANGED}</i>'],
    ['adm', 'A'],
    ['bot', f'{WRONG_DATA}\n<i>{USE_KEYBOARD}</i>'],
    ['adm', 'GET'],
    ['bot', COMPLETED],
)


MOD_RULE_G_COMMON = (
    *MENU_SELECT_C1_S1_S2,
    ['adm', f'1{ITEM}C1'],
    ['bot', f'{TO_DO}', ACTION],
    ['adm', MenuAction.UPDATE],
    ['bot', f'{RULE_CONFIG}', [f'1{ITEM}rule_b', f'2{ITEM}rule_d',
                               f'3{ITEM}{RULE_F_NAME}', f'4{ITEM}rule_g']],
    ['adm', f'4{ITEM}rule_g'],
    ['bot', f'{RULE_NAME}{CURRENT}rule_g', NAME_UPDATE],
    ['adm', UNCHANGED_OPTION],
)


MOD_RULE_G_1_CHECK = RULE_G_CHECK
MOD_RULE_G_1 = (
    *MOD_RULE_G_COMMON,
    ['bot', f'{REGEX_CODE}{CURRENT}{html.escape(RULE_G_REGEX)}', REGEX_UPDATE],
    ['adm', _RULE_G_REGEX],
    #10
    ['bot', f'{EDITED_VAR_MOD}', RCONF],
    ['adm', YES_OPTION],
    ['bot', f'{REGEX_CODE}{CURRENT}{html.escape(_RULE_G_REGEX)}', REGEX_UPDATE],
    ['adm', RULE_G_REGEX],
    ['bot', f'{LIST_NAME}{CURRENT}array', LNAME_UPDATE],
    ['adm', 'var'],
    ['bot', f'{NAME_EXISTS_MOD}', LCONF],
    ['adm', YES_OPTION],
    ['bot', f'{REGEX_CODE}{CURRENT}{html.escape(RULE_G_REGEX)}', REGEX_UPDATE],
    ['adm', UNCHANGED_OPTION],
    #20
    ['bot', f'{LIST_NAME}{CURRENT}var', LNAME_UPDATE],
    ['adm', 'chat_id'],
    ['bot', f'{ARGS_NAME}{CURRENT}chat_id', get_args_kb('channel', False, 'CID')],
    ['adm', MenuArgs.END],
    ['bot', f'{ARGS_OVERLAP_MOD.format("chat_id")}', ACONF],
    ['adm', YES_OPTION],
    ['bot', f'{REGEX_CODE}{CURRENT}{html.escape(RULE_G_REGEX)}', REGEX_UPDATE],
    ['adm', UNCHANGED_OPTION],
    ['bot', f'{LIST_NAME}{CURRENT}chat_id', LNAME_UPDATE],
    ['adm', 'array'],
    #30
    ['bot', f'{ARGS_NAME}{CURRENT}chat_id', get_args_kb('channel', False, 'CID')],
    ['adm', UNCHANGED_OPTION],
    ['bot', f'{WEBHOOK}{CURRENT}{html.escape(RULE_G_URL)}', URL_UPDATE],
    ['adm', UNCHANGED_OPTION],
    ['bot', f'{SEND_TYPE + SendType.full_list()}{CURRENT}GET', SEND_UPDATE],
    ['adm', UNCHANGED_OPTION],
    ['bot', COMPLETED],
    #37
)


MOD_RULE_G_2_REGEX = r'^(?P<edited>\w+)'
MOD_RULE_G_2_CHECK = *RULE_G_CHECK[:1], MOD_RULE_G_2_REGEX, None, *RULE_G_CHECK[3:]

MOD_RULE_G_2 = (
    *MOD_RULE_G_COMMON,
    ['bot', f'{REGEX_CODE}{CURRENT}{html.escape(RULE_G_REGEX)}', REGEX_UPDATE],
    ['adm', MOD_RULE_G_2_REGEX],
    #10
    ['bot', f'{EDITED_VAR_MOD}', RCONF],
    ['adm', NO_OPTION],
    ['bot', f'{ARGS_NAME}{CURRENT}chat_id', get_args_kb('channel', False, 'CID')],
    ['adm', MenuArgs.END],
    ['bot', f'{WEBHOOK}{CURRENT}{html.escape(RULE_G_URL)}', URL_UPDATE],
    ['adm', UNCHANGED_OPTION],
    ['bot', f'{SEND_TYPE + SendType.full_list()}{CURRENT}GET', SEND_UPDATE],
    ['adm', 'GET'],
    ['bot', COMPLETED],
    #19
)


MOD_RULE_G_3_CHECK = *RULE_G_CHECK[:2], 'var', None, *RULE_G_CHECK[4:]
MOD_RULE_G_3 = (
    *MOD_RULE_G_COMMON,
    ['bot', f'{REGEX_CODE}{CURRENT}{html.escape(MOD_RULE_G_2_REGEX)}', REGEX_UPDATE],
    ['adm', RULE_G_REGEX],
    #10
    ['bot', f'{LIST_NAME}{CURRENT}{NOT_SET}', LNAME_UPDATE],
    ['adm', 'var'],
    ['bot', f'{NAME_EXISTS_MOD}', LCONF],
    ['adm', YES_OPTION],
    ['bot', f'{REGEX_CODE}{CURRENT}{html.escape(RULE_G_REGEX)}', REGEX_UPDATE],
    ['adm', UNCHANGED_OPTION],
    ['bot', f'{LIST_NAME}{CURRENT}var', LNAME_UPDATE],
    ['adm', UNCHANGED_OPTION],
    ['bot', f'{NAME_EXISTS_MOD}', LCONF],
    ['adm', NO_OPTION],
    #20
    ['bot', f'{ARGS_NAME}{CURRENT}chat_id', get_args_kb('channel', False, 'CID')],
    ['adm', MenuArgs.CLS],
    ['bot', *get_args_text_and_kb('channel', False)],
    ['adm', MenuArgs.END],
    ['bot', f'{WEBHOOK}{CURRENT}{html.escape(RULE_G_URL)}', URL_UPDATE],
    ['adm', UNCHANGED_OPTION],
    ['bot', f'{SEND_TYPE + SendType.full_list()}{CURRENT}GET', SEND_UPDATE],
    ['adm', 'GET'],
    ['bot', COMPLETED],
    #29
)


# ------------------------------------------------------


TEST_RULE_A_QUERY = f'&N=abc&user_id={ADMID}'
TEST_RULE_A_TEXT = TEST_SUMMARY.format('<code>N: abc</code>',
                                       'GET',
                                       html.escape(RULE_A_URL + TEST_RULE_A_QUERY),
                                       '')
TEST_RULE_A = (
    *MENU_SELECT_G1_C1,
    ['adm', f'1{ITEM}G1'],
    ['bot', f'{TO_DO}', ACTION],
    ['adm', MenuAction.TEST],
    ['bot', f'{PROCESSING}<code>rule_a2</code>\n{TEST_DATA}'],
    ['adm', f'abc 000'],
    ['bot', f'{TEST_RULE_A_TEXT}'],
)


TEST_RULE_B_QUERY = '&array=a&array=b&post_author=None'
TEST_RULE_B_DATA = '<code>array: ["a", "b"]</code>'.replace('"', '&#x27;')
TEST_RULE_B_TEXT = TEST_SUMMARY.format(TEST_RULE_B_DATA,
                                       'GET',
                                       html.escape(RULE_B_URL + TEST_RULE_B_QUERY),
                                       '')
TEST_RULE_B_NOTE2 = TEST_SUMMARY_NOTE2.format('post_author')

TEST_RULE_B = (
    *MENU_SELECT_G1_C1,
    ['adm', f'2{ITEM}C1'],
    ['bot', f'{TO_DO}', ACTION],
    ['adm', MenuAction.TEST],  # only those with regex patterns
    ['bot', f'{RULE_CONFIG}', [f'1{ITEM}rule_b', f'2{ITEM}rule_d']],
    ['adm', f'1{ITEM}rule_b'],
    ['bot', f'{TEST_DATA}'],
    ['adm', f'ab000'],
    ['bot', f'{TEST_RULE_B_TEXT}\n{TEST_RULE_B_NOTE2}'],
)


WORK_G1 = (
    clear_request,
    ['adm:g1', 'abc 000'],  # rule_a2                     chat=G1, message_id=1
    functools.partial(check_request, expected=[
        f'GET:http://ok.net/get?v=12&N=abc&edited=False&user_id={ADMID}',
    ]),
)

WORK_G1_EDITED = (
    clear_request,
    ['adm:g1', 'edit:1:abcdef 000'],
    functools.partial(check_request, expected=[
        f'GET:http://ok.net/get?v=12&N=abcdef&edited=True&user_id={ADMID}',
    ]),
)


WORK_C1_AUTHOR = f'{USERS[ADMID]["first_name"]} {USERS[ADMID]["last_name"]}'
WORK_C1_GLOBAL = ['POST:http://ok.net/post',                    # rule_c
                  'GET:http://ok.net/get?v=5&edited=False']     # rule_e
WORK_C1 = (
    clear_request,
    ['adm:c1', 'ab000'],  # rule_b
    functools.partial(check_request, expected=[
        'GET:http://ok.net/get?' + urllib.parse.quote_plus('&'.join((
            'v=2',
            'array=a',
            'array=b',
            'edited=False',
            f'post_author={WORK_C1_AUTHOR}',
        )), safe='&='),
        *WORK_C1_GLOBAL,
    ]),

    clear_request,
    ['adm:c1', 't ex 1 23'],  # rule_d
    functools.partial(check_request, expected=[
        'POST:http://ok.net/post',
        *WORK_C1_GLOBAL,
    ]),
)


TEST_RULE_F_TEXT = TEST_SUMMARY.format('<code>var: tex</code>',
                                       'POST',
                                       html.escape(RULE_F_URL.split('?')[0]),
                                       html.escape(str({'v':6, 'var': 'tex'})))
TEST_RULE_F = (
    *MENU_SELECT_G1_C1,
    ['adm', f'2{ITEM}C1'],
    ['bot', f'{TO_DO}', ACTION],
    ['adm', MenuAction.TEST],  # only those with regex patterns
    ['bot', f'{RULE_CONFIG}', [f'1{ITEM}rule_b', f'2{ITEM}rule_d',
                               f'3{ITEM}{RULE_F_NAME}']],
    ['adm', f'3{ITEM}{RULE_F_NAME}'],
    ['bot', f'{TEST_DATA}'],
    ['adm', f'tex!'],
    ['bot', f'{TEST_RULE_F_TEXT}'],
)


ERROR_URL_1 = 'http://raise.invalid_url/test?e=1'
ERROR_URL_2 = 'http://raise.response_error/test?e=2'
ERROR_URL_3 = 'http://error.unauthorized/test?e=3'


ERROR_TEST_RULE_A_TEXT = '\n'.join((
    TEST_WEBHOOK_ERROR.format('raise.invalid_url'),
    TEST_SUMMARY.format('<code>N: abc</code>',
                        'GET',
                        html.escape(ERROR_URL_1 + TEST_RULE_A_QUERY),
                        '')))

ERROR_1_WORK_G1_TEXT = GROUP_RULES_ERROR.format('G1', 'invalid url')
ERROR_2_WORK_G1_TEXT = GROUP_RULES_ERROR.format('G1', 'response error')
ERROR_3_WORK_G1_TEXT = GROUP_RULES_ERROR.format('G1', '401 Unauthorized: unauthorized')

ERROR_1_WORK_C1_TEXT = CHANNEL_RULES_ERROR.format('C1', 'invalid url')

ERROR_TEST_RULE_F_TEXT = '\n'.join((
    TEST_WEBHOOK_ERROR.format('raise.response_error'),
    TEST_SUMMARY.format('<code>var: tex</code>',
                        'POST',
                        html.escape(ERROR_URL_2.split('?')[0]),
                        html.escape(str({'e': 2, 'var': 'tex'})))))


# ------------------------------------------------------


DEL_RULE_E = (
    *MENU_SELECT_G1_C1,
    ['adm', f'2{ITEM}C1'],
    ['bot', f'{TO_DO}', ACTION],
    ['adm', MenuAction.DELETE],
    ['bot', f'{RULE_CONFIG}', [f'1{ITEM}rule_b', f'2{ITEM}rule_c',
                               f'3{ITEM}rule_d', f'4{ITEM}rule_e']],
    ['adm', f'4{ITEM}rule_e'],
    ['bot', COMPLETED],
)


DEL_RULE_A = (
    *MENU_SELECT_G1_C1,
    ['adm', f'1{ITEM}G1'],
    ['bot', f'{TO_DO}', ACTION],
    ['adm', MenuAction.DELETE],
    ['bot', f'{PROCESSING}<code>rule_a2</code>'],
    ['bot', COMPLETED],
)


SAY_G1_C1 = SAY_TEXT.format(f' <code>{G1ID}</code> ─ G1\n <code>{C1ID}</code> ─ C1')


FATAL_ERROR_RE = re.compile(r'The error <code>.+?</code> happened'
                            r' with the user <a href="tg://user\?id=\d+">.+?</a>'
                            r' within the chat <i>.+?</i> \(@.+?\)'
                            r'. '
                            r'The full traceback:\n\n'
                            r'<code>'
                            r'(  File ".+?", line \d+, in .+\n    .+\n)+'
                            r'</code>')


# ------------------------------------------------------

# Rule Chat Regex Method    In Test/Work
#   a   G1    ✓   GET        ✓
#   b   C1    ✓   GET        ✓
#   c   C1    ✕   POST_JSON  ✓
#   d   C1    ✓   POST_FORM  ✓
#   e   C1    ✕   GET        ✓
#   f   C1    ✓   POST_JSON  ✕
#   g   C1    ✓   GET        ✕

# a: administrator
# c: creator
# m: member
# l: left
# k: kicked
# r: restricted
STATUS_TABLE = '''
    g1 g2 g3 s1 s2 s3 c1 c2
bot  a  m  m  a  m  m  a  m
dev  a  m  m  m  m  m  a  m
adm  c  a  m  c  a  m  c  a
usr  m  m  m  m  m  m  m  m
'''

# Format: header, content[, caption][, markup]
#   header:  STR    'user:chat'             # user username and chat username
#   content: STR    'photo'                 # sendPhoto
#                   'sticker'               # sendSticker
#                   'document'              # sendDocument
#                   'animation'             # sendAnimation
#                   'title:text'            # new_chat_title
#                   'members:[-]UID1,UID2'  # left_chat_member, new_chat_members
#                   'migrate:CID'           # migrate_to_chat_id, migrate_from_chat_id
#                   'forward:CID:MID'       # forwardMessage
#                   'reply:MID:text'        # sendMessage (reply_to_message)
#                   'edit:MID:text'         # editMessageText
#                   'text'                  # sendMessage
#                   # callback is automatic
#   caption: STR    'text'
#   markup:  LIST   ['options']

INTERACTIONS = (
    # Interaction in a group, nothing to do
    ['usr', 'text'],
    ['bot', TEX_ERROR],

    ['usr', '/menu'],
    ['bot', NOTHING_TO_DO],

    ['adm', '/menu'],
    ['bot', NOTHING_TO_DO],

    functools.partial(check_database, expected={
        'chat': [],
        'rule': [],
    }),

    1,

    ['usr:g1', 'text'],
    ['usr', '/menu'],
    ['bot', NOTHING_TO_DO],

    functools.partial(check_database, expected={
        'chat': [*CHAT_G1],
        'rule': [],
    }),

    2,

    # Added a rule_a to the G1 group
    *RULE_A,

    functools.partial(check_database, expected={
        'chat': [*CHAT_G1],
        'rule': [*RULE_A_CHECK],
    }),

    3,

    # Modify rule_a from group G1
    *MOD_RULE_A,

    functools.partial(check_database, expected={
        'chat': [*CHAT_G1],
        'rule': [*MOD_RULE_A_CHECK],
    }),

    4,

    # Testing /cancel
    *RULE_B_UNDONE[:2], *TO_CANCEL, 4.1,
    *RULE_B_UNDONE[:4], *TO_CANCEL, 4.2,
    *RULE_B_UNDONE[:6], *TO_CANCEL, 4.3,
    *RULE_B_UNDONE[:8], *TO_CANCEL, 4.4,
    *RULE_B_UNDONE[:10], *TO_CANCEL, 4.5,
    *RULE_B_UNDONE,

    5,

    # Testing of erroneous data
    *RULE_A_B_UNDONE,

    6,

    # Interaction in a channel
    ['usr:c1', 'text'],
    ['usr', '/menu'],
    ['bot', NOTHING_TO_DO],

    functools.partial(check_database, expected={
        'chat': [*CHAT_G1, *CHAT_C1],
        'rule': [*MOD_RULE_A_CHECK],
    }),

    7,

    # Added a rule_b to the C1 channel
    *RULE_B,

    8,

    # Testing one /cancel
    *RULE_B[:4], *TO_CANCEL,

    # Error test for chat already taken
    *RULE_C_UNDONE,

    9,

    # Added a rule_c to the C1 channel
    *RULE_C,

    functools.partial(check_database, expected={
        'chat': [*CHAT_G1, *CHAT_C1],
        'rule': [*MOD_RULE_A_CHECK, *RULE_B_CHECK, *RULE_C_CHECK],
    }),

    10,

    # Modify rule_c from channel C1
    *MOD_RULE_C,

    functools.partial(check_database, expected={
        'chat': [*CHAT_G1, *CHAT_C1],
        'rule': [*MOD_RULE_A_CHECK, *RULE_B_CHECK, *MOD_RULE_C_CHECK],
    }),

    11,

    *MOD_RULE_C[:12],
    ['adm', 'rule_c'],
    *MOD_RULE_C[13:16],
    ['adm', MenuArgs.END],
    *MOD_RULE_C[-5:],

    12,

    *MOD_RULE_C[:12],
    ['adm', 'rule_b'],
    ['bot', f'{WRONG_DATA}\n<i>{NAME_EXISTS}</i>'],
    *MOD_RULE_C[12:],

    functools.partial(check_database, expected={
        'chat': [*CHAT_G1, *CHAT_C1],
        'rule': [*MOD_RULE_A_CHECK, *RULE_B_CHECK, *MOD_RULE_C_CHECK],
    }),

    13,

    # List rules
    *LIST_G1,
    *LIST_C1,

    14,

    # Added a rule_d in C1, testing alternative routes in the configuration
    *RULE_D,

    functools.partial(check_database, expected={
        'chat': [*CHAT_G1, *CHAT_C1],
        'rule': [*MOD_RULE_A_CHECK, *RULE_B_CHECK, *MOD_RULE_C_CHECK, *RULE_D_CHECK],
    }),

    15,

    # Added a rule_e to the C1 channel
    *RULE_E,

    functools.partial(check_database, expected={
        'chat': [*CHAT_G1, *CHAT_C1],
        'rule': [*MOD_RULE_A_CHECK, *RULE_B_CHECK, *MOD_RULE_C_CHECK, *RULE_D_CHECK,
                 *RULE_E_CHECK],
    }),

    16,

    # Attempt to add a rule outside the limit
    *RULE_F_UNDONE,

    functools.partial(check_database, expected={
        'chat': [*CHAT_G1, *CHAT_C1],
        'rule': [*MOD_RULE_A_CHECK, *RULE_B_CHECK, *MOD_RULE_C_CHECK, *RULE_D_CHECK,
                 *RULE_E_CHECK],
    }),

    17,

    *TEST_RULE_A,
    *TEST_RULE_B,

    18,

    *TEST_RULE_B[:-2],
    ['adm', f'---'],
    ['bot', f'{TEST_NO_DATA_FOUND}'],

    19,

    *WORK_G1,
    *WORK_G1_EDITED,
    *WORK_C1,

    20,

    # With error
    functools.partial(edit_url_rule, name='rule_a2', url=ERROR_URL_1),

    # Test
    *TEST_RULE_A[:-1],
    ['bot', ERROR_TEST_RULE_A_TEXT],

    # Work
    ['adm:g1', 'abc 000'],
    ['bot:adm', ERROR_1_WORK_G1_TEXT],

    functools.partial(edit_url_rule, name='rule_a2'),       # reset url

    21,

    # Test with error
    functools.partial(edit_url_rule, name='rule_a2', url=ERROR_URL_2),
    ['adm:g1', 'abc 000'],
    ['bot:adm', ERROR_2_WORK_G1_TEXT],
    functools.partial(edit_url_rule, name='rule_a2'),       # reset url

    22,

    # Test with error
    functools.partial(edit_url_rule, name='rule_a2', url=ERROR_URL_3),
    ['adm:g1', 'abc 000'],
    ['bot:adm', ERROR_3_WORK_G1_TEXT],
    functools.partial(edit_url_rule, name='rule_a2'),       # reset url

    23,

    # Test with error
    functools.partial(edit_url_rule, name='rule_c', url=ERROR_URL_1),
    ['adm:c1', 'ab000'],
    ['bot:adm', ERROR_1_WORK_C1_TEXT],
    functools.partial(edit_url_rule, name='rule_c'),        # reset url

    24,

    # Test with error, which cannot be reported
    functools.partial(edit_consent, cid=ADMID, value=False),
    functools.partial(edit_url_rule, name='rule_c', url=ERROR_URL_1),
    ['adm:c1', 'ab000'],
    functools.partial(edit_url_rule, name='rule_c'),        # reset url
    functools.partial(edit_consent, cid=ADMID),             # reset consent

    25,

    # Deletion of the rule_a from the G1 group
    *DEL_RULE_A,

    functools.partial(check_database, expected={
        'chat': [*CHAT_G1, *CHAT_C1],
        'rule': [*RULE_B_CHECK, *MOD_RULE_C_CHECK, *RULE_D_CHECK, *RULE_E_CHECK],
    }),

    26,

    # Deletion of the rule_e from the C1 channel
    *DEL_RULE_E,

    functools.partial(check_database, expected={
        'chat': [*CHAT_G1, *CHAT_C1],
        'rule': [*RULE_B_CHECK, *MOD_RULE_C_CHECK, *RULE_D_CHECK],
    }),

    27,

    # Error in memory
    *RULE_E[:-2],
    functools.partial(delete_cache, user_id=ADMID),
    RULE_E[-2],
    ['bot', MEM_ERROR],

    28,

    # Error in permissions
    *RULE_E[:-2],
    functools.partial(set_status, cid=C1ID, uid=ADMID, status='member'),
    RULE_E[-2],
    ['bot', NO_HAVE_RIGHTS],
    functools.partial(set_status, cid=C1ID, uid=ADMID),     # reset status

    29,

    # Error in database
    *MOD_RULE_C[:5],
    ['bot', f'{RULE_CONFIG}', [f'1{ITEM}rule_b', f'2{ITEM}rule_c', f'3{ITEM}rule_d']],
    *MOD_RULE_C[6:-2],
    functools.partial(del_rule, name='rule_c'),
    ['adm', UNCHANGED_OPTION],
    ['bot', RULE_NOT_AVAILABLE],

    30,

    # Added a rule_f to the C1 channel
    *RULE_F,

    functools.partial(check_database, expected={
        'chat': [*CHAT_G1, *CHAT_C1],
        'rule': [*RULE_B_CHECK, *RULE_D_CHECK, *RULE_F_CHECK],
    }),

    31,

    *TEST_RULE_F,

    32,

    *RULE_F_G_UNDONE,

    functools.partial(check_database, expected={
        'chat': [*CHAT_G1, *CHAT_C1],
        'rule': [*RULE_B_CHECK, *RULE_D_CHECK, *RULE_F_CHECK],
    }),

    33,

    # Test with error
    functools.partial(edit_url_rule, name=RULE_F_NAME, url=ERROR_URL_2),
    *TEST_RULE_F[:-1],
    ['bot', ERROR_TEST_RULE_F_TEXT],
    functools.partial(edit_url_rule, name=RULE_F_NAME),     # reset url

    34,

    # Uses not allowed
    ['usr:g1', '/menu'],
    ['bot:g1', PRIVATE_USE],

    ['adm:g1', '/menu'],
    ['bot:g1', PRIVATE_USE],

    ['adm:g1', '/start'],
    ['bot:g1', USAGE_PUBLIC.strip()],

    ['adm:g1', '/help'],
    ['bot:g1', USAGE_PUBLIC.strip()],

    ['adm', '/start'],
    ['bot', USAGE_PRIVATE.strip()],

    ['adm', '/help'],
    ['bot', USAGE_PRIVATE.strip()],

    35,

    ['adm', '/say'],
    ['adm', '/news'],
    ['adm', '/ignore'],
    ['adm', '/debug'],

    ['adm:g1', '/say'],
    ['adm:g1', '/news'],
    ['adm:g1', '/ignore'],
    ['adm:g1', '/debug'],

    ['dev:g1', '/say'],
    ['dev:g1', '/news'],
    ['dev:g1', '/ignore'],
    ['dev:g1', '/debug'],

    36,

    # Command for exclusive use
    ['dev', '/say'],
    ['bot', f'{SAY_G1_C1}'],

    ['dev', '/news'],
    ['bot', f'{NEWS_TEXT}'],

    ['dev', '/ignore'],
    ['bot', f'{IGNORE_TEXT}'],

    ['dev', '/debug'],

    37,

    ['dev', '/say G1 text'],
    ['bot:g1', 'text'],

    ['dev', f'/say {G1ID} text'],
    ['bot:g1', 'text'],

    ['dev', '/say "chat nonexistent" text'],
    ['bot', f'{SAY_G1_C1}'],

    38,

    ['dev', '/news text'],
    ['bot:c1', 'text'],
    ['bot:g1', 'text'],

    39,

    # News that cannot be sent
    functools.partial(edit_consent, cid=G1ID, value=False),
    ['dev', '/news text'],
    ['bot:c1', 'text'],
    ['bot:dev', NEWS_ERROR.format('G1', f'do not have permission in chat {G1ID}')],
    functools.partial(edit_consent, cid=G1ID),              # reset consent

    40,

    ['dev', f'/ignore {G1ID} si'],
    ['bot', IGNORE_STATUS.format(G1ID, True)],

    ['dev', f'/ignore {G1ID} no'],
    ['bot', IGNORE_STATUS.format(G1ID, False)],

    41,

    # Some simple uses
    ['usr', 'animation'],
    ['usr:g1', 'animation'],
    ['adm', 'photo', 'text'],
    ['adm:g1', 'photo', 'text'],
    ['adm', '/cancel'],

    42,

    # Migration Test (re add rule_a)

    *MENU_SELECT_G1_C1,
    ['adm', f'1{ITEM}G1'],
    ['bot', f'{TO_DO}', ACTION],
    *RULE_A[2:4],
    # Elimination of the two-user test (index: 4-5)
    *RULE_A[6:],

    functools.partial(check_database, expected={
        'chat': [*CHAT_G1, *CHAT_C1],
        'rule': [*RULE_B_CHECK, *RULE_D_CHECK, *RULE_F_CHECK, *RULE_A_CHECK],
    }),

    43,

    ['adm:g1', f'migrate:{S1ID}'],
    ['adm:s1', 'text'],  # to update the title

    functools.partial(check_database, expected={
        'chat': [*CHAT_C1, *CHAT_S1],
        'rule': [*RULE_B_CHECK, *RULE_D_CHECK, *RULE_F_CHECK,
                 *RULE_A_CHECK_2],
    }),

    ['adm:s2', f'migrate:{G2ID}'],
    ['adm:s2', 'text'],  # to update the title

    functools.partial(check_database, expected={
        'chat': [*CHAT_C1, *CHAT_S1, *CHAT_S2],
        'rule': [*RULE_B_CHECK, *RULE_D_CHECK, *RULE_F_CHECK,
                 *RULE_A_CHECK_2],
    }),

    44,

    ['adm', '/erroneous'],
    ['bot:adm', ERROR_HAPPENED],
    ['bot:dev', FATAL_ERROR_RE],

    45,

    # Added a rule_g to the C1 channel
    *RULE_G,

    functools.partial(check_database, expected={
        'chat': [*CHAT_C1, *CHAT_S1, *CHAT_S2],
        'rule': [*RULE_B_CHECK, *RULE_D_CHECK, *RULE_F_CHECK,
                 *RULE_A_CHECK_2, *RULE_G_CHECK],
    }),

    46,

    # Modify rule_g from channel C1
    *MOD_RULE_G_1,

    functools.partial(check_database, expected={
        'chat': [*CHAT_C1, *CHAT_S1, *CHAT_S2],
        'rule': [*RULE_B_CHECK, *RULE_D_CHECK, *RULE_F_CHECK,
                 *RULE_A_CHECK_2, *MOD_RULE_G_1_CHECK],
    }),

    47,

    # Modify (2) rule_g from channel C1
    *MOD_RULE_G_2,

    functools.partial(check_database, expected={
        'chat': [*CHAT_C1, *CHAT_S1, *CHAT_S2],
        'rule': [*RULE_B_CHECK, *RULE_D_CHECK, *RULE_F_CHECK,
                 *RULE_A_CHECK_2, *MOD_RULE_G_2_CHECK],
    }),

    48,

    ['usr:g3', 'text'],                                 # chat=G3, message_id=1
    ['usr:g3', 'sticker'],                              # chat=G3, message_id=2
    ['usr:s3', f'forward:{G3ID}:1'],                    # chat=S3, message_id=1
    ['usr:s3', f'forward:{G3ID}:2'],                    # chat=S3, message_id=2

    49,

    # Modify (3) rule_g from channel C1
    *MOD_RULE_G_3,

    functools.partial(check_database, expected={
        'chat': [*CHAT_C1, *CHAT_S1, *CHAT_S2, *CHAT_G3, *CHAT_S3],
        'rule': [*RULE_B_CHECK, *RULE_D_CHECK, *RULE_F_CHECK,
                 *RULE_A_CHECK_2, *MOD_RULE_G_3_CHECK],
    }),

    50,

    # Inclusion in groups (does not work in supergroups)
    ['dev:g2', f'members:bot'],

    functools.partial(check_database, expected={
        'chat': [*CHAT_C1, *CHAT_S1, *CHAT_S2, *CHAT_G3, *CHAT_S3, *CHAT_G2],
        'rule': [*RULE_B_CHECK, *RULE_D_CHECK, *RULE_F_CHECK,
                 *RULE_A_CHECK_2, *MOD_RULE_G_3_CHECK],
    }),

    51,

    # Chat where the bot is no longer present, is not displayed in the menu

    functools.partial(edit_consent, cid=G2ID, value=False),

    ['adm', '/menu'],
    ['bot', f'{CAN_CANCEL}{CHAT_CONFIG}', [f'1{ITEM}C1', f'2{ITEM}S1', f'3{ITEM}S2']],
    ['adm', '/cancel'],
    ['bot', CANCELED],

    functools.partial(check_database, expected={
        'chat': [*CHAT_C1, *CHAT_S1, *CHAT_S2, *CHAT_G3, *CHAT_S3],
        'rule': [*RULE_B_CHECK, *RULE_D_CHECK, *RULE_F_CHECK,
                 *RULE_A_CHECK_2, *MOD_RULE_G_3_CHECK],
    }),

    functools.partial(edit_consent, cid=G2ID),              # reset consent

    52,

    # ... apart from not appearing in the menu,
    # the chat and its rules must be removed from the database

    functools.partial(edit_consent, cid=S1ID, value=False),

    ['adm', '/menu'],
    ['bot', f'{CAN_CANCEL}{CHAT_CONFIG}', [f'1{ITEM}C1', f'2{ITEM}S2']],
    ['adm', '/cancel'],
    ['bot', CANCELED],

    functools.partial(check_database, expected={
        'chat': [*CHAT_C1, *CHAT_S2, *CHAT_G3, *CHAT_S3],
        'rule': [*RULE_B_CHECK, *RULE_D_CHECK, *RULE_F_CHECK,
                 *MOD_RULE_G_3_CHECK],
    }),

    functools.partial(edit_consent, cid=S1ID),              # reset consent
)


# Function that sends text to a chat that doesn't exist, throwing the error
# so that the error handler of the telegram library can process it
@core.flogger
@core.context
def erroneous_handler(ctx):
    ctx.send(chat_id=1, text='text', throw_exc=True)


class TestBot(unittest.TestCase):


    def setUp(self):
        self.maxDiff = None     # pylint: disable=invalid-name
        CFG.__dict__.clear()    # pylint: disable=protected-access

        # Logging enabled and configured
        logging.disable(0)
        logging.basicConfig(filename='unittest.testbot.log',
                            level=logging.DEBUG,
                            format=CFG.log_format,
                            datefmt=CFG.DATETIME_FORMAT)
        logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)
        core.only_bot_logging()

        # Bot and Server Initialization
        core.context.initialize()
        tgs = TelegramServer()
        tgs.set_states(STATUS_TABLE)
        tgs.add_handlers(core.add_handlers)
        self.bio = BotIOAdapter(self, tgs)

        # Extra handlers
        tgs.dispatcher.add_handler(CommandHandler('erroneous', erroneous_handler))


    def tearDown(self):
        # Shutdown and disabled
        logging.disable()


    @skipIf(UNTESTED_BOT, 'untested bot')
    @mock.patch('indexius.core.Session', side_effect=mocked_session)
    @mock.patch('indexius.core.socket.create_connection', side_effect=mocked_connection)
    def test_bot(self, _connection, _session):
        # The tests are planned for a limit of 4 rules per chat
        self.assertEqual(CFG.LIMIT_RULE_NUMBER, 4)
        # Run test
        self.bio.run(INTERACTIONS)
