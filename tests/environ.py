# -*- coding: UTF-8 -*-
# Copyright (C) 2019 Schmidt Cristian Hernán

# pylint: disable=unused-import,wrong-import-position,wrong-import-order

import sys
assert sys.hexversion >= 0x03060000, 'requires python 3.6 or higher'

import os
import re
import socket
import urllib
import pprint
import logging

import unittest
from unittest import mock, skipIf

from requests import Request, Response
from requests.exceptions import InvalidURL, RetryError
from urllib3.exceptions import MaxRetryError, ResponseError

from tgs import DEVID, TOKEN

UNTESTED_BOT = os.environ.get('UNTESTED_BOT', 'false').lower() == 'true'
URL_CLEAN_RE = re.compile(r'(^\s*https?:/+|/*\s*$)', re.IGNORECASE)

ENVIRON = {
    'TELEGRAM_USERNAME': 'bot',
    'TELEGRAM_TOKEN': TOKEN,
    'TELEGRAM_FID': DEVID,
    'DATABASE_URL': 'sqlite://',
    'HOST': 'https://bot.io',
    'PORT': '443',
    'BIND': '0.0.0.0',
    'RELAX_TIME': '0',          # everything is completely synchronous (one thread)
    'DATETIME_IN_LOG': 'true',
    'CACHING_ADM_TIME': '0.1',
    'TIMEOUT_EDIT_CHAT': '99',  # avoid problems if the system is too busy
}
mock.patch.object(os, 'environ', ENVIRON).start()

import telegram.ext
mock.patch.object(telegram.ext, 'run_async', lambda func: func).start()

sys.path.insert(0, '..')
from indexius import cache
from indexius import config
from indexius import context
from indexius import core
from indexius import database
from indexius import debug
from indexius import models
from indexius import texts
from indexius import tools
from indexius import version

CFG = config.CFG

logging.disable()


class EmptyGenericClass:
    # pylint: disable=too-few-public-methods

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)


def get_response(content, url):
    # pylint: disable=protected-access

    request = Request()
    request.url = url
    request.body = b''
    request.method = 'GET'
    request.headers = {}

    response = Response()
    response.url = url
    response.reason = 'OK'
    response.status_code = 200
    response.headers = {}
    response.encoding = 'utf-8'
    response.request = request
    response._content_consumed = True
    response._content = content.encode() if hasattr(content, 'encode') else content

    return response


def mocked_connection(address, timeout=None, source_address=None):
    host, port = address

    if host.startswith('ok.') or host.startswith('error.'):
        return mock.Mock()

    if host.startswith('raise.'):
        raise socket.error(host)

    raise NotImplementedError(address)


MOCKED_SESSION_REQUEST_HISTORY = []


def mocked_session(*args, **kwargs):
    # pylint: disable=no-self-use,no-self-argument,no-method-argument,unused-argument

    class MockSession:

        def mount(*args, **kwargs):
            pass

        def close(*args, **kwargs):
            pass

        def request(method, url, **kwargs):
            # pylint: disable=protected-access,no-member

            params = urllib.parse.urlencode(kwargs.get('params', {}), doseq=True)
            if params:
                sep = '&' if '?' in url else '?'
                url = f'{url}{sep}{params}'

            MOCKED_SESSION_REQUEST_HISTORY.append(f'{method.upper()}:{url}')

            response = get_response('', url)
            url = URL_CLEAN_RE.sub('', url)

            if url.startswith('ok.bool'):
                response._content = b'true'

            elif url.startswith('ok.json'):
                response._content = b'{"ok":true}'

            elif url.startswith('error.text'):
                response._content = b'"fail"'

            elif url.startswith('error.json'):
                response._content = b'{"error":"fail"}'

            elif url.startswith('error.unauthorized'):
                response._content = b'{"message":"unauthorized"}'
                response.status_code = 401
                response.reason = 'Unauthorized'

            elif url.startswith('raise.invalid_url'):
                raise InvalidURL("invalid url")

            elif url.startswith('raise.response_error'):
                err = ResponseError('response error')
                err = MaxRetryError(None, url, err)
                raise RetryError(err)

            elif not url.startswith('ok.'):
                raise NotImplementedError(url)

            return response

    return MockSession


class BotIOAdapter:
    """ Adapts test data to TelegramServer system """

    UN = r'[a-zA-Z]\w*'  # "UserName" for user and chats
    HEADER = re.compile(fr'^(?P<user>{UN})(:(?P<chat>{UN}))?$')

    def __init__(self, test, tgs):
        self.last_chat = None  # the test set is in series (no multi-threading)
        self.bookmark = 0      # useful for finding the origin of one error in dev
        self.history = []      # current execution sequence (from the last bookmark)
        self.test = test       # unittest.TestCase class instance
        self.tgs = tgs         # TelegramServer class instance

    def run(self, interactions):
        """ Execution of integration tests """
        interaction = None
        try:
            for interaction in interactions:

                if hasattr(interaction, '__call__'):
                    interaction(core=core, test=self.test, tgs=self.tgs)

                elif isinstance(interaction, (int, float)):
                    self.test.assertTrue(self.tgs.empty())

                    # Avoid typing errors in the numerical sequence of tests
                    delta = interaction - self.bookmark
                    if not 0 <= delta < 2:
                        raise ValueError('errors in bookmark')

                    self.bookmark = interaction
                    self.history.clear()

                else:
                    header, content, *optionals = interaction
                    header = self.HEADER.match(header).groupdict()

                    if not isinstance(content, re.Pattern):
                        content = str(content)

                    user = header['user']
                    chat = header['chat']
                    data = {'user': user, 'chat': chat, 'content': content}
                    self.set_optionals(data, optionals)

                    if user == 'bot':
                        # Bot Response
                        response = self.tgs.recv()
                        self.adjust(data, response)
                        self.test.assertEqual(data, response)
                    else:
                        # User request
                        self.tgs.send(data)
                        self.last_chat = chat or user

                    self.history.append(interaction)

            self.test.assertTrue(self.tgs.empty())

        except:
            print(f'\nbookmark {self.bookmark}')
            pprint.pprint(self.history)
            print('\n', len(self.history))
            pprint.pprint(interaction)
            for index in range(1, len(self.tgs.responses) + 1):
                print(f'\ntgs.recv {index}')
                pprint.pprint(self.tgs.recv())
            print('\n')
            raise

    @staticmethod
    def set_optionals(data, optionals):
        for opt in optionals:
            if isinstance(opt, str):
                data['caption'] = opt
            elif isinstance(opt, list):
                data['markup'] = opt

    def adjust(self, data, response):

        if not data['chat']:
            data['chat'] = self.last_chat

        for key, value in data.items():
            if isinstance(value, re.Pattern):
                if value.match(response[key]):
                    data[key] = ''
                    response[key] = ''
