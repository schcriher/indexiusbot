# -*- coding: UTF-8 -*-
# Copyright (C) 2019 Schmidt Cristian Hernán

from environ import unittest, texts


class TestTexts(unittest.TestCase):

    def test_sec_to_text(self):
        data = (
            (0.1, '0.1 segundos'),
            (0, '0 segundos'),
            (1, '1 segundo'),
            (2, '2 segundos'),
            (60, '1 minuto'),
            (120, '2 minutos'),
            (121, '2 minutos y 1 segundo'),
            (3600, '1 hora'),
            (3610, '1 hora y 10 segundos'),
            (3699, '1 hora, 1 minuto y 39 segundos'),
            (3999, '1 hora, 6 minutos y 39 segundos'),
            (9999, '2 horas, 46 minutos y 39 segundos'),
            (99999, '1 día, 3 horas, 46 minutos y 39 segundos'),
        )
        for seconds, expected in data:
            with self.subTest(seconds=seconds, expected=expected):
                self.assertEqual(texts.sec_to_text(seconds), expected)
