# -*- coding: UTF-8 -*-
# Copyright (C) 2019 Schmidt Cristian Hernán

import json
import datetime

from environ import unittest, CFG, tools, get_response

# re:
#   \w → [a-zA-Z0-9_]
#   \s → [ \t\n\r\f\v]
#   \d → [0-9]


DICT_DATA = (({}, ''),
             ({'error': 'ERROR'}, 'ERROR'),
             ({'error': 'description'}, 'description'),
             ({'errors': ['name']}, 'name'),
             ({'errors': {'description': 'error??'}}, 'error??'),
             ({'result': [{'name': 'Stark'}, {'message': 'meSSage'}]}, 'meSSage'),
             ({'result': {'error': {'name': 'Stark', 'result': 'ReSuLt'}}}, 'ReSuLt'),
             ({'result': {'error': {'name': 'Stark'}}}, ''),
             ({'result': {'name': {'name': 'Stark'}}}, ''),
             ({'message': 'MEssAGE'}, 'MEssAGE'),
             ({'description': {'name': 'Stark'}}, ''))


class TestTools(unittest.TestCase):


    def test_regex_line_re(self):
        data = (
            (r' \s+', r' ', r'\s+'),
            (r'  \s+', r' ', r'\s+'),
            (r'  \s+ ', r' ', r'\s+'),
            (r'd \d+', r'd', r'\d+'),
            (r'd  \d+', r'd', r'\d+'),
            (r'd  \d+  ', r'd', r'\d+'),
            (r'  \s+ \w', r' ', r'\s+ \w'),
            (r'd  \d+ \D*', r'd', r'\d+ \D*'),
        )
        for entry, alias_expected, value_expected in data:
            with self.subTest(entry=entry):
                obj = tools.REGEX_LINE_RE.search(entry)
                alias = obj.group(1) or ' '
                value = obj.group(3)
                self.assertEqual(alias, alias_expected)
                self.assertEqual(value, value_expected)
        data = (
            r'\s+',
            r'\s+ ',
            r'\s+   ',
        )
        for entry in data:
            with self.subTest(entry=entry):
                self.assertFalse(bool(tools.REGEX_LINE_RE.match(entry)))


    def test_no_word_re(self):
        char = 'ø'
        data = (
            ('test: 1', f'test{char}1'),
            ('_test: 1', f'_test{char}1'),
            ('test?? 1ñ', f'test{char}1ñ'),
            ('(test./ 1)', f'{char}test{char}1{char}'),
            ('tést 1', f'tést{char}1'),
        )
        for entry, expected in data:
            with self.subTest(entry=entry, expected=expected):
                self.assertEqual(tools.NO_WORD_RE.sub(char, entry), expected)


    def test_number_re(self):

        data = ('0', '0.', '1', '20.', '.1', '.12', '10', '999', '1,2', ',2')
        for value in data:
            with self.subTest(case='number', value=value):
                self.assertTrue(bool(tools.NUMBER_RE.match(value)))

        data = ('0a', 'a0.', 'b1', 'b20.', 'text', 'a,b', '1,a')
        for value in data:
            with self.subTest(case='no number', value=value):
                self.assertFalse(bool(tools.NUMBER_RE.match(value)))


    def test_key_ok_re(self):
        data = (
            'ok', 'Ok', 'OK',
            'result', 'Result', 'RESULT', 'resuLt',
        )
        for entry in data:
            with self.subTest(entry=entry):
                self.assertTrue(bool(tools.KEY_OK_RE.match(entry)))
        data = (
            'ot',
            'test',
            'error',
            'success',
        )
        for entry in data:
            with self.subTest(entry=entry):
                self.assertFalse(bool(tools.KEY_OK_RE.match(entry)))


    def test_val_ok_re(self):
        data = (
            'ok', 'Ok', 'OK',
            'true', 'True', 'TRUE',
            'SUCCESS', 'success', 'suces', 'suCCess',
            'yes', 'YES',
            '1', '1.', '1.0', '1.000',
        )
        for entry in data:
            with self.subTest(entry=entry):
                self.assertTrue(bool(tools.VAL_OK_RE.match(entry)))
        data = (
            'ot',
            'test',
            'error',
            'RESULT',
            'xyz',
        )
        for entry in data:
            with self.subTest(entry=entry):
                self.assertFalse(bool(tools.VAL_OK_RE.match(entry)))


    def test_key_er_re(self):
        data = (
            'error', 'Error', 'ERROR', 'eror', 'EROR',
            'message', 'mesage', 'Message', 'MESSAGE', 'MESAGE',
            'result', 'results', 'Result', 'RESULT', 'resuLts',
            'description', 'DESCRIPTION', 'Description',
            'descrip', 'descript', 'Descript',
        )
        for entry in data:
            with self.subTest(entry=entry):
                self.assertTrue(bool(tools.KEY_ER_RE.match(entry)))
        data = (
            'descripion',
            'ok', 'Ok', 'OK', 'ot',
            'test',
            'success',
        )
        for entry in data:
            with self.subTest(entry=entry):
                self.assertFalse(bool(tools.KEY_ER_RE.match(entry)))


    def test_yes_re(self):
        data = (
            'yes', 'ya', 'ye', 'yi',
            'sa', 'se', 'si',
            'ta', 'ja',
            'ok',
            'oui',
            'если',
            'ναι',
        )
        for entry in data:
            with self.subTest(entry=entry):
                self.assertTrue(bool(tools.YES_RE.match(entry)))
        data = (
            'no', 'not',
            'may be',
            'success',
            'error',
            'нет',
            'όχι',
            'pas',
        )
        for entry in data:
            with self.subTest(entry=entry):
                self.assertFalse(bool(tools.YES_RE.match(entry)))


    def test_https_re(self):
        char = ''
        data = (
            ('http://test.io/', 'test.io'),
            ('Http://test.io/', 'test.io'),
            ('https://test.io/', 'test.io'),
            ('HTTPS://test.io/', 'test.io'),
            ('http://test.io//', 'test.io'),
            ('https://test.io///', 'test.io'),
            ('http://test.io/api', 'test.io/api'),
            ('http://test.io//api///', 'test.io//api'),
            ('http://test.io/api/?v=1', 'test.io/api/?v=1'),
        )
        for entry, expected in data:
            with self.subTest(entry=entry, expected=expected):
                self.assertEqual(tools.HTTPS_RE.sub(char, entry), expected)


    def test_id_sep(self):
        data = (
            ("123 yes", '', '123', ' ', 'yes'),
            ("123 yes ", '', '123', ' ', 'yes '),
            ("123  yes", '', '123', '  ', 'yes'),
            ("123 yes no", '', '123', ' ', 'yes no'),
            ("-123 yes", '', '-123', ' ', 'yes'),
            ("-123 yes no", '', '-123', ' ', 'yes no'),
            ("'123' yes", '\'', '123', ' ', 'yes'),
            ("'123' yes no", '\'', '123', ' ', 'yes no'),
            ("'-123' yes", '\'', '-123', ' ', 'yes'),
            ("'-123' yes no", '\'', '-123', ' ', 'yes no'),
            ("\"123\" yes", '"', '123', ' ', 'yes'),
            ("\"123\" yes no", '"', '123', ' ', 'yes no'),
            ("\"-123\" yes", '"', '-123', ' ', 'yes'),
            ("\"-123\" yes no", '"', '-123', ' ', 'yes no'),
            ("'abc def' yes", '\'', 'abc def', ' ', 'yes'),
            ("'abc def' yes no", '\'', 'abc def', ' ', 'yes no'),
            ("\"abc def\" yes", '"', 'abc def', ' ', 'yes'),
            ("\"abc def\" yes no", '"', 'abc def', ' ', 'yes no'),
        )
        for entry, *expected in data:
            with self.subTest(entry=entry, expected=expected):
                self.assertEqual(list(tools.ID_SEP.match(entry).groups()), expected)


    def test_querydict(self):
        qdic = tools.QueryDict()
        self.assertEqual(qdic, {})
        data = (
            ('a', 1, {'a': 1}),
            ('b', 2, {'a': 1, 'b': 2}),
            ('a', 11, {'a': [1, 11], 'b': 2}),
            ('c', 'a', {'a': [1, 11], 'b': 2, 'c': 'a'}),
            ('a', '111', {'a': [1, 11, 111], 'b': 2, 'c': 'a'}),
            ('a', '111a', {'a': [1, 11, 111, '111a'], 'b': 2, 'c': 'a'}),
        )
        for key, value, expected in data:
            with self.subTest(method='__setitem__', key=key, value=value):
                qdic[key] = value
                self.assertEqual(qdic, expected)

        time = datetime.datetime(2019, 12, 2, 3, 20, 8)
        stamp = int(time.timestamp())
        dic = {'a': 1, 'b': 2.5, 'c': stamp}

        qdic = tools.QueryDict()
        qdic.update(a='1', b='2.5', c=time)
        self.assertEqual(qdic, dic)

        qdic = tools.QueryDict(a='1', b='2.5', c=time)
        self.assertEqual(qdic, dic)

        qdic = tools.QueryDict()
        qdic.update([('a', '1'), ('b', '2.5'), ('c', time)])
        self.assertEqual(qdic, dic)

        qdic = tools.QueryDict([('a', '1'), ('b', '2.5'), ('c', time)])
        self.assertEqual(qdic, dic)

        qdic = tools.QueryDict()
        qdic.update(dic)
        self.assertEqual(qdic, dic)


    def test_pattern_normalizer(self):
        data = (
            (r'\s', r'\s'),
            (r'\s*', r'\s*'),
            (r'\s+', r'\s+'),

            (r'\s\s\s', r'\s+'),
            (r'\s*\s*\s*', r'\s*'),
            (r'\s+\s+\s+', r'\s+'),

            (r'\s\s*\s+', r'\s+'),

            (r'\s\s*', r'\s+'),
            (r'\s\s+', r'\s+'),
            (r'\s*\s+', r'\s+'),

            (r'\s-\s+', r'\s-\s+'),
            (r'\s\s+\s-\s\s+', r'\s+-\s+'),
            (r'\s\s*\s-\s*\s*', r'\s+-\s*'),

            # ---

            (r'\S', r'\S'),
            (r'\S*', r'\S*'),
            (r'\S+', r'\S+'),

            (r'\S\S\S', r'\S+'),
            (r'\S*\S*\S*', r'\S*'),
            (r'\S+\S+\S+', r'\S+'),

            (r'\S\S*\S+', r'\S+'),

            (r'\S\S*', r'\S+'),
            (r'\S\S+', r'\S+'),
            (r'\S*\S+', r'\S+'),

            (r'\S-\S+', r'\S-\S+'),
            (r'\S\S+\S-\S\S+', r'\S+-\S+'),
            (r'\S\S*\S-\S*\S*', r'\S+-\S*'),

            # ---

            (r'\s\S', r'\s\S'),
            (r'\s*\S*', r'\s*\S*'),
            (r'\s+\S+', r'\s+\S+'),

            (r'\s\s\S', r'\s+\S'),
            (r'\s\S\S', r'\s\S+'),
            (r'\s+\S*\S', r'\s+\S+'),
            (r'\s+\S+\S', r'\s+\S+'),
            (r'\s\S\S-\S+\s', r'\s\S+-\S+\s'),

            # ---

            (r'\s*\s*\h*\h*\s+\s*', r'\s*\h*\s+'),
            (r'\\s*\s*\h*\h*\s+\s*', r'\\s*\s*\h*\s+'),
            (r'\\s*\s*\s*\h*\h*\s+\s*', r'\\s*\s*\h*\s+'),
            (r'\\s*\s*\s+\h*\h*\s+\s*', r'\\s*\s+\h*\s+'),
        )
        for entry, expected in data:
            with self.subTest(entry=entry, expected=expected):
                entry = tools.PATTERN_RE.sub(tools.pattern_normalizer, entry)
                self.assertEqual(entry, expected)


    def test_get_limited_name(self):
        self.assertEqual(CFG.LIMIT_NAME_LENGTH, 32)
        data = (
            ('', ''),
            ('name', 'name'),
            ('Bran Stark', 'Bran Stark'),
            ('https://test.io/', 'https://test.io/'),
            ('text to test the maximum length allowed',
             'text to test the maximum length…'),
        )
        for entry, expected in data:
            with self.subTest(entry=entry, expected=expected):
                self.assertEqual(tools.get_limited_name(entry), expected)


    def test_get_arg(self):
        data = (
            ('', ''),
            (None, ''),
            ('text', ''),
            ('/', ''),
            ('/cmd', ''),
            ('/cmd ', ''),
            ('/ cmd ', ''),
            ('/cmd a', 'a'),
            ('/cmd text', 'text'),
            ('/cmd text ', 'text'),
            ('/cmd text for testing ', 'text for testing'),
            ('cmd text for testing ', ''),
        )
        for entry, expected in data:
            with self.subTest(entry=entry, expected=expected):
                self.assertEqual(tools.get_arg(entry), expected)


    def test_extract_error_from_dict(self):
        for entry, expected in DICT_DATA:
            with self.subTest(entry=entry, expected=expected):
                self.assertEqual(tools.extract_error_from_dict(entry), expected)


    def test_extract_errors_from_response(self):
        url = 'unit.test'
        data = (
            ('', ''),
            ({'ok': 'true'}, ''),
            ({'result': 'success'}, ''),
            ('ok', ''),
            (['error'], 'error'),
            ([1, 2, 3], ''),
            ({'error': 'erROR'}, 'erROR'),
        )
        for entry, expected in DICT_DATA + data:
            with self.subTest(entry=entry, expected=expected):
                response = get_response(json.dumps(entry), url)
                error = tools.extract_errors_from_response(response)
                self.assertEqual(error, expected)

        data = ('1', '{1}', '', b'', b'{1}', b'<a>1</a>')
        for entry in data:
            with self.subTest(entry=entry):
                response = get_response(entry, url)
                error = tools.extract_errors_from_response(response)
                self.assertEqual(error, '')


    def test_pseudo_text_length(self):
        data = (                    # total = norm + slim (slim // 2)
            ('✒️ Agregar', 7),      #     9      6      3           1
            ('🔧 Modificar', 8),    #    11      6      5           2
            ('🧹 Eliminar', 7),     #    10      5      5           2
            ('🔦 Listar', 6),       #     8      4      4           2
            ('🔨 Probar', 6),       #     8      5      3           1
        )
        # length = norm + slim // 2
        for text, length in data:
            with self.subTest(text=text, length=length):
                self.assertEqual(tools.pseudo_text_length(text), length)


    def test_remove_diacritics(self):
        data = (
            ('Hernán', 'Hernan'),
            ('schön', 'schon'),
            ('lEçon', 'lEcon'),
            ('áíóúý', 'aiouy'),
            ('Año 3', 'Ano 3'),
            ('Conceição', 'Conceicao'),
        )
        for entry, expected in data:
            with self.subTest(entry=entry, expected=expected):
                self.assertEqual(tools.remove_diacritics(entry), expected)


    def test_get_positions(self):
        data = (
            (('Python', 'P', 0), [0]),
            (('Python', 'P', 5), [5]),
            (('PythonTest', 't', 0), [2, 9]),
            (('PythonTest', 't', -1), [1, 8]),
            (('Telegram_test', '', 0), []),
            (('Telegram_test', 'Z', 10), []),
            (('', '', 0), []),
        )
        for entry, expected in data:
            with self.subTest(entry=entry, expected=expected):
                result = list(tools.get_positions(*entry))
                self.assertEqual(result, expected)


    def test_texdiff(self):
        data = (
            #text_a, text_b, insensitive, accents, onlyword
            (('Python', 'Python', False, False, False), 0),
            (('Python', 'Python', True, False, False), 0),
            (('Python', 'Python', False, True, True), 0),
            (('ABC', 'abc', False, False, False), 1),
            (('ABC', 'abc', True, False, False), 0),
            (('Conceição', 'conceicao', False, False, False), 1/9),
            (('Conceição', 'conceicao', True, False, False), 0),
            (('Conceição', 'conceicao', False, True, False), 1/3),
            (('ABC!!=', 'abc??:', False, False, False), 1),
            (('ABC!!=', 'abc??:', True, False, False), 1/2),
            (('ABC!!=', 'abc??:', True, False, True), 0),
        )
        for entry, expected in data:
            with self.subTest(entry=entry, expected=expected):
                result = tools.texdiff(*entry)
                self.assertEqual(result, expected)
