# -*- coding: UTF-8 -*-
# Copyright (C) 2019 Schmidt Cristian Hernán

from environ import unittest, mock, os, config, CFG, version

USERNAME = 'Indexius_Bot'
BOTNAME = USERNAME.lower().rstrip('bot').rstrip('_').capitalize()
VERSION = 'X.Y'

BID = 111
FID = 222

TOKEN = f'{BID}:abc'
BOT_URL = f'https://t.me/{USERNAME}'
USER_AGENT = f'{BOTNAME}/{VERSION} ({BOT_URL})'

ENVIRON = {
    'TELEGRAM_USERNAME': USERNAME,
    'TELEGRAM_TOKEN': TOKEN,
    'TELEGRAM_FID': str(FID),
    'a': '1.0',
    'b': '1.5',
    'c': 'true',
    'd': '10.0',
    'e': '20.0',
    'f': 'no',
    'internal': 'ENVIRON',  # is ignored, because it is present in INTERNAL
    'wrong_boolean': 'text',  # must be: true, 1, yes, false, 0, no (case-insensitive)
    'key_without_default': 'text',
}

DEFAULT = {
    'TELEGRAM_USERNAME': str,
    'TELEGRAM_TOKEN': str,
    'TELEGRAM_FID': int,
    'LOG_TIME': 'LOG_TIME',
    'LOG_LINE': 'LOG_LINE',
    'a': int,
    'b': float,
    'c': bool,
    'd': 1,
    'e': 2.0,
    'f': True,
    'g': 'no environ',
    'internal': 'DEFAULT',  # is ignored, because it is present in INTERNAL
    'wrong_boolean': bool,
    'type_without_environ': str,  # as it is not present in ENVIRON
                                  # it must have a value and not a type
}

INTERNAL = {
    'internal': 'INTERNAL',
}

class TestConfig(unittest.TestCase):

    def setUp(self):
        # pylint: disable=protected-access
        CFG.__dict__.clear()

    def tearDown(self):
        # pylint: disable=protected-access
        CFG.__dict__.clear()

    @mock.patch.object(os, 'environ', ENVIRON)
    @mock.patch.object(config, 'DEFAULT', DEFAULT)
    @mock.patch.object(config, 'INTERNAL', INTERNAL)
    @mock.patch.object(version, 'VERSION', VERSION)
    def test_cfg(self):

        self.assertEqual(CFG.bot_url, BOT_URL)
        self.assertEqual(CFG.user_agent, USER_AGENT)
        self.assertEqual(CFG.bid, BID)
        self.assertEqual(CFG.fid, FID)

        CFG.DATETIME_IN_LOG = True
        self.assertEqual(CFG.log_format, 'LOG_TIME LOG_LINE')
        CFG.DATETIME_IN_LOG = False
        self.assertEqual(CFG.log_format, 'LOG_LINE')

        self.assertEqual(CFG.TELEGRAM_USERNAME, USERNAME)
        self.assertEqual(CFG.TELEGRAM_TOKEN, TOKEN)
        self.assertEqual(CFG.TELEGRAM_FID, FID)

        data = (
            (CFG.a, 1),
            (CFG.b, 1.5),
            (CFG.c, True),
            (CFG.d, 10),
            (CFG.e, 20.0),
            (CFG.f, False),
            (CFG.g, 'no environ'),
        )
        for value, expected in data:
            with self.subTest(value=value, expected=expected):
                self.assertEqual(value, expected)
                self.assertEqual(type(value), type(expected))

        self.assertEqual(CFG.internal, 'INTERNAL')

        with self.assertRaises(ValueError):
            _ = CFG.wrong_boolean

        with self.assertRaises(KeyError):
            _ = CFG.key_without_default

        with self.assertRaises(KeyError):
            _ = CFG.type_without_environ
