# -*- coding: UTF-8 -*-
# Copyright (C) 2019 Schmidt Cristian Hernán

import re
import copy
import json
import time
import unittest

from unittest import mock

from telegram.error import BadRequest

from tgs import (TOKEN, BOTID, DEVID, ADMID, USRID, G1ID, S1ID, C1ID, G2ID, S2ID,
                 C2ID, G3ID, S3ID, C3ID, is_channel, adjust_offsets,
                 extract_entities_html, introduce_entities_html, extract_markup,
                 get_callback_data, return_deepcopy, dict_to_bytes, remove_keyboard,
                 make_dict, get_int, get_author_signature, check_message,
                 STATUS, USERS, CHATS, NICKS, CONSENT, PHOTO, MEDIA_MAP,
                 HandlersDict, TelegramServer)

BOT_G1 = {'from': USERS[BOTID], 'chat': CHATS[G1ID]}
BOT_DEV = {'from': USERS[BOTID], 'chat': CHATS[DEVID]}
DEV_DEV = {'from': USERS[DEVID], 'chat': CHATS[DEVID]}
DEV_G1 = {'from': USERS[DEVID], 'chat': CHATS[G1ID]}
DEV_S1 = {'from': USERS[DEVID], 'chat': CHATS[S1ID]}
DEV_C1 = {'author_signature': get_author_signature(USERS[DEVID]), 'chat': CHATS[C1ID]}

MARKUP_KB_TEXT = ['1', '2', '3', '4']
MARKUP_KB_DICT = {'keyboard': [['1', '2'], ['3', '4']],
                  'resize_keyboard': True,
                  'one_time_keyboard': True,
                  'selective': False}

MARKUP_IKB_TEXT = ['A1', 'A2', 'A3']
MARKUP_IKB_DATA = ['01', '02', '03']
MARKUP_IKB_DICT = {'inline_keyboard': [[{'text': 'A1', 'callback_data': '01'},
                                        {'text': 'A2', 'callback_data': '02'}],
                                       [{'text': 'A3', 'callback_data': '03'}]]}

ENTITIES_DATA = (
    ('@text', '@text', [{'type': 'mention', 'offset': 0, 'length': 5}]),
    ('#text', '#text', [{'type': 'hashtag', 'offset': 0, 'length': 5}]),
    ('/text', '/text', [{'type': 'bot_command', 'offset': 0, 'length': 5}]),
    ('<b>text</b>', 'text', [{'type': 'bold', 'offset': 0, 'length': 4}]),
    ('<strong>text</strong>', 'text', [{'type': 'bold', 'offset': 0, 'length': 4}]),
    ('<i>text</i>', 'text', [{'type': 'italic', 'offset': 0, 'length': 4}]),
    ('<em>text</em>', 'text', [{'type': 'italic', 'offset': 0, 'length': 4}]),
    ('<code>text</code>', 'text', [{'type': 'code', 'offset': 0, 'length': 4}]),
    ('<pre>text</pre>', 'text', [{'type': 'pre', 'offset': 0, 'length': 4}]),
    ('@aaa#bbb', '@aaa#bbb', [{'type': 'mention', 'offset': 0, 'length': 4},
                              {'type': 'hashtag', 'offset': 4, 'length': 4}]),
    ('/a @b <b>c</b> #d', '/a @b c #d',
     [{'type': 'bot_command', 'offset': 0, 'length': 2},
      {'type': 'mention', 'offset': 3, 'length': 2},
      {'type': 'bold', 'offset': 6, 'length': 1},
      {'type': 'hashtag', 'offset': 8, 'length': 2}]),
    ('<code>/a</code> @b', '/a @b',
     [{'type': 'code', 'offset': 0, 'length': 2},
      {'type': 'mention', 'offset': 3, 'length': 2}]),
    ('<i>a</i> <i>b</i> <code>c</code> <i>d</i>', 'a b c d',
     [{'type': 'italic', 'offset': 0, 'length': 1},
      {'type': 'italic', 'offset': 2, 'length': 1},
      {'type': 'code', 'offset': 4, 'length': 1},
      {'type': 'italic', 'offset': 6, 'length': 1}]),
)


def not_doing_anything(_):
    pass


class TestTgsFunctions(unittest.TestCase):

    maxDiff = None  # pylint: disable=invalid-name


    def test_is_channel(self):
        data = (
            # message
            ({'chat': {'type': 'channel'}}, True),
            ({'chat': {'type': 'group'}}, False),
            ({'chat': {'type': 'supergroup'}}, False),
            # chat
            ({'type': 'channel'}, True),
            ({'type': 'group'}, False),
            ({'type': 'supergroup'}, False),
        )
        for dic, expected, in data:
            with self.subTest(dic=dic, expected=expected):
                self.assertEqual(is_channel(dic), expected)
        data = (
            ({'chat': {'id': 'channel'}}, KeyError),
            ({'chat': {'id': 'group'}}, KeyError),
            ({'chat': {'id': 'channel'}}, KeyError),
            ({'id': 'channel'}, ValueError),
            ({'id': 'group'}, ValueError),
            ({'id': 'channel'}, ValueError),
            ({}, ValueError),
            (123, TypeError),
            (object(), TypeError),
        )
        for obj, error in data:
            with self.subTest(obj=obj):
                with self.assertRaises(error):
                    is_channel(obj)


    def test_adjust_offsets(self):
        entities = [
            {'type': 'typ', 'offset': 0, 'length': 3},
            {'type': 'typ', 'offset': 8, 'length': 3},
        ]
        expected_entities = [
            {'type': 'typ', 'offset': 0, 'length': 3},
            {'type': 'typ', 'offset': 9, 'length': 3},
        ]
        adjust_offsets(entities, 5, 1)
        self.assertEqual(entities, expected_entities)


    def test_extract_entities_html(self):
        for text, expected_text, expected_entities in ENTITIES_DATA:
            with self.subTest(text=text):
                obtained_text, obtained_entities = extract_entities_html(text)
                self.assertEqual(obtained_text, expected_text)
                self.assertEqual(obtained_entities, expected_entities)


    def test_introduce_entities_html(self):
        fix_bold = re.compile(r'<(/)?strong>')
        fix_italic = re.compile(r'<(/)?em>')
        for expected_text, text, entities in ENTITIES_DATA:
            expected_text = fix_bold.sub(r'<\1b>', expected_text)
            expected_text = fix_italic.sub(r'<\1i>', expected_text)
            with self.subTest(expected_text=expected_text):
                obtained_text = introduce_entities_html(text, entities)
                self.assertEqual(obtained_text, expected_text)


    def test_extract_markup(self):
        self.assertEqual(extract_markup(MARKUP_KB_DICT), MARKUP_KB_TEXT)
        self.assertEqual(extract_markup(MARKUP_IKB_DICT), MARKUP_IKB_TEXT)

        with self.assertRaises(ValueError):
            extract_markup({'a': 1})


    def test_get_callback_data(self):
        message = {'reply_markup': MARKUP_IKB_DICT}
        for text, data in zip(MARKUP_IKB_TEXT, MARKUP_IKB_DATA):
            with self.subTest(text=text):
                self.assertEqual(get_callback_data(message, text), data)

        with self.assertRaises(KeyError):
            get_callback_data(message, 'a')

        with self.assertRaises(KeyError):
            message = {'a': 1}
            get_callback_data(message, 'a')

        with self.assertRaises(KeyError):
            message = {'reply_markup': {'inline_keyboard': 1}}
            get_callback_data({'a': 1}, 'a')


    def test_return_deepcopy(self):
        dic0 = {'list': [1, 2], 'dict': {'key': 'val'}}

        @return_deepcopy
        def func():
            return dic0

        dic1 = func()
        self.assertIsNot(dic1, dic0)
        self.assertIsNot(dic1['list'], dic0['list'])
        self.assertIsNot(dic1['dict'], dic0['dict'])


    def test_dict_to_bytes(self):
        dic = {'a': 1, 'b': [2, 3]}
        out = b'{"a": 1, "b": [2, 3]}'

        @dict_to_bytes
        def func():
            return dic

        self.assertEqual(func(), out)


    def test_remove_keyboard(self):
        data = (
            (123,
             123),

            (['reply_markup', 'keyboard'],
             ['reply_markup', 'keyboard']),

            ({'a': 1, 'reply_markup': {'b': 2}},
             {'a': 1, 'reply_markup': {'b': 2}}),

            ({'a': 1, 'reply_markup': {'keyboard': 2}},
             {'a': 1}),

            ({'a': 1, 'reply_markup': {'inline_keyboard': 2}},
             {'a': 1, 'reply_markup': {'inline_keyboard': 2}}),

            ({'a': 1, 'b': {'reply_markup': {'keyboard': 2}}},
             {'a': 1, 'b': {}}),

            ({'a': 1, 'b': {'reply_markup': {'inline_keyboard': 2}}},
             {'a': 1, 'b': {'reply_markup': {'inline_keyboard': 2}}}),
        )

        # Decorator
        for index, (in_obj, out_obj) in enumerate(data):
            with self.subTest(index=index):

                @remove_keyboard
                def func(obj):
                    return copy.deepcopy(obj)

                self.assertEqual(func(in_obj), out_obj)

        # Removal function
        for index, (in_obj, out_obj) in enumerate(data):
            with self.subTest(index=index):
                self.assertEqual(remove_keyboard(in_obj), out_obj)


    def test_make_dict(self):
        head = ('a', 'b', 'id')
        row = ('4', 3.0, 99999)
        expected = {'a': '4', 'b': 3.0, 'id': 99999}
        self.assertEqual(make_dict(head, row), expected)

        head = ('a', 'b', 'id')
        row = (True, None, 1)
        expected = {'a': True, 'b': None, 'id': 1}
        self.assertEqual(make_dict(head, row), expected)


    def test_get_int(self):
        data = (
            ('a', {'a': 1}, 1),
            ('a', {'a': '1'}, 1),
            ('b', {'a': 0}, None),
            ('c', None, None),
        )
        for var, dic, expected in data:
            with self.subTest(var=var):
                self.assertEqual(get_int(dic, var), expected)


    def test_get_author_signature(self):
        data = (
            ({'first_name': 'aaa', 'last_name': 'bbb', 'id': 1}, 'aaa bbb'),
            ({'first_name': 'aaa', 'last_name': None, 'id': 2}, 'aaa'),
            ({'first_name': None, 'last_name': 'bbb', 'id': 3}, 'bbb'),
            ({'first_name': 'aaa b', 'last_name': 'cC', 'id': 4}, 'aaa b cC'),
            ({'first_name': None, 'last_name': None, 'id': 5}, ''),
        )
        for dic, expected in data:
            with self.subTest(dic=dic):
                self.assertEqual(get_author_signature(dic), expected)

        data = (
            {'last_name': 'bbb', 'id': 6},
            {'first_name': 'aaa', 'id': 7},
            {'id': 8},
        )
        for dic in data:
            with self.subTest(dic=dic):
                with self.assertRaises(KeyError):
                    get_author_signature(dic)


    def test_check_message(self):
        msg_1 = {'message_id': 1}
        msg_2 = {'message_id': 2}
        msg_4 = {'message_id': 4}

        with self.subTest(dic='sorted messages'):
            self.assertIsNone(check_message([msg_1, msg_2, msg_4]))

        with self.subTest(dic='unsorted messages'):
            with self.assertRaises(IndexError):
                check_message([msg_1, msg_4, msg_2])

        with self.subTest(dic='duplicate messages'):
            with self.assertRaises(IndexError):
                check_message([msg_1, msg_2, msg_2])


    def test_global_variables(self):
        self.assertRegex(TOKEN, r'\d+:[\w+-]+')

        self.assertEqual(STATUS, {
            -10011: {11111: 'member', 22222: 'left', 33333: 'left', 44444: 'left'},
            -10012: {11111: 'member', 22222: 'left', 33333: 'left', 44444: 'left'},
            -10013: {11111: 'member', 22222: 'left', 33333: 'left', 44444: 'left'},
            -10021: {11111: 'member', 22222: 'left', 33333: 'left', 44444: 'left'},
            -10022: {11111: 'member', 22222: 'left', 33333: 'left', 44444: 'left'},
            -10023: {11111: 'member', 22222: 'left', 33333: 'left', 44444: 'left'},
            -10031: {11111: 'member', 22222: 'left', 33333: 'left', 44444: 'left'},
            -10032: {11111: 'member', 22222: 'left', 33333: 'left', 44444: 'left'},
            -10033: {11111: 'member', 22222: 'left', 33333: 'left', 44444: 'left'},
        })

        self.assertEqual(USERS, {
            11111: {'id': 11111, 'is_bot': True, 'username': 'bot',
                    'first_name': 'Cupidus', 'last_name': None},
            22222: {'id': 22222, 'is_bot': False, 'username': 'dev',
                    'first_name': 'Bran', 'last_name': 'Stark'},
            33333: {'id': 33333, 'is_bot': False, 'username': 'adm',
                    'first_name': 'Arya', 'last_name': 'Stark'},
            44444: {'id': 44444, 'is_bot': False, 'username': 'usr',
                    'first_name': 'Robb', 'last_name': 'Stark'},
        })

        sgt = 'supergroup'
        self.assertEqual(CHATS, {
            22222: {'id': 22222, 'username': 'dev', 'first_name': 'Bran',
                    'last_name': 'Stark', 'type': 'private'},
            33333: {'id': 33333, 'username': 'adm', 'first_name': 'Arya',
                    'last_name': 'Stark', 'type': 'private'},
            44444: {'id': 44444, 'username': 'usr', 'first_name': 'Robb',
                    'last_name': 'Stark', 'type': 'private'},
            -10011: {'id': -10011, 'title': 'G1', 'username': 'g1', 'type': 'group'},
            -10012: {'id': -10012, 'title': 'G2', 'username': 'g2', 'type': 'group'},
            -10013: {'id': -10013, 'title': 'G3', 'username': 'g3', 'type': 'group'},
            -10021: {'id': -10021, 'title': 'S1', 'username': 's1', 'type': sgt},
            -10022: {'id': -10022, 'title': 'S2', 'username': 's2', 'type': sgt},
            -10023: {'id': -10023, 'title': 'S3', 'username': 's3', 'type': sgt},
            -10031: {'id': -10031, 'title': 'C1', 'username': 'c1', 'type': 'channel'},
            -10032: {'id': -10032, 'title': 'C2', 'username': 'c2', 'type': 'channel'},
            -10033: {'id': -10033, 'title': 'C3', 'username': 'c3', 'type': 'channel'},
        })

        self.assertEqual(NICKS, {
            'bot': 11111,
            'Cupidus': 11111,
            'dev': 22222,
            'Bran Stark': 22222,
            'adm': 33333,
            'Arya Stark': 33333,
            'usr': 44444,
            'Robb Stark': 44444,
            'g1': -10011,
            'g2': -10012,
            'g3': -10013,
            's1': -10021,
            's2': -10022,
            's3': -10023,
            'c1': -10031,
            'c2': -10032,
            'c3': -10033
        })

        self.assertEqual(CONSENT, {
            11111: False,
            22222: False,
            33333: False,
            44444: False,
            -10011: True,
            -10012: True,
            -10013: True,
            -10021: True,
            -10022: True,
            -10023: True,
            -10031: True,
            -10032: True,
            -10033: True,
        })

        base = {'file_id': 'FILE_ID', 'file_size': 123}
        size = {'height': 123, 'width': 123}
        baze = {**base, **size}
        self.assertEqual(MEDIA_MAP, {
            'photo': {'photo': [baze, baze]},
            'sticker': {'sticker': {
                **baze,
                'emoji': '👍', 'is_animated': False, 'set_name': 'SET_NAME',
                'thumb': baze,
            }},
            'document': {'document': {
                **base,
                'thumb': baze,
                'file_name': 'FILE_NAME.pdf',
                'mime_type': 'document/pdf',
            }},
            'animation': {
                'document': {
                    **base,
                    'thumb': baze,
                    'file_name': 'FILE_NAME.mp4',
                    'mime_type': 'video/mp4'
                },
                'animation': {
                    **baze,
                    'thumb': baze,
                    'file_name': 'FILE_NAME.mp4',
                    'mime_type': 'video/mp4',
                    'duration': 1
                }
            },
        })


    @mock.patch('sys.exit')
    def test_handlersdict(self, _exit):
        dic = HandlersDict()

        self.assertEqual(dic, {})

        handler_0 = mock.Mock()
        handler_0.handle_update.side_effect = KeyError('KeyError')

        handler_1 = mock.Mock()
        handler_1.handle_update.side_effect = BadRequest('BadRequest')

        dic[0] = [handler_0, handler_1]

        self.assertEqual(dic, {0: [handler_0, handler_1]})
        self.assertIn(0, dic)

        lst = dic[0]

        lst[0].handle_update()

        with self.assertRaises(BadRequest):
            lst[1].handle_update()

        self.assertIs(handler_0.__wrapped__, True)
        self.assertIs(handler_1.__wrapped__, True)
        self.assertEqual(_exit.call_args_list, [mock.call('bot execution error')])


class TestTgsTelegramServer(unittest.TestCase):

    maxDiff = None  # pylint: disable=invalid-name

    def setUp(self):
        self.ini = int(time.time())
        self.tgs = TelegramServer()
        self.burl = f'{self.tgs.BASE_URL}{TOKEN}/'


    def check_message(self, message, expected, message_id=1):
        # If the message is not stored, the message_id is always 1

        self.assertEqual(message.pop('message_id'), message_id)

        now = int(time.time())
        for key in ('date', 'edit_date', 'forward_date'):
            diff = now - message.pop(key, now)
            self.assertGreaterEqual(diff, 0)
            self.assertLessEqual(diff, now - self.ini)

        # The rest must be the same
        self.assertEqual(message, expected)


    def check_update(self, update, expected, variable):
        full_dic = update.to_dict()
        self.assertIn(variable, full_dic)
        message = full_dic[variable]
        dic = {key: message.get(key) for key in expected}
        self.assertEqual(dic, expected)


    def test_get_user_and_user_id(self):
        data = (
            ('bot', 11111),
            (11111, 11111),
            ('dev', 22222),
            (22222, 22222),
        )
        for key, uid in data:
            with self.subTest(key=key, uid=uid):
                self.assertEqual(self.tgs.get_user_id(key), uid)
                self.assertEqual(self.tgs.get_user(key)['id'], uid)
        data = (
            0,
            99999,
            'Bran',  # first_name no username
            'fake',
            # chat
            -10011,
        )
        for key in data:
            with self.subTest(key=key):
                with self.assertRaises(BadRequest):
                    self.tgs.get_user_id(key)
                with self.assertRaises(BadRequest):
                    self.tgs.get_user(key)


    def test_get_chat_and_chat_id(self):
        data = (
            ('g1', -10011),
            (-10011, -10011),
            ('s2', -10022),
            (-10022, -10022),
            ('c3', -10033),
            (-10033, -10033),
        )
        for key, cid in data:
            with self.subTest(key=key, cid=cid):
                self.assertEqual(self.tgs.get_chat_id(key), cid)
                self.assertEqual(self.tgs.get_chat(key)['id'], cid)
        data = (
            0,
            99999,
            'G1',  # title no username
            'fake',
            # users are private chats (valid data)
        )
        for key in data:
            with self.subTest(key=key):
                with self.assertRaises(BadRequest):
                    self.tgs.get_chat_id(key)
                with self.assertRaises(BadRequest):
                    self.tgs.get_chat(key)


    def test_set_states(self):
        data = {
            'bot': {'g1': 'member', 'g2': 'member', 'g3': 'member',
                    's1': 'member', 's2': 'member', 's3': 'member',
                    'c1': 'member', 'c2': 'member', 'c3': 'member'},
            'dev': {'g1': 'left', 'g2': 'left', 'g3': 'left',
                    's1': 'left', 's2': 'left', 's3': 'left',
                    'c1': 'left', 'c2': 'left', 'c3': 'left'},
            'adm': {'g1': 'left', 'g2': 'left', 'g3': 'left',
                    's1': 'left', 's2': 'left', 's3': 'left',
                    'c1': 'left', 'c2': 'left', 'c3': 'left'},
            'usr': {'g1': 'left', 'g2': 'left', 'g3': 'left',
                    's1': 'left', 's2': 'left', 's3': 'left',
                    'c1': 'left', 'c2': 'left', 'c3': 'left'},
        }
        for user, chat_status in data.items():
            for chat, status in chat_status.items():
                with self.subTest(user=user, chat=chat, status=status):
                    uid = self.tgs.get_user_id(user)
                    cid = self.tgs.get_chat_id(chat)
                    self.assertEqual(self.tgs.status[cid][uid], status)
        # a: administrator
        # c: creator
        # m: member
        # l: left
        # k: kicked
        # r: restricted
        table = ('    g1 s2 c3\n'
                 'bot  a  m  m\n'
                 'dev  c  a  a\n'
                 'usr  l  k  r')
        self.tgs.set_states(table)
        data['bot']['g1'] = 'administrator'
        data['dev']['g1'] = 'creator'
        data['dev']['s2'] = 'administrator'
        data['dev']['c3'] = 'administrator'
        data['usr']['s2'] = 'kicked'
        data['usr']['c3'] = 'restricted'
        for user, chat_status in data.items():
            for chat, status in chat_status.items():
                with self.subTest(user=user, chat=chat, status=status):
                    uid = self.tgs.get_user_id(user)
                    cid = self.tgs.get_chat_id(chat)
                    self.assertEqual(self.tgs.status[cid][uid], status)


    def test_next_unique_id(self):
        self.assertEqual(self.tgs.next_unique_id(), 1)
        self.assertEqual(self.tgs.next_unique_id(), 2)


    def test_next_message_id(self):
        chat_id = 1

        # With no messages always returns 1
        self.assertEqual(self.tgs.next_message_id(chat_id), 1)
        self.assertEqual(self.tgs.next_message_id(chat_id), 1)

        data = (
            ([{'message_id': 1}, {'message_id': 2}, {'message_id': 3}], 4),
            ([{'message_id': 1}, {'message_id': 5}, {'message_id': 6}], 7),
            ([{'message_id': 1}, {'message_id': 5}, {'message_id': 8}], 9),
        )
        for messages, next_id in data:
            with self.subTest(messages=messages, next_id=next_id):
                self.tgs.messages = {chat_id: messages}
                self.assertEqual(self.tgs.next_message_id(chat_id), next_id)
                # Repeated calls do not change the result
                self.assertEqual(self.tgs.next_message_id(chat_id), next_id)

        with self.subTest(case='unsorted messages'):
            with self.assertRaises(IndexError):
                messages = [{'message_id': 1}, {'message_id': 4}, {'message_id': 2}]
                self.tgs.messages = {chat_id: messages}
                self.tgs.next_message_id(chat_id)


    def test_get_message(self):
        chat_id = 1

        with self.subTest(case='chat not found'):
            self.assertEqual(self.tgs.get_message(chat_id, None, False), {})
            with self.assertRaises(BadRequest):
                self.tgs.get_message(chat_id)

        self.tgs.messages = {chat_id: []}  # e.g. all messages have been deleted

        with self.subTest(case='message not found'):
            self.assertEqual(self.tgs.get_message(chat_id, None, False), {})
            with self.assertRaises(BadRequest):
                self.tgs.get_message(chat_id, None)

            self.assertEqual(self.tgs.get_message(chat_id, 1, False), {})
            with self.assertRaises(BadRequest):
                self.tgs.get_message(chat_id, 1)

        msg_1 = {'message_id': 1}
        msg_2 = {'message_id': 2}
        msg_4 = {'message_id': 4}
        self.tgs.messages = {chat_id: [msg_1, msg_2, msg_4]}

        self.assertEqual(self.tgs.get_message(chat_id), msg_4)
        self.assertEqual(self.tgs.get_message(chat_id, 1), msg_1)
        self.assertEqual(self.tgs.get_message(chat_id, 2), msg_2)
        self.assertEqual(self.tgs.get_message(chat_id, 4), msg_4)

        with self.subTest(case='message not found'):
            self.assertEqual(self.tgs.get_message(chat_id, 5, False), {})
            with self.assertRaises(BadRequest):
                self.tgs.get_message(chat_id, 5)

        with self.subTest(case='unsorted messages'):
            with self.assertRaises(IndexError):
                self.tgs.messages = {chat_id: [msg_1, msg_4, msg_2]}
                self.tgs.get_message(chat_id)


    def test_add_message(self):
        chat_id = 1

        msg_1 = {'message_id': 1, 'chat': {'id': chat_id}, 'key': '1'}
        msg_2 = {'message_id': 2, 'chat': {'id': chat_id}, 'key': '2'}
        msg_2b = {'message_id': 2, 'chat': {'id': chat_id}, 'key': '2b'}
        msg_3 = {'message_id': 3, 'chat': {'id': chat_id}, 'key': '3'}

        self.tgs.messages = {chat_id: [msg_1, msg_2]}

        with self.subTest(case='is not message'):
            with self.assertRaises(ValueError):
                self.tgs.add_message(5)

        self.tgs.add_message(msg_3)
        self.assertEqual(self.tgs.messages, {chat_id: [msg_1, msg_2, msg_3]})

        self.tgs.add_message(msg_2b)
        self.assertEqual(self.tgs.messages, {chat_id: [msg_1, msg_2b, msg_3]})


    def test_del_message(self):
        chat_id = 1
        msg_1 = {'message_id': 1}
        msg_2 = {'message_id': 2}
        self.tgs.messages = {chat_id: [msg_1, msg_2]}

        with self.subTest(case='message not found'):
            with self.assertRaises(BadRequest):
                self.tgs.del_message(chat_id, 3)
            self.assertEqual(self.tgs.messages, {chat_id: [msg_1, msg_2]})

        self.tgs.del_message(chat_id, 2)
        self.assertEqual(self.tgs.messages, {chat_id: [msg_1]})


    def test_get_emitter_username(self):
        data = (
            # Does not use self.nicks or self.users
            ({'from': {'username': 'aaa', 'id': 1}}, 'aaa'),
            #
            ({'author_signature': 'Cupidus'}, 'bot'),
            ({'author_signature': 'dev'}, 'dev'),
            ({'author_signature': 'Bran Stark'}, 'dev'),
        )
        for dic, expected in data:
            with self.subTest(dic=dic):
                self.assertEqual(self.tgs.get_emitter_username(dic), expected)

        data = (
            ({'from': {'id': 1}}, KeyError),
            ({'username': 'Cupidus'}, KeyError),
        )
        for dic, error in data:
            with self.subTest(dic=dic):
                with self.assertRaises(error):
                    self.tgs.get_emitter_username(dic)


    def test_generate_message(self):
        cmd = {'length': 4, 'offset': 0, 'type': 'bot_command'}
        dev = USERS[DEVID]

        msg_1 = {**DEV_G1, 'date': int(time.time()), 'message_id': 1, 'text': 'text'}
        msg_2 = {**DEV_G1, 'date': int(time.time()), 'message_id': 2, 'photo': PHOTO}
        self.tgs.messages[G1ID] = [msg_1, msg_2]

        data = (
            ('dev', None, 'text', {**DEV_DEV, 'text': 'text'}),
            ('dev', None, '/cmd', {**DEV_DEV, 'text': '/cmd', 'entities': [cmd]}),
            ('dev', 'g1', 'photo', {**DEV_G1, 'photo': PHOTO}),
            ('dev', 'g1', 'title:GG1', {**DEV_G1, 'new_chat_title': 'GG1'}),
            ('dev', 'g1', 'members:dev', {**DEV_G1, 'new_chat_members': [dev]}),
            ('dev', 'g1', 'members:-dev', {**DEV_G1, 'left_chat_member': dev}),
            ('dev', 'g1', f'migrate:{S1ID}', {**DEV_G1, 'migrate_to_chat_id': S1ID}),
            ('dev', 's1', f'migrate:{G1ID}', {**DEV_S1, 'migrate_from_chat_id': G1ID}),
            ('dev', 'g1', 'edit:1:tex', {**DEV_G1, 'text': 'tex'}),
            ('dev', 'g1', 'reply:1:tex', {**DEV_G1,
                                          'text': 'tex',
                                          'reply_to_message': msg_1}),
            ('dev', None, f'forward:{G1ID}:1', {**DEV_DEV,
                                                'text': 'text',
                                                'forward_from': DEV_G1['from']}),
            ('dev', None, f'forward:{G1ID}:2', {**DEV_DEV,
                                                'photo': PHOTO,
                                                'forward_from': DEV_G1['from']}),
        )
        for user, chat, content, expected in data:
            with self.subTest(user=user, chat=chat, content=content):
                message = self.tgs.generate_message(user, chat, content)

                message_id = 3 if chat == 'g1' else 1
                if content == 'edit:1:tex':
                    message_id = 1

                self.check_message(message, expected, message_id)

        data = (
            ('dev', 'g1', f'migrate:{G1ID}'),
            ('dev', 'g1', f'migrate:{C1ID}'),
            ('dev', 's1', f'migrate:{S1ID}'),
            ('dev', 's1', f'migrate:{C1ID}'),
            ('dev', 'c1', f'migrate:{G1ID}'),
            ('dev', 'c1', f'migrate:{S1ID}'),
            ('dev', 'c1', f'migrate:{C1ID}'),
        )
        for user, chat, content in data:
            with self.subTest(user=user, chat=chat, content=content):
                with self.assertRaises(ValueError):
                    self.tgs.generate_message(user, chat, content)


    def test_process_message(self):
        msg_a = {**BOT_DEV, 'date': int(time.time()), 'message_id': 1, 'text': 'text'}
        msg_b = {**BOT_G1, 'date': int(time.time()), 'message_id': 1, 'text': 'text'}
        msg_c = {**BOT_G1, 'date': int(time.time()), 'message_id': 2, 'photo': PHOTO}
        self.tgs.messages[DEVID] = [msg_a]
        self.tgs.messages[G1ID] = [msg_b, msg_c]
        data = (
            # New message
            (2,
             {'text': 'text', 'chat_id': DEVID, 'reply_markup': '{"a": "b"}'},
             {**BOT_DEV, 'text': 'text', 'reply_markup': {'a': 'b'}}),

            # Edit message
            (1,
             {'text': 'text', 'chat_id': DEVID, 'message_id': '1'},
             {**BOT_DEV, 'text': 'text'}),

            # Reply to message
            (2,
             {'text': 'text', 'chat_id': DEVID, 'reply_to_message_id': '1'},
             {**BOT_DEV, 'text': 'text', 'reply_to_message': msg_a}),

            # Forward text message
            (2,
             {'chat_id': DEVID, 'from_chat_id': str(G1ID), 'message_id': '1'},
             {**BOT_DEV, 'text': msg_b['text'], 'forward_from': msg_b['from']}),

            # Forward media message
            (2,
             {'chat_id': DEVID, 'from_chat_id': str(G1ID), 'message_id': '2'},
             {**BOT_DEV, 'photo': PHOTO, 'forward_from': msg_c['from']}),
        )
        for message_id, body, expected in data:
            with self.subTest(body=body, expected=expected):
                message = self.tgs.process_message(body)
                self.check_message(message, expected, message_id)


    def test__enabled_edit_members(self):
        # pylint: disable=protected-access
        status = {
            'c': 'creator',
            'a': 'administrator',
            'm': 'member',
            'r': 'restrict',
        }
        data = (
            ('restrictchatmember', G1ID, 'c', 'm', True),
            ('restrictchatmember', G1ID, 'c', 'a', True),
            ('restrictchatmember', G1ID, 'a', 'm', True),
            ('restrictchatmember', G1ID, 'a', 'a', True),
            ('restrictchatmember', G1ID, 'm', 'm', True),
            ('restrictchatmember', G1ID, 'm', 'a', True),
            ('restrictchatmember', G1ID, 'r', 'a', True),

            ('restrictchatmember', C1ID, 'c', 'm', True),
            ('restrictchatmember', C1ID, 'c', 'a', True),
            ('restrictchatmember', C1ID, 'a', 'm', True),
            ('restrictchatmember', C1ID, 'a', 'a', True),
            ('restrictchatmember', C1ID, 'm', 'm', True),
            ('restrictchatmember', C1ID, 'm', 'a', True),
            ('restrictchatmember', C1ID, 'r', 'a', True),

            ('restrictchatmember', S1ID, 'c', 'm', True),
            ('restrictchatmember', S1ID, 'c', 'a', True),
            ('restrictchatmember', S1ID, 'a', 'm', True),
            ('restrictchatmember', S1ID, 'a', 'a', True),
            ('restrictchatmember', S1ID, 'm', 'm', True),
            ('restrictchatmember', S1ID, 'm', 'a', False),
            ('restrictchatmember', S1ID, 'r', 'a', False),

            ('kickchatmember', G1ID, 'c', 'm', True),
            ('kickchatmember', G1ID, 'c', 'a', True),
            ('kickchatmember', G1ID, 'a', 'm', True),
            ('kickchatmember', G1ID, 'a', 'a', True),
            ('kickchatmember', G1ID, 'm', 'm', True),
            ('kickchatmember', G1ID, 'm', 'a', False),
            ('kickchatmember', G1ID, 'r', 'a', False),

            ('kickchatmember', C1ID, 'c', 'm', True),
            ('kickchatmember', C1ID, 'c', 'a', True),
            ('kickchatmember', C1ID, 'a', 'm', True),
            ('kickchatmember', C1ID, 'a', 'a', True),
            ('kickchatmember', C1ID, 'm', 'm', True),
            ('kickchatmember', C1ID, 'm', 'a', False),
            ('kickchatmember', C1ID, 'r', 'a', False),

            ('kickchatmember', S1ID, 'c', 'm', True),
            ('kickchatmember', S1ID, 'c', 'a', True),
            ('kickchatmember', S1ID, 'a', 'm', True),
            ('kickchatmember', S1ID, 'a', 'a', True),
            ('kickchatmember', S1ID, 'm', 'm', True),
            ('kickchatmember', S1ID, 'm', 'a', False),
            ('kickchatmember', S1ID, 'r', 'a', False),
        )
        for cmd, cid, dev, bot, error in data:
            with self.subTest(cmd=cmd, cid=cid, dev=dev, bot=bot, error=error):

                self.tgs.status[cid][DEVID] = status[dev]
                self.tgs.status[cid][BOTID] = status[bot]

                if error:
                    with self.assertRaises(BadRequest):
                        self.tgs._enabled_edit_members(cmd, cid, DEVID)
                else:
                    response = self.tgs._enabled_edit_members(cmd, cid, DEVID)
                    self.assertEqual(response, {'ok': True, 'result': True})


    def test__check_consent(self):
        # pylint: disable=protected-access

        # By default in all chat (except private) can be sent
        data = (G1ID, S1ID, C1ID, G2ID, S2ID, C2ID, G3ID, S3ID, C3ID)
        for cid in data:
            with self.subTest(cid=cid):
                self.assertIsNone(self.tgs._check_consent(cid))

        # ... in private chat cannot
        data = (DEVID, ADMID, USRID)
        for cid in data:
            with self.subTest(cid=cid):
                with self.assertRaises(BadRequest):
                    self.tgs._check_consent(cid)

        # ... and send himself in a meaningless
        with self.subTest(cid=BOTID):
            with self.assertRaises(ValueError):
                self.tgs._check_consent(BOTID)


    # --- More integral tests


    @mock.patch('tgs.Bot')
    @mock.patch('tgs.Dispatcher')
    @mock.patch('traceback.print_exc')
    def test_request(self, _print_exc, _dispatcher, _bot):
        # pylint: disable=protected-access,too-many-locals

        self.tgs.add_handlers(not_doing_anything)

        request = self.tgs.bot._request._request_wrapper

        message = {**BOT_DEV, 'text': 'text'}
        forward = {**BOT_G1, 'text': 'text', 'forward_from': message['from']}

        result_ok = {'ok': True, 'result': True}

        self.tgs.status[S1ID][BOTID] = 'administrator'
        self.tgs.status[S1ID][DEVID] = 'creator'
        self.tgs.consent[DEVID] = True

        s1_dev_status = {'user': USERS[DEVID], 'status': 'creator'}
        s1_bot_status = {'user': USERS[BOTID], 'status': 'administrator'}

        data = (
            ('getme',
             None,
             {'ok': True, 'result': USERS[BOTID]}),

            ('answercallbackquery',
             None,
             result_ok),

            ('sendchataction',
             None,
             result_ok),


            ('sendmessage',
             {'text': 'text', 'chat_id': DEVID},
             {'ok': True, 'result': message}),

            ('editmessagetext',
             {'text': 'text', 'chat_id': DEVID, 'message_id': '1'},
             {'ok': True, 'result': message}),

            ('forwardmessage',
             {'chat_id': G1ID, 'from_chat_id': str(DEVID), 'message_id': '1'},
             {'ok': True, 'result': forward}),

            ('deletemessage',
             {'chat_id': DEVID, 'message_id': '1'},
             result_ok),


            ('sendphoto',
             {'text': 'text', 'chat_id': DEVID},
             {'ok': True, 'result': {**BOT_DEV, 'photo': PHOTO}}),


            ('getchatmember',
             {'chat_id': S1ID, 'user_id': DEVID},
             {'ok': True, 'result': s1_dev_status}),

            ('getchat',
             {'chat_id': G1ID},
             {'ok': True, 'result': CHATS[G1ID]}),

            ('getchatadministrators',
             {'chat_id': S1ID},
             {'ok': True, 'result': [s1_bot_status, s1_dev_status]}),


            ('restrictchatmember',
             {'chat_id': S1ID, 'user_id': USRID},
             {'ok': True, 'result': True}),

            ('kickchatmember',
             {'chat_id': S1ID, 'user_id': USRID},
             {'ok': True, 'result': True}),
        )
        for cmd, body, expected in data:
            with self.subTest(cmd=cmd, body=body):
                response = request('GET', self.burl + cmd, body=json.dumps(body))
                response = json.loads(response)
                result = response['result']
                if isinstance(result, dict) and 'message_id' in result:
                    self.check_message(result, expected['result'])
                    self.assertEqual(response['ok'], expected['ok'])
                else:
                    self.assertEqual(response, expected)

        # Bot without permission
        cmd = 'sendphoto'
        body = {'text': 'text', 'chat_id': USRID}
        with self.subTest(case='Bot without permission'):
            with self.assertRaises(BadRequest):
                request('GET', self.burl + cmd, body=json.dumps(body))

        # Commands not implemented
        data = (
            'getUpdates',
            'setWebhook',
            'deleteWebhook',
            'getWebhookInfo',
        )
        for cmd in data:
            with self.subTest(cmd=cmd):
                _print_exc.reset_mock()

                with self.assertRaises(NotImplementedError):
                    request('GET', self.burl + cmd)

                self.assertEqual(_print_exc.call_count, 1)

        # Invalid values
        valid_url = self.burl + 'cmd'
        data = (
            ({'url': object()}, TypeError),
            ({'url': 'url'}, AttributeError),
            ({'url': valid_url, 'body': 'text'}, json.JSONDecodeError),
            ({'url': valid_url, 'body': object()}, TypeError),
        )
        for params, error in data:
            with self.subTest(params=params):
                _print_exc.reset_mock()

                with self.assertRaises(error):
                    request('METHOD', **params)

                self.assertEqual(_print_exc.call_count, 1)


    def test_empty(self):
        self.assertTrue(self.tgs.empty())
        self.tgs.responses.append('a')
        self.assertFalse(self.tgs.empty())
        self.tgs.responses.popleft()
        self.assertTrue(self.tgs.empty())


    def test_send(self):
        self.tgs.status[G1ID][DEVID] = 'member'
        self.tgs.status[C1ID][DEVID] = 'member'

        class FakeDispatcher:
            # pylint: disable=too-few-public-methods
            updates = []
            def process_update(self, update):
                self.updates.append(update)
        self.tgs.dispatcher = FakeDispatcher()

        cb_text = MARKUP_IKB_DICT['inline_keyboard'][0][0]['text']
        cb_data = MARKUP_IKB_DICT['inline_keyboard'][0][0]['callback_data']
        msg_1 = {**DEV_G1, 'message_id': 1, 'date': 1, 'reply_markup': MARKUP_IKB_DICT}

        msg_2 = {**DEV_C1, 'message_id': 1, 'date': 1, 'text': 'text'}

        data = (
            ({'user': 'dev', 'chat': None, 'content': 'text a'},
             {**DEV_DEV, 'text': 'text a'},
             'message',
             {}),

            ({'user': 'dev', 'chat': 'dev', 'content': 'text b', 'caption': 'c'},
             {**DEV_DEV, 'text': 'text b', 'caption': 'c'},
             'message',
             {}),

            ({'user': 'dev', 'chat': 'c1', 'content': 'text b'},
             {**DEV_C1, 'text': 'text b'},
             'channel_post',
             {}),

            ({'user': 'dev', 'chat': 'c1', 'content': 'edit:1:text c'},
             {**DEV_C1, 'text': 'text c'},
             'edited_channel_post',
             {C1ID: [msg_2]}),

            ({'user': 'dev', 'chat': 'g1', 'content': cb_text},
             {'from': USERS[DEVID], 'data': cb_data},
             'callback_query',
             {G1ID: [msg_1]}),
        )
        for in_dic, expected, variable, messages in data:
            with self.subTest(in_dic=in_dic):
                self.tgs.messages = messages
                self.tgs.send(in_dic)
                update = self.tgs.dispatcher.updates.pop()
                self.check_update(update, expected, variable)

        with self.subTest(case='user is not in chat'):
            with self.assertRaises(ValueError):
                dic = {'user': 'dev', 'chat': 'c2', 'content': 'text'}
                self.tgs.send(dic)


    def test_recv(self):
        cmd = {'length': 4, 'offset': 0, 'type': 'bot_command'}
        data = (
            ({**DEV_DEV, 'text': '/cmd', 'entities': [cmd]},
             {'user': 'dev', 'chat': None, 'content': '/cmd'}),

            ({**DEV_DEV, 'text': 'text', 'caption': 'c'},
             {'user': 'dev', 'chat': 'dev', 'content': 'text', 'caption': 'c'}),

            ({**DEV_DEV, 'reply_markup': {'keyboard': [['a']]}},
             {'user': 'dev', 'chat': None, 'content': None, 'markup': ['a']}),

            ({**DEV_DEV, 'photo': PHOTO},
             {'user': 'dev', 'chat': None, 'content': 'photo'}),
        )
        for in_dic, expected in data:
            with self.subTest(in_dic=in_dic):
                self.tgs.responses.append(in_dic)
                out_dic = self.tgs.recv()

                for dic in (expected, out_dic):
                    if dic['chat'] == dic['user']:
                        dic['chat'] = None

                self.assertEqual(out_dic, expected)
