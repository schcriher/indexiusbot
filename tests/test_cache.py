# -*- coding: UTF-8 -*-
# Copyright (C) 2019 Schmidt Cristian Hernán

import copy
import time

from environ import unittest, cache, CFG


class TestCache(unittest.TestCase):


    def test_adm_cache(self):
        old = CFG.CACHING_ADM_TIME
        new = 0.005

        CFG.CACHING_ADM_TIME = new

        run = False

        @cache.adm_cache
        def func(_, chat_id):
            nonlocal run
            run = True
            return chat_id

        self.assertFalse(run)
        self.assertEqual(func(None, 1), 1)
        self.assertTrue(run)

        run = False
        self.assertFalse(run)
        self.assertEqual(func(None, 1), 1)
        self.assertFalse(run)
        time.sleep(new)
        self.assertEqual(func(None, 1), 1)
        self.assertTrue(run)

        run = False
        func.clear()
        self.assertEqual(func(None, 1), 1)
        self.assertTrue(run)

        run = False
        self.assertEqual(func(None, 2), 2)
        self.assertTrue(run)

        CFG.CACHING_ADM_TIME = old


class TestCacheMemoryCache(unittest.TestCase):


    def setUp(self):
        self.cache = cache.MemoryCache()
        self.cache['a', 'b', 'c'] = 1  # involves testing __setitem__
        self.cache['a', 'x', 'y'] = 2  # involves testing __setitem__


    def test___getitem__(self):
        # * → with slice
        self.assertEqual(self.cache['a'], {'b': {'c': 1}, 'x': {'y': 2}})
        self.assertEqual(self.cache['a', 'b'], {'c': 1})
        self.assertEqual(self.cache['a', 'x'], {'y': 2})
        self.assertEqual(self.cache['a', 'b', 'c'], 1)
        self.assertEqual(self.cache['a', 'x', 'y'], 2)
        self.assertEqual(self.cache['m'], None)
        self.assertEqual(self.cache['b'], None)
        self.assertEqual(self.cache['a', 'y'], None)
        self.assertEqual(self.cache['a', 'c'], None)
        self.assertEqual(self.cache['a', 'b', 'y'], None)
        self.assertEqual(self.cache['a', 'x', 'c'], None)


    def test___delitem__(self):
        self.assertEqual(self.cache['a', 'b', 'c'], 1)
        self.assertEqual(self.cache['a', 'x', 'y'], 2)
        del self.cache['a', 'b', 'c']
        del self.cache['a', 'x', 'y']
        self.assertEqual(self.cache['a', 'b', 'c'], None)
        self.assertEqual(self.cache['a', 'x', 'y'], None)
        self.assertEqual(self.cache['a'], {'b': {}, 'x': {}})
        del self.cache['a', 'b', 'c']
        del self.cache['a', 'x', 'y']
        self.assertEqual(self.cache['a'], {'b': {}, 'x': {}})
        del self.cache['a']
        self.assertEqual(self.cache['a'], None)


    def test_pop(self):
        self.assertEqual(self.cache['a', 'b', 'c'], 1)
        self.assertEqual(self.cache['a', 'x', 'y'], 2)
        self.assertEqual(self.cache.pop('a', 'b', 'c'), 1)
        self.assertEqual(self.cache.pop('a', 'x', 'y'), 2)
        self.assertEqual(self.cache['a', 'b', 'c'], None)
        self.assertEqual(self.cache['a', 'x', 'y'], None)
        self.assertEqual(self.cache['a'], {'b': {}, 'x': {}})
        self.assertEqual(self.cache.pop('a'), {'b': {}, 'x': {}})
        self.assertEqual(self.cache['a'], None)


    def check(self, dic_a, dic_b):
        dic_a = copy.deepcopy(dic_a)
        dic_b = copy.deepcopy(dic_b)

        for val_a, val_b in zip(dic_a.values(), dic_b.values()):
            last_a = val_a.pop('last')
            last_b = val_b.pop('last')
            self.assertLess(abs(last_a - last_b), 1)

        self.assertEqual(dic_a, dic_b)


    def test_edited_chat(self):
        # pylint: disable=protected-access
        del self.cache['a']
        self.assertEqual(self.cache._MemoryCache__data, {})

        nick = 'chat2user1'
        uid = 1
        cid = 2
        now = time.time()
        dic = {'chat': {'id': cid}, 'nick': nick, 'last': now - 10}
        self.cache[uid] = dic

        self.assertEqual(self.cache.edited_chat(1), False)
        self.assertEqual(self.cache.edited_chat(2), nick)  # cid
        self.assertEqual(self.cache.edited_chat(3), False)
        self.check(self.cache._MemoryCache__data, {uid: dic})

        self.assertEqual(self.cache.edited_chat(cid, timeout=9), False)
        self.assertEqual(self.cache._MemoryCache__data, {})

        self.assertEqual(self.cache.edited_chat(cid, uid, nick), True)
        dic['last'] = time.time()
        self.check(self.cache._MemoryCache__data, {uid: dic})
