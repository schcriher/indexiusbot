# -*- coding: UTF-8 -*-
# Copyright (C) 2019 Schmidt Cristian Hernán

from environ import unittest, database, models, CFG

CHATS = (
    {'id': 0, 'title': 'G0', 'type': 'group'},
    {'id': 1, 'title': 'S1', 'type': 'supergroup'},
    {'id': 2, 'title': 'C2', 'type': 'channel'},
)

RULES = (
    {'id': 0,
     'name': '«§ł·ĸ»',
     'regex': None,
     'lname': None,
     'args': None,
     'url': 'url',
     'send': 'GET',
     'user_id': 1,
     'chat_id': 0},
    {'id': 1,
     'name': 'rule_a',
     'regex': 'regex',
     'lname': None,
     'args': None,
     'url': 'url',
     'send': 'GET',
     'user_id': 1,
     'chat_id': 0},
    {'id': 2,
     'name': 'rule_b',
     'regex': 'regex',
     'lname': 'lname',
     'args': 'args',
     'url': 'url',
     'send': 'POST_FORM',
     'user_id': 1,
     'chat_id': 0},
    {'id': 3,
     'name': '<rule_c>',
     'regex': 'regex',
     'lname': None,
     'args': None,
     'url': 'http://example.com?a=1&b=2',
     'send': 'GET',
     'user_id': 2,
     'chat_id': 1},
    {'id': 4,
     'name': 'rule_d',
     'regex': 'regex',
     'lname': 'lname',
     'args': 'args',
     'url': 'url',
     'send': 'POST_JSON',
     'user_id': 2,
     'chat_id': 2,
     'attempts': CFG.RULE_MAX_NUM_ERRS + 1},
)

class TestDatabase(unittest.TestCase):


    def setUp(self):
        self.dbs = database.create_session(create_all=True)()

        for data in CHATS:
            self.dbs.add(database.Chat(**data))

        for data in RULES:
            self.dbs.add(database.Rule(**data))

        self.dbs.commit()


    def check(self, obj, dic):
        self.assertEqual(bool(obj), bool(dic))
        for key, val in (dic or {}).items():
            self.assertEqual(getattr(obj, key), val, key)


    def test_exists_rule_name(self):
        self.assertTrue(database.exists_rule_name(self.dbs, '«§ł·ĸ»', 0))
        self.assertTrue(database.exists_rule_name(self.dbs, 'rule_a', 0))
        self.assertFalse(database.exists_rule_name(self.dbs, 'wrong', 0))
        self.assertFalse(database.exists_rule_name(self.dbs, 'rule-a', 0))
        self.assertFalse(database.exists_rule_name(self.dbs, 'rule_a', 1))


    def test_get_rule(self):
        rule_id = 3
        rule = database.get_rule(self.dbs, rule_id)
        self.check(rule, RULES[rule_id])
        self.assertEqual(rule.send_type, models.SendType.GET)
        self.assertEqual(rule.name_html, '&lt;rule_c&gt;')
        self.assertEqual(rule.url_html, 'http://example.com?a=1&amp;b=2')


    def test_get_rules(self):
        data = (
            ({'chat_id': 0, 'which': models.RuleRegex.ALL, 'count':True}, 3),
            ({'chat_id': 0, 'which': models.RuleRegex.ONLY_SET, 'count':True}, 2),
            ({'chat_id': 0, 'which': models.RuleRegex.ONLY_NULL, 'count':True}, 1),
            ({'chat_id': 1, 'which': models.RuleRegex.ALL, 'count':False}, [RULES[3]]),
            ({'chat_id': 2, 'which': models.RuleRegex.ALL, 'count':False}, []),
        )
        for params, expected in data:
            with self.subTest(params=params):
                obj = database.get_rules(self.dbs, **params)
                if isinstance(expected, list):
                    self.assertEqual(len(obj), len(expected))
                    for _obj, _expected in zip(obj, expected):
                        self.check(_obj, _expected)
                else:
                    self.assertEqual(obj, expected)


    def test_get_chat(self):
        chat_id = 0
        chat_dic = CHATS[chat_id]

        old_title = chat_dic['title']
        new_title = "Title's no safe"

        self.assertNotEqual(old_title, new_title)

        # Mod
        chat_dic['title'] = new_title
        chat_obj = database.get_chat(self.dbs, chat_id, 'group', new_title)
        self.check(chat_obj, chat_dic)
        self.assertEqual(chat_obj.title_html, 'Title&#x27;s no safe')

        # New
        chat_dic = {'id': 8, 'title': 'title', 'type': 'type'}
        chat_obj = database.get_chat(self.dbs, 8, 'type', 'title')
        self.check(chat_obj, chat_dic)


    def test_migrate_chat(self):
        database.migrate_chat(self.dbs, 0, 9, 'title')
        data = (
            (0, 0),
            (9, 3),
        )
        for chat_id, expected in data:
            with self.subTest(chat_id=chat_id, expected=expected):
                count = database.get_rules(self.dbs,
                                           chat_id=chat_id,
                                           which=models.RuleRegex.ALL,
                                           count=True)
                self.assertEqual(count, expected)


    def test_get_query(self):
        q_chat = self.dbs.query(database.Chat)
        q_rule = self.dbs.query(database.Rule)
        data = (
            ({'arg': 'S1 text 1', 'query': q_chat, 'attribute': 'title'},
             CHATS[1],
             'text 1'),
            ({'arg': '1 text 2', 'query': q_chat},
             CHATS[1],
             'text 2'),
            ({'arg': 'channel text 3', 'query': q_chat, 'attribute': 'type'},
             CHATS[2],
             'text 3'),
            ({'arg': '"<rule c" text 4', 'query': q_rule, 'attribute': 'name'},
             RULES[3],
             'text 4'),
            ({'arg': '"xyz" text 5', 'query': q_rule, 'attribute': 'name'},
             None,
             'text 5'),
        )
        for params, expected_dic, expected_tex in data:
            with self.subTest(params=params):
                obj, text = database.get_query(**params)
                self.check(obj, expected_dic)
                self.assertEqual(text, expected_tex)
