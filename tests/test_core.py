# -*- coding: UTF-8 -*-
# Copyright (C) 2019 Schmidt Cristian Hernán

import re
import sys
import datetime
import collections

from environ import (unittest, mock, core, texts, models, tools, EmptyGenericClass,
                     mocked_connection, mocked_session, CFG)


class Update(EmptyGenericClass):
    # pylint: disable=too-few-public-methods

    def __getattr__(self, attr):
        # Can be used in cases such as: Update.message.«attr»
        return self


class TestCore(unittest.TestCase):


    def test_compile_regex(self):
        newl = '\n'
        data = (
            (r'(?P<n>.*)',
             r'(?P<n>.*)'),

            (r'(?P<n>.*)(\w+)\s(\d+)',
             r'(?P<n>.*)(\w+)\s(\d+)'),

            (fr' \s+{newl}(\w+) (\w+)',
             r'(\w+)\s+(\w+)'),

            (fr' \s|\S{newl}(\w+) (\w+)',
             r'(\w+)(?:\s|\S)(\w+)'),

            (fr' \s+{newl}null{newl}·  \s*\.\s*{newl}^(\w+·\w+) (\w+)',
             r'^(\w+\s*\.\s*\w+)\s+(\w+)'),

            (fr'" " "{newl} \s+{newl}("\w+") .*',
             r'("\s+"\w+"\s+")\s+.*'),
        )
        for regex, expected in data:
            with self.subTest(regex=regex, expected=expected):
                self.assertEqual(core.compile_regex(regex).pattern, expected)


    def test_get_regex_list_size(self):
        data = (
            (r'(?P<n>.*)', 0),
            (r'(?P<n>.*)(?P<m>.*)', 0),
            (r'(\w+)', 1),
            (r'(\w+)\s(\d+)', 2),
            (r'(?P<n>.*)(\w+)\s(\d+)', 2),
        )
        for regex, expected in data:
            with self.subTest(regex=regex, expected=expected):
                self.assertEqual(core.get_regex_list_size(regex), expected)

        data = (
            (r'', ValueError),
            (r'.*', ValueError),
            (r'\s+', ValueError),
            (r'\w+', ValueError),
            (r'(', re.error),
            (r'(?P<n>*)', re.error),
            (r'(?P<n>.*)(?P<n>\w*)', re.error),
        )
        for regex, error in data:
            with self.subTest(regex=regex, error=error):
                with self.assertRaises(error):
                    core.get_regex_list_size(regex)


    def test_get_fields_from_regex(self):
        data = (
            (r'(?P<n>.*)', ['n']),
            (r'(?P<n>.*)(?P<m>.*)', ['n', 'm']),
            (r'(\w+)', []),
            (r'(\w+)\s(\d+)', []),
            (r'(?P<n>.*)(\w+)\s(\d+)', ['n']),
            (r'', []),
            (r'.*', []),
            (r'\s+', []),
            (r'\w+', []),
        )
        for regex, expected in data:
            with self.subTest(regex=regex, expected=expected):
                self.assertEqual(core.get_fields_from_regex(regex), expected)


    def test_get_qdic_from_text(self):
        data = (
            (r'(?P<n>\w)', None, '&', {}),
            (r'(?P<n>\w)', 'z', 'a', {'n': 'a'}),
            (r'(?P<n>\w)(\d)', 'z', 'a1', {'n': 'a', 'z': 1}),
            (r'(\d)(\d)(\d)', 'z', '123', {'z': [1, 2, 3]}),
            (r'(?P<n>\w)(\d)(\d)', 'z', 'a12', {'n': 'a', 'z': [1, 2]}),
            (r'(?P<n>\w)(\d)(?P<m>\w)', 'z', 'a1b', {'n': 'a', 'z': 1, 'm': 'b'}),
        )
        for regex, lname, text, expected in data:
            with self.subTest(regex=regex, lname=lname, text=text):
                pairs = core.get_qdic_from_text(regex, lname, text)
                self.assertEqual(pairs, expected)


    def test_get_qdic_from_update(self):

        up1 = Update(message_id=10)
        up2 = Update(username='dev')
        up3 = Update(first_name='Bran')
        up4 = Update(date=0, author_signature=None)

        data = (
            ('', None, {}),
            ('message_id', up1, {'message_id': 10}),
            ('chat_username', up2, {'chat_username': 'dev'}),
            ('user_first_name', up3, {'user_first_name': 'Bran'}),
            ('message_date;post_author', up4, {'message_date': 0, 'post_author': None})
        )
        for args, update, expected in data:
            with self.subTest(args=args, expected=expected):
                pairs = core.get_qdic_from_update(args, update)
                self.assertEqual(pairs, expected)


    @mock.patch('indexius.core.socket.create_connection', side_effect=mocked_connection)
    def test_check_server_connection(self, _connection):
        data = (
            ('http://ok.net/test?v=1', None),
            ('https://ok.net/test?v=1', None),
            ('http://ok.net:/test?v=1', None),
            ('http://ok.bool/test?v=1', None),
            ('http://ok.json/test?v=1', None),
            ('http://error.text/test?v=1', None),
            ('http://error.json/test?v=1', None),
            ('http://error.unauthorized/test?v=1', None),
            ('http://raise.invalid_url/test?v=1', 'raise.invalid_url'),
            ('http://raise.response_error/test?v=1', 'raise.response_error'),
            # Only: http and https
            ('ftp://ok:1a/', 'Invalid URL'),
            ('ftp://ok.net/', 'Invalid URL'),
            ('//ok.net/', 'Invalid URL'),
            ('ok', 'Invalid URL'),
            ('ftp://[1:2:3:4:5:6]/', 'Invalid URL'),  # ipv6
        )
        for url, expected in data:
            with self.subTest(url=url, expected=expected):
                self.assertEqual(core.check_server_connection(url), expected)


    @mock.patch('indexius.core.Session', side_effect=mocked_session)
    def test_call_webhook(self, _session):
        data = (
            ('http://ok.net/test?v=1', ''),
            ('http://ok.bool/test?v=1', ''),
            ('http://ok.json/test?v=1', ''),
            ('http://error.text/test?v=1', 'fail'),
            ('http://error.json/test?v=1', 'fail'),
            ('http://error.unauthorized/test?v=1', '401 Unauthorized: unauthorized'),
            ('http://raise.invalid_url/test?v=1', 'invalid url'),
            ('http://raise.response_error/test?v=1', 'response error'),
        )
        for url, expected in data:
            with self.subTest(url=url, expected=expected):
                data = {'method': 'GET', 'url': url}
                self.assertEqual(core.call_webhook(data), expected)


    def test_make_webhook(self):
        method = 'METHOD'
        url = 'http://www.example.com/get?a=1&a=2&c=CCC#index'
        varname = 'VARNAME'
        qdic_regex_args = {'a': 3, 'b': 4}

        data = core.make_webhook(method, url, varname, qdic_regex_args)

        expected = {
            'method': 'METHOD',
            'url': 'http://www.example.com/get#index',
            'VARNAME': {'a': [1, 2, 3], 'c': 'CCC', 'b': 4},
            'timeout': CFG.URL_TIMEOUT,
            'headers': {'User-Agent': CFG.user_agent},
        }

        self.assertEqual(data, expected)


    @mock.patch('indexius.core.Session', side_effect=mocked_session)
    def test_dispatch_webhook(self, _session):
        # pylint: disable=no-member

        rule = EmptyGenericClass(
            args='message_id',
            url='http://error.text/',
            send_type=models.SendType.GET,
            user_id=1,
            used_at=0,
            attempts=0,
            interacts=0,
        )
        update = Update(message_id=1)
        chat = EmptyGenericClass(
            id=2,
            type='group',
            title='Title',
        )
        qdic = tools.QueryDict(var=0)
        errores = collections.defaultdict(lambda: collections.defaultdict(list))

        core.dispatch_webhook(rule, update, chat, qdic, errores)

        self.assertEqual(qdic, {'var': 0, 'message_id': 1})
        self.assertEqual(errores[rule.user_id][chat], ['fail'])
        self.assertEqual(rule.attempts, 1)
        self.assertEqual(rule.interacts, 0)

        rule.url = 'http://ok.net/'

        core.dispatch_webhook(rule, update, chat, qdic, errores)

        self.assertEqual(qdic, {'var': 0, 'message_id': [1, 1]})
        self.assertEqual(errores[rule.user_id][chat], ['fail'])
        self.assertEqual(rule.attempts, 0)
        self.assertEqual(rule.interacts, 1)

        core.dispatch_webhook(rule, update, chat, qdic, errores)

        self.assertEqual(qdic, {'var': 0, 'message_id': [1, 1, 1]})
        self.assertEqual(errores[rule.user_id][chat], ['fail'])
        self.assertEqual(rule.attempts, 0)
        self.assertEqual(rule.interacts, 2)


    def test_row_to_dict_chat(self):
        now = datetime.datetime.utcnow()
        chat = EmptyGenericClass(
            id=111111,
            type='group',
            title='Title',
            ignore=False,
            interacts=1,
            created_at=now,
            updated_at=now,
            rules=object(),
            _sa_instance_state=object(),
        )
        result = core.row_to_dict(chat)
        expected = {'id': 111111,
                    'type': 'group',
                    'title': 'Title',
                    'interacts': 1}
        self.assertEqual(result, expected)


    def test_row_to_dict_rule(self):
        now = datetime.datetime.utcnow()
        rule = EmptyGenericClass(
            id=222222,
            name='Name',
            regex='(.*)',
            lname='array',
            args='message_id;chat_id; chat_username',
            url='https://test.io/',
            send='GET',
            ignore=False,
            attempts=0,
            created_at=now,
            updated_at=now,
            used_at=now,
            user_id=333333,
            chat_id=111111,
            chat=object(),
            _sa_instance_state=object()
        )
        result = core.row_to_dict(rule)
        expected = {'id': 222222,
                    'name': 'Name',
                    'regex': '(.*)',
                    'lname': 'array',
                    'args': ['message_id', 'chat_id', 'chat_username'],
                    'url': 'https://test.io/',
                    'send': 'GET',
                    'attempts': 0,
                    'user_id': 333333}
        self.assertEqual(result, expected)


    def test_show_regex_html(self):
        data = (
            (None, texts.SET_GLOBAL),
            ('(\\w+)', '(\\w+)'),
            (' \\s\n\\w+ \\d+', '«multilinea»\n␠\\s\n\\w+␠\\d+'),
        )
        for regex, expected in data:
            with self.subTest(regex=regex, expected=expected):
                self.assertEqual(core.show_regex_html(regex), expected)


    @mock.patch('indexius.core.Updater')
    def test_run(self, class_updater):
        data = (
            # argv, start_polling, start_webhook
            ([], False, True),
            (['-v'], False, True),
            (['-vv'], False, True),
            (['-vvv'], False, True),
            (['-v', '-v'], False, True),
            (['-v', '-c'], False, True),
            (['-p'], True, False),
            (['-p', '-v'], True, False),
            (['-p', '-c'], True, False),
            (['-p', '-c', '-v'], True, False),
            (['-p', '-c', '-v', '-v'], True, False),
            (['-p', '-c', '-vvvv'], True, False),
        )
        updater = class_updater.return_value
        for argv, start_polling, start_webhook in data:
            with self.subTest(argv=argv):
                with mock.patch.object(sys, 'argv', ['python3'] + argv):
                    updater.reset_mock()
                    exit_status = core.run()
                    self.assertTrue(updater.dispatcher.add_handler.called)
                    self.assertTrue(updater.dispatcher.add_error_handler.called)
                    self.assertEqual(updater.start_polling.called, start_polling)
                    self.assertEqual(updater.start_webhook.called, start_webhook)
                    self.assertTrue(updater.idle.called)
                    self.assertEqual(exit_status, 0)
