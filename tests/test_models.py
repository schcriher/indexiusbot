# -*- coding: UTF-8 -*-
# Copyright (C) 2019 Schmidt Cristian Hernán

import telegram

from environ import unittest, CFG, texts, models


def is_keyboard_correct(obj):
    if not hasattr(obj, 'keyboard'):
        return False
    if obj.keyboard and isinstance(obj.keyboard, list):
        for row in obj.keyboard:
            if isinstance(row, list):
                if not all(isinstance(b, str) for b in row):
                    return False
            else:
                return False
        return True
    return False


class TestModels(unittest.TestCase):


    def test_menuaction_str(self):
        self.assertEqual(str(models.MenuAction.CREATE), texts.CREATE_ACTION)
        self.assertEqual(str(models.MenuAction.UPDATE), texts.UPDATE_ACTION)
        self.assertEqual(str(models.MenuAction.DELETE), texts.DELETE_ACTION)
        self.assertEqual(str(models.MenuAction.LIST), texts.LIST_ACTION)
        self.assertEqual(str(models.MenuAction.TEST), texts.TEST_ACTION)


    def test_menuargs_correct_values(self):
        data = (
            # name, value, for_channel, for_group, for_control
            # For all
            ('MID', 'message_id', True, True, False),
            ('MDT', 'message_date', True, True, False),
            ('CID', 'chat_id', True, True, False),
            ('CTP', 'chat_type', True, True, False),
            ('CTT', 'chat_title', True, True, False),
            ('CUN', 'chat_username', True, True, False),
            # Only in channels
            ('PAU', 'post_author', True, False, False),
            ('PCW', 'post_website', True, False, False),
            # Only in groups
            ('UID', 'user_id', False, True, False),
            ('UFN', 'user_first_name', False, True, False),
            ('ULN', 'user_last_name', False, True, False),
            ('UUN', 'user_username', False, True, False),
            # For all (control)
            ('CLS', texts.MENUARGS_CLS_OPTION, False, False, True),
            ('END', texts.MENUARGS_END_OPTION, False, False, True),
        )
        for entry in data:
            with self.subTest(entry=entry):
                name, value, for_channel, for_group, for_control = entry
                obj = models.MenuArgs[name]
                self.assertEqual(str(obj), value)
                self.assertEqual(obj.value, value)
                self.assertEqual(obj.name, name)
                self.assertEqual(obj.for_channel, for_channel)
                self.assertEqual(obj.for_group, for_group)
                self.assertEqual(obj.for_control, for_control)


    def test_menuargs_methods(self):

        with self.subTest(test='MenuArgs.get_only_channels()'):
            lst = ['post_author', 'post_website']
            self.assertEqual(models.MenuArgs.get_only_channels(), lst)

        with self.subTest(test='MenuArgs.get_only_groups()'):
            lst = ['user_id', 'user_first_name', 'user_last_name', 'user_username']
            self.assertEqual(models.MenuArgs.get_only_groups(), lst)

        with self.subTest(test='MenuArgs.get_channels()'):
            lst = ['message_id', 'message_date', 'chat_id', 'chat_type',
                   'chat_title', 'chat_username', 'post_author', 'post_website']
            self.assertEqual(models.MenuArgs.get_channels(), lst)

        with self.subTest(test='MenuArgs.get_groups()'):
            lst = ['message_id', 'message_date', 'chat_id', 'chat_type',
                   'chat_title', 'chat_username', 'user_id',
                   'user_first_name', 'user_last_name', 'user_username']
            self.assertEqual(models.MenuArgs.get_groups(), lst)

        with self.subTest(test='MenuArgs.get_control()'):
            lst = [texts.MENUARGS_CLS_OPTION, texts.MENUARGS_END_OPTION]
            self.assertEqual(models.MenuArgs.get_control(), lst)


    def test_menuargs_incorrect_values(self):
        tgg = telegram.Chat.GROUP
        tgs = telegram.Chat.SUPERGROUP
        tgc = telegram.Chat.CHANNEL
        data = (
            (('message_id', tgg), models.MenuArgs.MID),
            (('message_id', tgs), models.MenuArgs.MID),
            (('message_id', tgc), models.MenuArgs.MID),
            (('post_author', tgg), None),
            (('post_author', tgs), None),
            (('post_author', tgc), models.MenuArgs.PAU),
            (('user_id', tgg), models.MenuArgs.UID),
            (('user_id', tgs), models.MenuArgs.UID),
            (('user_id', tgc), None),
        )
        for entry, expected in data:
            with self.subTest(entry=entry, expected=expected):
                arg, _error = models.MenuArgs.get(*entry)
                self.assertEqual(arg, expected)

        data = ('var_id', 'post_id', 'messageid', '')
        for entry in data:
            with self.subTest(entry=entry):
                arg, err = models.MenuArgs.get(entry, None)
                self.assertEqual(arg, None)
                self.assertEqual(err, 'Argumento no válido')


    def test_sendtype(self):
        data = (
            ('GET', 'GET', (texts.GET_OPTION, 'params')),
            ('POST', 'POST_FORM', (texts.POST_FORM_OPTION, 'data')),
            ('POST', 'POST_JSON', (texts.POST_JSON_OPTION, 'json')),
        )
        for method, name, value in data:
            with self.subTest(name=name, value=value):
                obj = models.SendType[name]
                self.assertEqual(obj.value, value)
                self.assertEqual(obj.name, name)
                self.assertEqual(obj.method, method)
                self.assertEqual(obj.var, value[1])

        lst = '\n'.join(f'\xa0•\xa0{i.name}:\xa0{i.value[0]}' for i in models.SendType)
        self.assertEqual(models.SendType.full_list(), lst)


    def test_keyboards(self):
        keyboards = [(k, v) for k, v in models.Keyboard.__dict__.items()
                     if k.isupper() and hasattr(v, 'keyboard')]

        for name, obj in keyboards:
            with self.subTest(name=name):
                self.assertTrue(is_keyboard_correct(obj))


    def test_keyboards_get(self):
        longer = 'ab' * CFG.MAX_CHARS_WIDE_KB

        lst1a = [texts.CREATE_ACTION, texts.UPDATE_ACTION, texts.DELETE_ACTION]
        lst1b = [lst1a]

        lst2a = ['message_date', 'chat_id', 'chat_type']
        lst2b = [['message_date', 'chat_id'], ['chat_type']]

        data = (
            (['a', 'b'], [[]], True, [['a', 'b']]),
            (['a', 'b'], [['c']], True, [['a', 'b'], ['c']]),
            (['a', 'b'], [['c', 'd']], False, [['a', 'b'], ['c', 'd']]),
            (['a', 'b'], [['c', 'd'], ['e']], False, [['a', 'b'], ['c', 'd'], ['e']]),
            (None, [['c', 'd'], ['e']], False, [['c', 'd'], ['e']]),
            (lst1a, None, True, lst1b),
            (lst2a, None, True, lst2b),
            (['a', longer, 'c'], None, True, [['a'], [longer], ['c']]),
        )
        for options, footer, one_time, expected in data:
            with self.subTest(options=options, footer=footer, one_time=one_time):
                keyboard = models.Keyboard.get(options, footer, one_time)
                self.assertTrue(is_keyboard_correct(keyboard))
                self.assertEqual(keyboard.keyboard, expected)
                self.assertEqual(keyboard.one_time_keyboard, one_time)
                self.assertTrue(keyboard.resize_keyboard)


    def test_keyboards_get_list(self):
        conf = [texts.NO_OPTION, texts.YES_OPTION]
        send = [i.name for i in models.SendType]
        data = (
            ('ACTION', [i.value for i in models.MenuAction]),
            ('NAME_UPDATE', [texts.UNCHANGED_OPTION]),
            ('REGEX_CREATE', [texts.OVERALL_OPTION]),
            ('REGEX_UPDATE', [texts.OVERALL_OPTION, texts.UNCHANGED_OPTION]),
            ('RCONF', conf),
            ('LNAME_UPDATE', [texts.UNCHANGED_OPTION]),
            ('LCONF', conf),
            ('ACONF', conf),
            ('URL_UPDATE', [texts.UNCHANGED_OPTION]),
            ('UCONF', conf),
            ('SEND_CREATE', send),
            ('SEND_UPDATE', send + [texts.UNCHANGED_OPTION]),
        )
        for name, expected in data:
            with self.subTest(case='name', name=name, expected=expected):
                self.assertEqual(models.Keyboard.get_list(name), expected)
            with self.subTest(case='keyboard', name=name, expected=expected):
                keyboard = getattr(models.Keyboard, name)
                self.assertEqual(models.Keyboard.get_list(keyboard), expected)


    def test_keyboards_get_args(self):
        channels = models.MenuArgs.get_channels()
        groups = models.MenuArgs.get_groups()
        control = models.MenuArgs.get_control()

        lcc = channels + control
        lccu = channels + control + [texts.UNCHANGED_OPTION]
        lgc = groups + control
        lgcu = groups + control + [texts.UNCHANGED_OPTION]

        exclude_1 = ['message_id', 'post_author']
        exclude_2 = ['chat_type', 'user_username']

        data = (
            ('channel', True, '', lcc),
            ('channel', False, '', lccu),
            ('channel', True, exclude_1, [i for i in lcc if i not in exclude_1]),
            ('channel', False, exclude_1, [i for i in lccu if i not in exclude_1]),

            ('group', True, '', lgc),
            ('group', False, '', lgcu),
            ('group', True, exclude_2, [i for i in lgc if i not in exclude_2]),
            ('group', False, exclude_2, [i for i in lgcu if i not in exclude_2]),

            ('supergroup', True, '', lgc),
            ('supergroup', False, '', lgcu),
            ('supergroup', True, exclude_2, [i for i in lgc if i not in exclude_2]),
            ('supergroup', False, exclude_2, [i for i in lgcu if i not in exclude_2]),
        )
        for entry in data:
            with self.subTest(entry=entry):
                keyboard = models.Keyboard.get_args(*entry[:3])
                self.assertTrue(is_keyboard_correct(keyboard))
                self.assertEqual(models.Keyboard.get_list(keyboard), entry[3])

        with self.subTest(case='private'):
            with self.assertRaises(AttributeError):
                keyboard = models.Keyboard.get_args('private', True, [])


    def test_wrong_keyboards(self):
        lst = enumerate(([], ['aa', 'b'], [['a', 'b'], 'c'], [['a'], [None]]))
        keyboards = [(i, models.Keyboard.get([], kb)) for i, kb in lst]

        keyboards.append((len(keyboards), True))
        keyboards.append((len(keyboards), object()))
        keyboards.append((len(keyboards), models.Keyboard.NULL))

        for idx, obj in keyboards:
            with self.subTest(idx=idx):
                self.assertFalse(is_keyboard_correct(obj))
