# -*- coding: UTF-8 -*-
# Copyright (C) 2019 Schmidt Cristian Hernán

import re
import socket
import pprint       # pylint: disable=unused-import
import builtins     # pylint: disable=unused-import
import unittest

from requests.exceptions import InvalidURL, RetryError

from environ import (mock, core, get_response, EmptyGenericClass, BotIOAdapter,
                     mocked_connection, mocked_session, MOCKED_SESSION_REQUEST_HISTORY)


class TestEnviron(unittest.TestCase):


    def setUp(self):
        self.maxDiff = None  # pylint: disable=invalid-name

        self.test = None
        self.tgs = None
        self.bio = None

        self.last_chat = None
        self.bookmark = 0
        self.history = []


    @mock.patch('indexius.core.socket.create_connection', side_effect=mocked_connection)
    def test_mocked_connection(self, create_connection):
        port = 0
        data = (
            'ok.',
            'ok.bool',
            'ok.json',
            'error.text',
            'error.json',
            'error.unauthorized',
        )
        for host in data:
            with self.subTest(host=host):
                self.assertIsInstance(create_connection((host, port)), mock.Mock)
        data = (
            'raise.',
            'raise.xxx',
            'raise.invalid_url',
            'raise.response_error',
        )
        for host in data:
            with self.subTest(host=host):
                with self.assertRaises(socket.error):
                    create_connection((host, port))
        data = (
            'ok',
            'error',
            'raise',
            'xxx',
        )
        for host in data:
            with self.subTest(host=host):
                with self.assertRaises(NotImplementedError):
                    create_connection((host, port))


    @mock.patch('indexius.core.Session', side_effect=mocked_session)
    def test_mocked_session(self, session_class):
        MOCKED_SESSION_REQUEST_HISTORY.clear()

        session = session_class()

        self.assertIsNone(session.mount('a', b='b'))
        self.assertIsNone(session.close('a', b='b'))

        data = (
            ('http://ok.net/test?v=1', 200, 'OK'),
            ('http://ok.bool/test?v=1', 200, 'OK'),
            ('http://ok.json/test?v=1', 200, 'OK'),
            ('http://error.text/test?v=1', 200, 'OK'),
            ('http://error.json/test?v=1', 200, 'OK'),
            ('http://error.unauthorized/test?v=1', 401, 'Unauthorized'),
        )
        for url, code, reason in data:
            with self.subTest(url=url, code=code, reason=reason):
                response = session.request('method', url)
                self.assertEqual(response.status_code, code)
                self.assertEqual(response.reason, reason)

                last = MOCKED_SESSION_REQUEST_HISTORY[-1]
                self.assertEqual(last, f'METHOD:{url}')

        data = (
            ('http://raise.invalid_url/test?v=1', InvalidURL),
            ('http://raise.response_error/test?v=1', RetryError),
        )
        for url, error in data:
            with self.subTest(url=url):
                with self.assertRaises(error):
                    session.request('method', url)

                last = MOCKED_SESSION_REQUEST_HISTORY[-1]
                self.assertEqual(last, f'METHOD:{url}')

        with self.subTest(case='with params'):
            url = 'http://ok.net/test?v=1'
            response = session.request('GET', url, params={'a': [1, 2], 'b': 3})

            last = MOCKED_SESSION_REQUEST_HISTORY[-1]
            self.assertEqual(last, f'GET:{url}&a=1&a=2&b=3')

        with self.subTest(case='not covered'):
            with self.assertRaises(NotImplementedError):
                session.request('method', 'url')


    def test_emptygenericclass(self):
        # pylint: disable=no-member

        obj = EmptyGenericClass(a=1, b=2, c='C')
        self.assertEqual(obj.a, 1)
        self.assertEqual(obj.b, 2)
        self.assertEqual(obj.c, 'C')
        self.assertFalse(hasattr(obj, 'd'))


    def test_get_response(self):
        # pylint: disable=protected-access
        content = b'content test'
        url = 'https://unit.test/'
        response = get_response(content, url)

        self.assertEqual(response.request.url, url)
        self.assertEqual(response.request.body, b'')
        self.assertEqual(response.request.method, 'GET')
        self.assertEqual(response.request.headers, {})

        self.assertEqual(response.url, url)
        self.assertEqual(response.reason, 'OK')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.headers, {})
        self.assertEqual(response.encoding, 'utf-8')
        self.assertEqual(response._content_consumed, True)
        self.assertEqual(response._content, content)


    def test_botioadapter_set_optionals(self):
        data = (
            ({}, ['a'], {'caption': 'a'}),
            ({}, [[1]], {'markup': [1]}),
            ({}, ['b', [2]], {'caption': 'b', 'markup': [2]}),
            ({'id': 3}, ['a'], {'id': 3, 'caption': 'a'}),
            ({'id': 3}, [[1]], {'id': 3, 'markup': [1]}),
            ({'id': 3}, ['b', [2]], {'id': 3, 'caption': 'b', 'markup': [2]}),
            ({'id': 3}, ['a', object()], {'id': 3, 'caption': 'a'}),
            ({'id': 3}, [[1], 9], {'id': 3, 'markup': [1]}),
            ({'id': 3}, ['b', [2], {}], {'id': 3, 'caption': 'b', 'markup': [2]}),
        )
        for dic, opts, expected in data:
            with self.subTest(dic=dic, opts=opts, expected=expected):
                BotIOAdapter.set_optionals(dic, opts)
                self.assertEqual(dic, expected)


    def test_botioadapter_adjust(self):
        test = mock.Mock(unsafe=True)
        tgs = mock.Mock()
        bio = BotIOAdapter(test, tgs)
        bio.last_chat = 9
        reg = re.compile(r'\w{3}\.\d{3}')
        data = (
            ({'chat': 1}, {'chat': 1}),
            ({'chat': None}, {'chat': 9}),
            ({'chat': 2, 'user': 'dev'}, {'chat': 2, 'user': 'dev'}),
            ({'chat': None, 'user': 'bot'}, {'chat': 9, 'user': 'bot'}),
            ({'chat': 3, 'tex': reg}, {'chat': 3, 'tex': 'abc.123'}),
        )
        for dic, response in data:
            with self.subTest(dic=dic, response=response):
                bio.adjust(dic, response)
                self.assertEqual(dic, response)


    def test_botioadapter_header(self):
        data = (
            ('a', {'user': 'a', 'chat': None}),
            ('a:b', {'user': 'a', 'chat': 'b'}),
        )
        for header, expected in data:
            with self.subTest(header=header, expected=expected):
                obj = BotIOAdapter.HEADER.match(header).groupdict()
                self.assertEqual(obj, expected)

        data = (
            'a:b:1'
            'a:2',
            'a:b:.3',
            'a:.4',
            #
            ':b',
            ':b:1',
            ':2',
            '3:a:b',
            '4:b',
            '5:b:a',
            'a:6:b',
        )
        for header in data:
            with self.subTest(header=header):
                with self.assertRaises(AttributeError):
                    BotIOAdapter.HEADER.match(header).groupdict()


    def check(self, call=1):
        # nonlocal
        self.assertEqual(self.bio.last_chat, self.last_chat)
        self.assertEqual(self.bio.bookmark, self.bookmark)
        self.assertEqual(self.bio.history, self.history)
        # mocked
        self.assertEqual(self.test.assertTrue.call_args_list,
                         [mock.call(self.tgs.empty())] * call)
        # reset
        self.test.reset_mock()
        self.tgs.reset_mock()


    @mock.patch('pprint.pprint')
    @mock.patch('builtins.print')
    def test_botioadapter(self, _print, _pprint):
        self.test = mock.Mock(unsafe=True)
        self.tgs = mock.Mock()
        self.bio = BotIOAdapter(self.test, self.tgs)

        interaction = ['dev:g1', 'content', 'caption', ['markup']]
        data = {'user': 'dev', 'chat': 'g1', 'content': 'content',
                'caption': 'caption', 'markup': ['markup']}
        self.last_chat = 'g1'
        self.history.append(interaction)
        self.bio.run([interaction])
        self.assertEqual(self.tgs.send.call_args, mock.call(data))
        self.check()

        func = mock.Mock(spec=lambda **kwargs: None)
        self.bio.run([func])
        self.assertEqual(func.call_args, mock.call(core=core,
                                                   test=self.test,
                                                   tgs=self.tgs))
        self.check()

        self.bookmark = 1
        self.history.clear()
        self.bio.run([self.bookmark])
        self.check(2)

        pattern = re.compile('tex')
        interaction = ['bot', pattern]
        data = {'user': 'bot', 'chat': self.last_chat, 'content': 'tex'}
        self.tgs.recv.return_value = data
        self.history.append(interaction)
        self.bio.run([interaction])
        self.assertEqual(self.tgs.recv.call_args, mock.call())
        self.assertEqual(self.test.assertEqual.call_args, mock.call(data, data))
        self.check()

        # ---

        interaction = 3

        self.tgs.responses.__len__ = mock.Mock(return_value=0)
        with self.assertRaises(ValueError):
            self.bio.run([interaction])  # the last bookmark was 1
        self.check()

        self.assertEqual(_print.call_args_list, [
            mock.call('\nbookmark 1'),
            mock.call('\n', len(self.history)),
            mock.call('\n'),
        ])
        self.assertEqual(_pprint.call_args_list, [
            mock.call(self.history),
            mock.call(interaction),
        ])

        _print.reset_mock()
        _pprint.reset_mock()

        interaction = ['1IncorrectUser', 'content']

        self.tgs.responses.__len__ = mock.Mock(return_value=2)
        with self.assertRaises(AttributeError):
            self.bio.run([interaction])
        self.tgs.empty.side_effect = None
        self.check(0)

        self.assertEqual(_print.call_args_list, [
            mock.call('\nbookmark 1'),
            mock.call('\n', len(self.history)),
            mock.call('\ntgs.recv 1'),
            mock.call('\ntgs.recv 2'),
            mock.call('\n'),
        ])
        self.assertEqual(_pprint.call_args_list, [
            mock.call(self.history),
            mock.call(interaction),
            mock.call(data),
            mock.call(data),
        ])
