# -*- coding: UTF-8 -*-
# Copyright (C) 2019 Schmidt Cristian Hernán

# https://core.telegram.org/bots/api
# https://github.com/python-telegram-bot/python-telegram-bot/wiki/Webhooks

import re
import sys
import copy
import json
import time
import inspect
import logging
import operator
import functools
import traceback
import collections

from telegram import Bot, Update
from telegram.ext import Dispatcher
from telegram.error import BadRequest

BOTID = 11111
DEVID = 22222
ADMID = 33333
USRID = 44444

G1ID = -10011
G2ID = -10012
G3ID = -10013
S1ID = -10021
S2ID = -10022
S3ID = -10023
C1ID = -10031
C2ID = -10032
C3ID = -10033

TOKEN = f'{BOTID}:abcdefghijklmnopqrstuvwxyzABCDEFGHI'

USER_HEAD = ('id', 'is_bot', 'username', 'first_name', 'last_name')
USER_DATA = ((BOTID, True, 'bot', 'Cupidus', None),
             (DEVID, False, 'dev', 'Bran', 'Stark'),
             (ADMID, False, 'adm', 'Arya', 'Stark'),
             (USRID, False, 'usr', 'Robb', 'Stark'))

CHAT_HEAD = ('id', 'title', 'username', 'type')
CHAT_DATA = ((G1ID, 'G1', 'g1', 'group'),
             (G2ID, 'G2', 'g2', 'group'),
             (G3ID, 'G3', 'g3', 'group'),
             (S1ID, 'S1', 's1', 'supergroup'),
             (S2ID, 'S2', 's2', 'supergroup'),
             (S3ID, 'S3', 's3', 'supergroup'),
             (C1ID, 'C1', 'c1', 'channel'),
             (C2ID, 'C2', 'c2', 'channel'),
             (C3ID, 'C3', 'c3', 'channel'))

EXTRACT_ENTITIES_HTML = {
    'bold': re.compile(r'<(b|strong)>(?P<text>[^<]+)</\1>'),
    'italic': re.compile(r'<(i|em)>(?P<text>[^<]+)</\1>'),
    'code': re.compile(r'<(code)>(?P<text>[^<]+)</\1>'),
    'pre': re.compile(r'<(pre)>(?P<text>[^<]+)</\1>'),
    'bot_command': re.compile(r'/\w+'),
    'mention': re.compile(r'@\w+'),
    'hashtag': re.compile(r'#\w+'),
}

INTRODUCE_ENTITIES_HTML = {
    'bold': '<b>{}</b>',
    'italic': '<i>{}</i>',
    'code': '<code>{}</code>',
    'pre': '<pre>{}</pre>',
}

STATUS_LETTER = {
    'a': 'administrator',
    'c': 'creator',
    'm': 'member',
    'l': 'left',
    'k': 'kicked',
    'r': 'restricted',
}


# ---


BASE0 = {'file_id': 'FILE_ID', 'file_size': 123}

BASE1 = BASE0.copy()
BASE1['height'] = 123
BASE1['width'] = 123

BASE2 = BASE0.copy()
BASE2['thumb'] = BASE1.copy()

PHOTO = [BASE1.copy(), BASE1.copy()]

STICKER = BASE1.copy()
STICKER['emoji'] = '👍'
STICKER['is_animated'] = False
STICKER['set_name'] = 'SET_NAME'
STICKER['thumb'] = BASE1.copy()

PDF = BASE2.copy()
PDF['file_name'] = 'FILE_NAME.pdf'
PDF['mime_type'] = 'document/pdf'

GIF_DOCUMENT = BASE2.copy()
GIF_DOCUMENT['file_name'] = 'FILE_NAME.mp4'
GIF_DOCUMENT['mime_type'] = 'video/mp4'

GIF_ANIMATION = GIF_DOCUMENT.copy()
GIF_ANIMATION['duration'] = 1
GIF_ANIMATION['height'] = 123
GIF_ANIMATION['width'] = 123

MEDIA_MAP = {
    'photo': {'photo': PHOTO},
    'sticker': {'sticker': STICKER},
    'document': {'document': PDF},
    'animation': {'document': GIF_DOCUMENT, 'animation': GIF_ANIMATION},
}


# ---


def flogger(func):
    logger = logging.getLogger(func.__module__)

    @functools.wraps(func)
    def flogger_wrapper(self, *args, **kwargs):
        logger.debug('→ %s: %s, %s', func.__name__, args, kwargs)
        result = func(self, *args, **kwargs)
        logger.debug('← %s: %s', func.__name__, result)
        return result

    return flogger_wrapper


def is_channel(dic):
    """ Determines if a chat or message is or comes from a channel """
    if 'chat' in dic:
        return dic['chat']['type'] == 'channel'
    if 'type' in dic:
        return dic['type'] == 'channel'
    raise ValueError('cannot get the chat type')


def adjust_offsets(entities, end, add):
    """ Adjusts the positions by inserting the format entities """
    for entity in entities:
        if entity['offset'] >= end:
            entity['offset'] += add


def extract_entities_html(text):
    """ Extracts the formatting entities from the text """
    entities = []
    for typ, pattern in EXTRACT_ENTITIES_HTML.items():
        obj = pattern.search(text)
        while obj:
            start, end = obj.span()
            for entity in entities:
                _start = entity['offset']
                _end = _start + entity['length']
                if not (start >= _end or end <= _start):
                    break  # tags must not be nested (CHANGE in API 4.5)
            else:
                components = obj.groupdict()
                if components:
                    num = len(text)
                    text = text[:start] + components['text'] + text[end:]
                    num = len(text) - num  # if text is shorter the num is negative
                    end += num
                    adjust_offsets(entities, end, num)

                length = end - start
                entities.append({'type': typ, 'offset': start, 'length': length})
            obj = pattern.search(text, end)
    entities.sort(key=operator.itemgetter('offset'))
    return text, entities


def introduce_entities_html(text, entities):
    """ Introduces formatting entities to the text """
    entities = copy.deepcopy(entities or [])
    entities.sort(key=operator.itemgetter('offset'))
    while entities:
        entity = entities.pop()
        template = INTRODUCE_ENTITIES_HTML.get(entity['type'])
        if template:
            start = entity['offset']
            end = start + entity['length']
            num = len(text)
            text = text[:start] + template.format(text[start:end]) + text[end:]
            num = len(text) - num  # if text is longer the num is positive
            end += num
            adjust_offsets(entities, end, num)
    return text


def extract_markup(markup):
    """ Extracts the texts from the keyboard markup """
    texts = []

    if 'keyboard' in markup:
        for row in markup['keyboard']:
            for col in row:
                texts.append(col)
        return texts

    if 'inline_keyboard' in markup:
        for row in markup['inline_keyboard']:
            for col in row:
                texts.append(col['text'])
        return texts

    raise ValueError('incorrect reply_markup')


def get_callback_data(message, text):
    """ Extract data from a callback, that corresponds to the text """
    ikb = message['reply_markup']['inline_keyboard']
    for row in ikb:
        for col in row:
            if col['text'] == text:
                return col['callback_data']
    raise KeyError(text)


def return_deepcopy(func):
    """ Returns a copy of the result """
    @functools.wraps(func)
    def return_deepcopy_wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        return copy.deepcopy(result)
    return return_deepcopy_wrapper


def dict_to_bytes(func):
    """ Decorator to convert a dictionary to a small json encoded in bytes """
    @functools.wraps(func)
    def dict_to_bytes_wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        return json.dumps(result).encode()
    return dict_to_bytes_wrapper


def remove_keyboard(obj):
    """ Decorator to remove the "keyboard" from the reply markup """

    # Decorator
    if inspect.isfunction(obj):
        @functools.wraps(obj)
        def remove_keyboard_wrapper(*args, **kwargs):
            result = obj(*args, **kwargs)
            result = remove_keyboard(result)
            return result
        return remove_keyboard_wrapper

    # Removal function
    if isinstance(obj, dict):

        markup = obj.get('reply_markup')
        if markup and 'keyboard' in markup:
            obj.pop('reply_markup')

        for value in obj.values():
            remove_keyboard(value)

    return obj


def make_dict(head, row):
    """ Create a dictionary with lists of names (head) and values (row) """
    data = {}
    for param, value in zip(head, row):
        data[param] = value
    return data


def get_int(dic, var):
    """ Returns an integer of the 'var' variable present in 'dic'
        If it does not exist returns None
    """
    if dic and var in dic:
        return int(dic[var])
    return None


def get_author_signature(user):
    """ Returns the signature of the author of a channel """
    return f'{user["first_name"] or ""} {user["last_name"] or ""}'.strip()


def check_message(messages):
    """ Checks that messages are in ascending order and are not duplicated """
    indexes = [m['message_id'] for m in messages]

    if indexes != sorted(indexes):
        raise IndexError('library error, disordered messages')

    if indexes != sorted(set(indexes)):
        raise IndexError('library error, duplicate messages')


# ---


USERS = {}      # {user_id: dict}
CHATS = {}      # {chat_id: dict}
NICKS = {}      # {username: chat_id/user_id}
STATUS = {}     # {chat_id: {user_id: status}}
CONSENT = {}    # {chat_id: True/False}             Can the bot send message?

_status = {}    # pylint: disable=invalid-name

for _row in USER_DATA:
    _data = make_dict(USER_HEAD, _row)
    _author = get_author_signature(_data)

    assert _data['id'] not in USERS
    assert _data['id'] not in CHATS
    assert _data['id'] not in CONSENT
    assert _data['username'] not in NICKS
    assert _author not in NICKS

    USERS[_data['id']] = copy.deepcopy(_data)
    NICKS[_data['username']] = _data['id']
    NICKS[_author] = _data['id']
    CONSENT[_data['id']] = False

    if _data['is_bot']:
        _status[_data['id']] = 'member'
    else:
        _status[_data['id']] = 'left'
        _data.pop('is_bot')
        _data['type'] = 'private'
        CHATS[_data['id']] = _data

for _row in CHAT_DATA:
    _data = make_dict(CHAT_HEAD, _row)

    assert _data['id'] not in CHATS
    assert _data['id'] not in STATUS
    assert _data['id'] not in CONSENT
    assert _data['username'] not in NICKS

    CHATS[_data['id']] = _data
    STATUS[_data['id']] = copy.deepcopy(_status)
    NICKS[_data['username']] = _data['id']
    CONSENT[_data['id']] = True

del _row
del _data
del _author
del _status


# ---


class HandlersDict(dict):

    def __getitem__(self, key):
        handlers = super().__getitem__(key)
        for handler in handlers:
            if not hasattr(handler, "__wrapped__"):
                handler.handle_update = self.handle_update(handler.handle_update)
                handler.__wrapped__ = True
        return handlers

    @classmethod
    def handle_update(cls, method):
        @functools.wraps(method)
        def handle_update_wrapper(*args, **kwargs):
            # pylint: disable=bare-except
            try:
                return method(*args, **kwargs)
            except BadRequest:
                raise
            except:
                # Allows to stop the execution of the tests
                # if there is a bug in the bot code.
                sys.exit('bot execution error')
        return handle_update_wrapper


class TelegramServer:
    """ Flow of requests and responses:

        Real    user    telegram    bot
                 |         |<-------a|
                 |         |b------->|
                 |         |         |
                 |c------->|d------->|
                 |<------f1|<-------e|
                 |         |f2------>|
        Fake  req/res    (TGS)
    """
    # pylint: disable=too-many-instance-attributes

    BASE_URL = 'https://api.telegram.org/bot'
    CMD_URL = re.compile(rf'{BASE_URL}(?P<TOKEN>(?P<ID>\d+):[\w-]+)/(?P<CMD>\w+)')

    def __init__(self):
        """ Fake Telegram Server Constructor """
        self.logger = logging.getLogger(__name__)

        self.unique_id = 0  # for chat_instance and id in callbacks, and update_id
        self.messages = {}  # all messages, {chat_id: [message,]}

        self.users = copy.deepcopy(USERS)       # {user_id: dict}
        self.chats = copy.deepcopy(CHATS)       # {chat_id: dict}
        self.nicks = copy.deepcopy(NICKS)       # {username: chat_id/user_id}
        self.status = copy.deepcopy(STATUS)     # {chat_id: {user_id: status}}
        self.consent = copy.deepcopy(CONSENT)   # {chat_id: True/False}

        self.responses = collections.deque()

        self.bot = None
        self.dispatcher = None


    def add_handlers(self, add_handlers):
        """ Set the handlers (from the bot) on the dispatcher """
        self.bot = Bot(TOKEN)

        self.dispatcher = Dispatcher(self.bot, None, workers=0)
        self.dispatcher.handlers = HandlersDict()

        add_handlers(self.dispatcher)

        # Patch bot actions
        # pylint: disable=protected-access
        request = self.bot._request
        request._request_wrapper = self._request(request._request_wrapper)


    def _request(self, func):
        """ Bot request patcher """
        @functools.wraps(func)
        def _request_wrapper(_method, url, **kwargs):
            """ Request method replacement `_request_wrapper` from `bot._request`
                is a wrap of `urllib3.request.RequestMethods`
            """
            try:
                match = self.CMD_URL.match(url)
                cmd = match.group('CMD').lower()
                body = kwargs.get('body')
                body = json.loads(body) if body else None
                return self.request(cmd, body)
            except BadRequest:
                raise
            except:
                traceback.print_exc()
                raise
        return _request_wrapper


    @dict_to_bytes
    @remove_keyboard
    @return_deepcopy
    @flogger
    def request(self, cmd, body):
        """ Server Request """
        # pylint: disable=too-many-branches,too-many-locals,too-many-statements

        message = None
        response = None

        cid = get_int(body, 'chat_id')
        uid = get_int(body, 'user_id')
        mid = get_int(body, 'message_id')

        if cmd == 'getme':
            response = {'ok': True, 'result': self.get_user('bot')}

        elif cmd in ('answercallbackquery', 'sendchataction'):
            response = {'ok': True, 'result': True}

        elif cmd in ('sendmessage', 'editmessagetext'):
            self._check_consent(cid)
            message = self.process_message(body)
            response = {'ok': True, 'result': message}

        elif cmd == 'deletemessage':
            self._check_consent(cid)
            self.del_message(cid, mid)
            response = {'ok': True, 'result': True}

        elif cmd == 'forwardmessage':
            self._check_consent(cid)
            message = self.process_message(body)
            response = {'ok': True, 'result': message}

        elif cmd in ('sendphoto', 'sendsticker', 'senddocument', 'sendanimation'):
            self._check_consent(cid)
            message = self.generate_message(BOTID, cid, cmd[4:])
            response = {'ok': True, 'result': message}

        elif cmd == 'getchatmember':
            self._check_consent(cid)
            member = {
                'user': self.get_user(uid),
                'status': self.status[cid][uid]
            }
            response = {'ok': True, 'result': member}

        elif cmd == 'getchat':
            self._check_consent(cid)
            chat = self.get_chat(cid)
            response = {'ok': True, 'result': chat}

        elif cmd == 'getchatadministrators':
            self._check_consent(cid)
            result = []
            for user_id, status in self.status[cid].items():
                if status in ('creator', 'administrator'):
                    user = self.get_user(user_id)
                    data = {'status': status, 'user': user}
                    result.append(data)
            response = {'ok': True, 'result': result}

        elif cmd == 'restrictchatmember':
            self._check_consent(cid)
            response = self._enabled_edit_members(cmd, cid, uid)
            unrestrict = all((
                body.get('can_send_messages', False),
                body.get('can_send_media_messages', False),
                body.get('can_send_other_messages', False),
                body.get('can_add_web_page_previews', False),
            ))
            status = 'member' if unrestrict else 'restricted'
            self.status[cid][uid] = status

        elif cmd == 'kickchatmember':
            self._check_consent(cid)
            response = self._enabled_edit_members(cmd, cid, uid)
            self.status[cid].pop(uid)

        if response:
            if message:
                self.add_message(message)       # store
                self.responses.append(message)  # send
            return response
        raise NotImplementedError(cmd)


    @flogger
    def _enabled_edit_members(self, cmd, cid, uid):
        chat = self.get_chat(cid)
        status = self.status[cid]
        cannot = (
            cmd == 'restrictchatmember' and chat['type'] != 'supergroup',
            status[uid] == 'creator',
            status[uid] == 'administrator',
            status[BOTID] != 'administrator',
        )
        if any(cannot):
            raise BadRequest(f'Cannot: cmd={cmd}, cid={cid}, uid={uid}')
        return {'ok': True, 'result': True}


    @flogger
    def _check_consent(self, cid):
        # If cid not exists can be a proof of non-existent chat
        if cid == BOTID:
            raise ValueError('bot not sent things to itself')
        if not self.consent.get(cid):
            raise BadRequest(f'do not have permission in chat {cid}')


    @flogger
    def process_message(self, body):
        """ Processes the messages, prepares the response """
        chat_id = int(body['chat_id'])

        if 'from_chat_id' in body:
            # content = 'forward:CID:MID'
            content = ['forward', body['from_chat_id'], body['message_id']]

        elif 'reply_to_message_id' in body:
            # content = 'reply:MID:text'
            content = ['reply', body['reply_to_message_id'], body['text']]

        elif 'message_id' in body:
            # content = 'edit:MID:text'
            content = ['edit', body['message_id'], body['text']]

        else:
            # content = 'text'
            content = [body['text']]

        message = self.generate_message(BOTID, chat_id, ':'.join(content))

        markup = body.get('reply_markup')
        if markup:
            message['reply_markup'] = json.loads(markup)
        else:
            message.pop('reply_markup', None)

        return message


    @flogger
    def generate_message(self, user, chat, content):
        """ Compose a message, chat is None "private message" is created """
        # pylint: disable=too-many-branches,too-many-locals,too-many-statements

        chat = self.get_chat(chat or user)
        user = self.get_user(user)

        if content.startswith('edit:'):
            # content = 'edit:MID:text'
            idx = content.index(':', 5)  # second ":"
            mid = int(content[5:idx])
            message = self.get_message(chat['id'], mid)
            message['edit_date'] = int(time.time())
            content = content[idx + 1:]
        else:
            message = {'chat': chat,
                       'date': int(time.time()),
                       'message_id': self.next_message_id(chat['id'])}

        if is_channel(chat):
            author = get_author_signature(user)
            message['author_signature'] = author
        else:
            message['from'] = user

        if content in MEDIA_MAP:
            # content = photo|sticker|document|animation
            message.update(MEDIA_MAP[content])

        elif content.startswith('title:'):
            # content = 'title:text'
            message['new_chat_title'] = content[6:]
            message.pop('author_signature', None)

        elif content.startswith('members:'):
            # content = 'members:[-]u1,u2'
            if content[8] == '-':
                message['left_chat_member'] = self.get_user(content[9:])
            else:
                members = [self.get_user(u) for u in content[8:].split(',')]
                message['new_chat_members'] = members

        elif content.startswith('migrate:'):
            # content = 'migrate:chat_id'
            _cid = int(content[8:])
            _chat = self.get_chat(_cid)

            if chat['type'] == 'group':
                if _chat['type'] != 'supergroup':
                    raise ValueError(f'chat {_cid} is not a supergroup')
                var = 'migrate_to_chat_id'

            elif chat['type'] == 'supergroup':
                if _chat['type'] != 'group':
                    raise ValueError(f'chat {_cid} is not a group')
                var = 'migrate_from_chat_id'

            else:
                raise ValueError('function valid only for groups and supergroups')

            message[var] = _cid

        elif content.startswith('forward:'):
            # content = 'forward:CID:MID'
            _cid, _mid = map(int, content.split(':')[1:])
            _message = self.get_message(_cid, _mid)
            message['forward_date'] = _message.pop('date')
            message['forward_from'] = _message.pop('from')

            for key in MEDIA_MAP:
                if key in _message:
                    message.update(MEDIA_MAP[key])
                    break
            else:
                message['text'] = _message.pop('text')
                message['entities'] = _message.pop('entities', [])

        else:
            if content.startswith('reply:'):
                # content = 'reply:MID:text'
                idx = content.index(':', 6)  # second ":"
                _mid = int(content[6:idx])
                message['reply_to_message'] = self.get_message(chat['id'], _mid)
                content = content[idx + 1:]

            text, entities = extract_entities_html(content)
            message['text'] = text
            message['entities'] = entities

        if not message.get('entities'):
            message.pop('entities', None)

        return message


    @flogger
    def next_unique_id(self):
        """ Returns the next unique id """
        self.unique_id += 1
        return self.unique_id


    @flogger
    def next_message_id(self, chat_id):
        """ Returns the next message_id of the chat_id """
        messages = self.messages.get(chat_id)
        if messages:
            check_message(messages)
            return messages[-1]['message_id'] + 1
        return 1


    @return_deepcopy
    @flogger
    def get_message(self, chat_id, message_id=None, throw_error=True):
        """ Returns the message_id from the chat_id,
            or the last one if the message_id is None
        """
        messages = self.messages.get(chat_id)

        if messages is None:
            if throw_error:
                raise BadRequest(f'chat {chat_id} not found')
            return {}

        check_message(messages)
        try:
            if message_id is None:
                return messages[-1]
            for message in messages:
                if message['message_id'] == message_id:
                    return message
        except IndexError:
            pass

        if throw_error:
            raise BadRequest(f'message {message_id} not found')
        return {}


    @flogger
    def add_message(self, message):
        """ Add or update a message """
        if isinstance(message, dict):
            messages = self.messages.setdefault(message['chat']['id'], [])

            # If updated, the old must be deleted
            for _message in messages:
                if _message['message_id'] == message['message_id']:
                    messages.remove(_message)
                    break

            messages.append(message)
            messages.sort(key=operator.itemgetter('message_id'))
        else:
            raise ValueError('is not message')


    @flogger
    def del_message(self, chat_id, message_id):
        """ Delete a message_id from the chat_id """
        messages = self.messages.get(chat_id, [])
        for message in messages:
            if message['message_id'] == message_id:
                messages.remove(message)
                return
        raise BadRequest(f'message {message_id} not found')


    @flogger
    def set_states(self, table):
        """ Setting the states """
        head, *data = table.strip().split('\n')
        head = head.split()
        data = [d.split() for d in data]
        for row in data:
            user, *chat_status = row
            user_id = self.get_user_id(user)
            for chat, status in zip(head, chat_status):
                chat_id = self.get_chat_id(chat)
                self.status[chat_id][user_id] = STATUS_LETTER[status]


    #@flogger
    def __get_id(self, key, dic, typ):
        """ Returns a user/chat id, key can be id or username """
        if key in dic:
            return key
        if key in self.nicks:
            return self.nicks[key]
        raise BadRequest(f'{typ} not found')


    @flogger
    def get_user_id(self, key):
        """ Returns a user_id, key can be id, username """
        return self.__get_id(key, self.users, 'user')


    @flogger
    def get_chat_id(self, key):
        """ Returns a chat_id, key can be id, username """
        return self.__get_id(key, self.chats, 'chat')


    @flogger
    def get_user(self, key):
        """ Returns a user's data, key can be id, username """
        user_id = self.__get_id(key, self.users, 'user')
        return self.users[user_id]


    @flogger
    def get_chat(self, key):
        """ Returns a chat's data, key can be id, username """
        chat_id = self.__get_id(key, self.chats, 'chat')
        return self.chats[chat_id]


    def get_emitter_username(self, message):
        """ Returns the username of the sender """
        if 'from' in message:
            return message['from']['username']

        uid = self.nicks[message['author_signature']]
        return self.get_user(uid)['username']


    @flogger
    def empty(self):
        return len(self.responses) == 0


    @flogger
    def send(self, data):
        """ Adapts the data to be sent to the server """
        user = data['user']
        chat = data['chat']

        uid = self.get_user_id(user)
        if chat and user != chat:
            cid = self.get_chat_id(chat)
            if self.status[cid][uid] in ('kicked', 'left'):
                raise ValueError(f'"{user}" is not in "{chat}"')
        else:
            cid = uid
        self.consent[cid] = True

        content = data.get('content')

        # callback_query are automatic, so you can't send a message without
        # replying to the last inline_keyboard (limitation for simplicity)
        last_message = self.get_message(cid, throw_error=False)
        same_chat = last_message.get('chat', {}).get('id') == cid
        inline_kb = last_message.get('reply_markup', {}).get('inline_keyboard')
        if same_chat and inline_kb:
            request = {'callback_query': {
                'chat_instance': self.next_unique_id(),
                'data': get_callback_data(last_message, content),
                'from': self.get_user(user),
                'id': self.next_unique_id(),
                'message': last_message}}
        else:
            message = self.generate_message(user, chat, content)
            var = 'channel_post' if is_channel(message) else 'message'
            if content.startswith('edit:'):
                var = f'edited_{var}'
            request = {var: message}

            if 'caption' in data:
                message['caption'] = data['caption']

            self.add_message(message)

        request['update_id'] = self.next_unique_id()

        update = Update.de_json(request, self.bot)
        self.dispatcher.process_update(update)


    @flogger
    def recv(self):
        """ Adapts the data received from the server """
        message = self.responses.popleft()

        response = {
            'user': self.get_emitter_username(message),
            'chat': message['chat']['username'],
            'content': None,
        }

        if 'text' in message:
            text = message.get('text')
            entities = message.get('entities')

            if entities:
                text = introduce_entities_html(text, entities)

            response['content'] = text
        else:
            for media in ('photo', 'sticker', 'animation', 'document'):
                if media in message:
                    response['content'] = media
                    break

        caption = message.get('caption')
        if caption:
            response['caption'] = caption

        markup = message.get('reply_markup')
        if markup and not markup.get('remove_keyboard'):
            response['markup'] = extract_markup(markup)

        return response
